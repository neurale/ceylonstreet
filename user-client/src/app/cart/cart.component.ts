import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from '../service/event-emitter.service';
import { ProductService } from '../service/product.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ppid } from 'process';
declare function Checkout(): any;

import io from 'socket.io-client';
import { ValueService } from '../service/value.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  sub = 0;
  x = [];
  socket;

  product = [];
  constructor(private eventEmitterService: EventEmitterService, private productService: ProductService, private router: Router, public valueService:ValueService) {
    const newLocal = new Promise(() => {
      this.loadScript();
    });
    this.socket = io.connect('https://ceylonstreet.herokuapp.com/');
    $('html, body').animate({
      scrollTop: 0
    }, 600);
    document.getElementsByTagName('body')[0].setAttribute('style', 'overflow:visible');
  }


  ngOnInit() {
    this.getproduct();
    this.eventEmitterService.
      invokeFirstComponentFunction1.subscribe((name: string) => {
        this.showCart();
      });
    this.socket.on('add-order', () => {
      setTimeout(() => {
        this.getproduct();
      }, 2000);
    });

  }

  getproduct(){
    this.productService.getProducts().subscribe(
      data => {
        // tslint:disable-next-line: no-string-literal
        this.product = data['data'];
        this.showCart();
      },
      error => {
        console.log(error);
      });
  }



  public loadScript() {
    const node = document.createElement('script');
    node.src = 'assets/js/scripts.js';
    node.type = 'text/javascript';
    node.async = true;
    // tslint:disable-next-line: deprecation
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }


  showCart() {
    this.x = [];
    this.sub = 0;
    let product = JSON.parse(localStorage.getItem('cart'));
    if (product !== null) {
      product.forEach(element => {
        this.product.forEach(e => {
          if (e._id === element._id) {
            if (!element.size) {
              if (e.qty >= element.qty) {
                if (element.qty === 0) {
                  product = product.filter(item => item._id !== element._id);
                  this.sub -= element.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                } else {
                  this.x.push({
                    picUrl: e.picUrl,
                    _id: e._id,
                    size: '',
                    name: e.name,
                    price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                    qty: element.qty,
                    pqty: e.qty,
                    weight: e.weight
                  });
                }
              } else {
                if (e.qty === 0) {
                  product = product.filter(item => item._id !== element._id);
                  this.sub -= e.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                } else {
                  this.x.push({
                    picUrl: e.picUrl,
                    _id: e._id,
                    size: '',
                    name: e.name,
                    price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                    qty: e.qty,
                    pqty: e.qty,
                    weight: e.weight
                  });
                  element.qty = e.qty;
                }
              }
            } else {
              if (e.qty === 0 || element.qty === 0) {
                product = product.filter(item => item._id !== element._id);
                this.sub -= element.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
              } else {
                element.size.forEach(element1 => {
                  let val = 0;
                  e.sizeArrayQty.forEach(element2 => {
                    if (element1.size === element2.size) {
                      val = element2.qty;
                    }
                  });
                  if (val >= element1.qty) {
                    if (element1.qty === 0) {
                      element.size = element.size.filter(item => item.size !== element1.size);
                      this.sub -= element1.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                    } else {
                      this.x.push({
                        picUrl: e.picUrl,
                        _id: e._id,
                        size: element1.size,
                        name: e.name,
                        price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                        qty: element1.qty,
                        pqty: val,
                        weight: e.weight
                      });
                    }
                  } else {
                    if (val === 0) {
                      element.size = element.size.filter(item => item.size !== element1.size);
                      this.sub -= element1.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                    } else {
                      this.x.push({
                        picUrl: e.picUrl,
                        _id: e._id,
                        size: element1.size,
                        name: e.name,
                        price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                        qty: val,
                        pqty: val,
                        weight: e.weight
                      });
                      this.sub -= (element1.qty - val) * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                      element.qty += (val - element1.qty);
                      element1.qty = val;
                    }
                  }
                });
              }
            }
            localStorage.setItem('cart', JSON.stringify(product));
            this.sub += element.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
          }
        });
      });
    }
  }

  increaseValue(x, pqty, size) {
    const cart = JSON.parse(localStorage.getItem('cart'));

    cart.forEach(element => {
      if (element._id === x) {
        if (size === '') {
          if (pqty > element.qty) {
            element.qty++;
          } else {
            Swal.fire({
              title: 'Out Of Stock',
              text: 'You cant\'t add anymore!',
              icon: 'warning',
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Ok'
            });
          }
        } else {
          let val = 0;
          element.size.forEach(e6 => {
            if (e6.size === size) {
              val = e6.qty;
            }
          });
          
          if (pqty > val) {
            if (element.qty >= 1) {
              element.size.forEach(element1 => {
                if (element1.size === size) {
                  element1.qty++;
                }
              });
              element.qty++;
            }
          } else {
            Swal.fire({
              title: 'Out Of Stock',
              text: 'You cant\'t add anymore!',
              icon: 'warning',
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Ok'
            });
          }
        }
      }
    });
    localStorage.setItem('cart', JSON.stringify(cart));
    this.showCart();
    this.eventEmitterService.onFirstComponentButtonClick();
  }

  decreaseValue(x,size) {
    const cart = JSON.parse(localStorage.getItem('cart'));

    cart.forEach(element => {
      if (element._id === x) {
        if (size === ''){
          if (element.qty > 1) {
            element.qty--;
          }
        } else {
          if (element.qty > 1) {
            element.size.forEach(element1 => {
              if (element1.size === size && element1.qty > 1) {
                element1.qty--;
                element.qty--;
              }
            });
          }
        }
      }
    });
    localStorage.setItem('cart', JSON.stringify(cart));
    this.showCart();
    this.eventEmitterService.onFirstComponentButtonClick();
  }

  removeItem(id, size) {
    const newp = [];
    const product = JSON.parse(localStorage.getItem('cart'));
    product.forEach(element => {
      if (element._id !== id) {
        newp.push(element);
      } else {
        if (size !== ''){
          const newp1 = [];
          let pp;
          if (element.size.length !== 1){
            element.size.forEach(element1 => {
              if (element1.size !== size) {
                newp1.push(element1);
              } else {
                pp = element1;
              }
            });
            newp.push({ _id: element._id, qty: element.qty - pp.qty, size: newp1 });
          }
        }
      }
    });
    localStorage.setItem('cart', JSON.stringify(newp));
    this.showCart();
    this.eventEmitterService.onFirstComponentButtonClick();
  }

  toProduct(id) {
    this.router.navigate(['/shop']).then(() => {
      this.router.navigate(['/product'], { skipLocationChange: true, queryParams: { id } });
    });
  }

}




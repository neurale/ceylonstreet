import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/product.service';
import { EventEmitterService } from '../service/event-emitter.service';
import Swal from 'sweetalert2';
import { ValueService } from '../service/value.service';
declare var $: any;

@Component({
  selector: 'app-quick-view',
  templateUrl: './quick-view.component.html',
  styleUrls: ['./quick-view.component.scss']
})
export class QuickViewComponent implements OnInit, OnChanges {

  product = {
    birthstone: '',
    colors: '',
    cut: '',
    description: '',
    discountRate: '',
    gemstone: '',
    gemstoneCut: '',
    gemstoneWeight: '',
    itemCollection: '',
    itemBrand: '',
    measurement: '',
    metal: '',
    name: '',
    necklaceLength: '',
    picUrl: [''],
    productCostPrice: '',
    productPrice: '',
    productType: '',
    qty: '0',
    rodiumPlated: '',
    saleOfferPrice: '',
    species: '',
    size: '',
    sizeArray: [],
    sizeArrayQty: [],
    treatments: '',
    variety: '',
    weight: '',
    grams: '',
    subProductType: '',
    __v: 0,
    _id: '',
  };
  offer;
  qty = 1;
  count = 0;
  id;
  pqty = 0;
  size = '';
  pqty1 = 0;
  val = [];

  ngOnInit(): void {
  }

  constructor(
    private productService: ProductService,
    private eventEmitterService: EventEmitterService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    public valueService: ValueService
  ){
    this.activeRoute.queryParams
      .subscribe(params => {
        if (params.id !== null) {
          this.id = params.id;
          this.productService.getProductById(params.id).subscribe((data) => {
            this.product = data.data;
            if (this.product.productType === 'fashion') {
              let cart;
              let myMap = new Map();
              if (localStorage.getItem('cart')) {
                cart = JSON.parse(localStorage.getItem('cart'));
                cart.forEach(element => {
                  if (element._id === this.product._id) {
                    element.size.forEach(e1 => {
                      myMap.set(e1.size, e1.qty);
                    });
                  }
                });
                for (const element1 of this.product.sizeArray) {
                  for (const index of this.product.sizeArrayQty) {
                    if (element1 === index.size) {
                      const qty = myMap.get(element1) ?? 0;
                      if (index.qty > qty) {
                        this.count += myMap.get(element1) ?? 0;
                        this.val.push(index.size);
                      }
                    }
                  }
                }
                if (this.val === []) {
                  this.pqty = 0;
                } else {
                  this.pqty = +this.product.qty;
                }
              } else {
                for (const index of this.product.sizeArrayQty) {
                  if (index.qty > 0) {
                    this.val.push(index.size);
                  }
                }
                this.pqty = +this.product.qty;
              }
            } else {
              if (!localStorage.getItem('cart')) {
                this.count = 0;

              } else {
                let check = false;
                const cart = JSON.parse(localStorage.getItem('cart'));
                cart.forEach(element => {
                  if (element._id === this.product._id) {
                    this.count = element.qty;
                    check = true;
                  }
                });
                if (!check) {
                  this.count = 0;
                }
              }
              this.pqty = +this.product.qty;
            }
            this.pqty1 = +this.product.qty;
            this.offer = 100 - ((+this.product.saleOfferPrice) / (+this.product.productPrice)) * 100;
          });
          const newLocal = new Promise(() => {
            this.loadScript();
          });
        } else {
          this.router.navigate(['/shop']);
        }
      });
  }

  change(x) {

    return (+x) * (+this.valueService.getValue());
  }
  
  // public loadScript() {
  //   const node = document.createElement('script');
  //   node.src = 'assets/js/scripts.js';
  //   node.type = 'text/javascript';
  //   node.async = true;
  //   // tslint:disable-next-line: deprecation
  //   node.charset = 'utf-8';
  //   document.getElementsByTagName('head')[0].appendChild(node);
  //   const node1 = document.createElement('script');
  //   node1.src = 'assets/js/main.js';
  //   node1.type = 'text/javascript';
  //   node1.async = true;
  //   // tslint:disable-next-line: deprecation
  //   node1.charset = 'utf-8';
  //   document.getElementsByTagName('head')[0].appendChild(node1);
  // }

  public loadScript() {
    const node1 = document.createElement('script');
    node1.src = 'assets/js/main.js';
    node1.type = 'text/javascript';
    node1.async = true;
    // tslint:disable-next-line: deprecation
    node1.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node1);
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    // Add '${implements OnChanges}' to the class.
  }




  increaseValue() {
    // if (this.size !== '') {
    //   this.product.sizeArrayQty.forEach(element => {
    //     if (element.size === this.size) {
    //       this.pqty = element.qty;
    //     }
    //   });
    // }
    this.count = 0;
    if (this.product.productType === 'fashion') {
      let cart;
      const myMap = new Map();
      const myMap1 = new Map();
      if (localStorage.getItem('cart')) {
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === this.product._id) {
            element.size.forEach(e1 => {
              myMap.set(e1.size, e1.qty);
            });
          }
        });
        if (this.val === []) {
          this.pqty = 0;
        } else {
          if (this.size !== '') {
            for (const index of this.product.sizeArrayQty) {
              if (this.size === index.size) {
                const qty = myMap.get(this.size) ?? 0;
                if (index.qty > qty) {
                  this.count += myMap.get(this.size) ?? 0;
                  this.pqty = index.qty;
                }
              }
            }
          } else {
            for (const index of this.product.sizeArrayQty) {
              if (this.val[0] === index.size) {
                const qty = myMap.get(this.val[0]) ?? 0;
                if (index.qty > qty) {
                  this.count += myMap.get(this.val[0]) ?? 0;
                  this.pqty = index.qty;
                }
              }
            }

          }
        }
      } else {
        this.product.sizeArrayQty.forEach(e1 => {
          myMap1.set(e1.size, e1.qty);
        });
        if (this.size !== '') {
          this.count = 0;
          this.pqty = myMap1.get(this.size) ?? 0;
        } else {
          this.count = 0;
          this.pqty = myMap1.get(this.val[0]) ?? 0;
        }
      }
    }
    if (this.pqty > (this.qty + this.count)) {
      this.qty++;
    } else {
      Swal.fire({
        title: 'Out Of Stock',
        text: 'You cant\'t add anymore!',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok'
      });
    }
    this.eventEmitterService.onFirstComponentButtonClick();
  }

  decreaseValue() {
    if (this.qty > 1) {
      this.qty--;
    }

    this.eventEmitterService.onFirstComponentButtonClick();
  }




  addToCart(id) {
    if (this.size !== '') {
      this.product.sizeArrayQty.forEach(element => {
        if (element.size === this.size) {
          if (this.qty > element.qty) {
            this.qty = element.qty;
          }
        }
      });
    }
    let cart;
    if (this.product.productType === 'fashion') {
      if (!localStorage.getItem('cart')) {
        cart = [{ _id: id, qty: this.qty, size: [{ size: this.size, qty: this.qty }] }];

      } else {
        let check = false;
        let check1 = false;
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === id) {
            element.size.forEach(element1 => {
              if (element1.size === this.size) {
                element.qty = element.qty + this.qty;
                element1.qty = element1.qty + this.qty;
                check1 = true;
              }
            });
            if (!check1) {
              element.qty = element.qty + this.qty;
              element.size.push({ size: this.size, qty: this.qty });
            }
            check = true;
          }
        });
        if (!check) {
          cart.push({ _id: id, qty: this.qty, size: [{ size: this.size, qty: this.qty }] });
        }
      }
    } else {
      if (!localStorage.getItem('cart')) {
        cart = [{ _id: id, qty: this.qty }];

      } else {
        let check = false;
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === id) {
            element.qty = element.qty + this.qty;
            check = true;
          }
        });
        if (!check) {
          cart.push({ _id: id, qty: this.qty });
        }
      }
    }

    localStorage.setItem('cart', JSON.stringify(cart));
    this.eventEmitterService.onFirstComponentButtonClick();
    Swal.fire({
      title: 'Added',
      text: 'continue shopping',
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'continue!'
    }).then((result) => {
      if (result.value) {
        window.parent.parent.document.getElementById('fancybox-close').click();
      }
    });
  }

  disable() {
    return this.product.productType === 'fashion' ? this.size === '' ? false : true : true;
  }

}

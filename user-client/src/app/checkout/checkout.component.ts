import Swal from 'sweetalert2';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroupDirective, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { EventEmitterService } from '../service/event-emitter.service';
import { OrderService } from '../service/order.service';
import { ProductService } from '../service/product.service';

import io from 'socket.io-client';
import { ValueService } from '../service/value.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  countrys =
    [
      {
        name: 'Afghanistan',
        zone: 'zone7'
      },
      {
        name: 'Aland Island (Finland)',
        zone: 'zone4'
      },
      {
        name: 'Albania',
        zone: 'zone7'
      },
      {
        name: 'Algeria',
        zone: 'zone6'
      },
      {
        name: 'American Samoa',
        zone: 'zone2'
      },
      {
        name: 'Andorra',
        zone: 'zone4'
      },
      {
        name: 'Angola',
        zone: 'zone4'
      },
      {
        name: 'Anguilla',
        zone: 'zone7'
      },
      {
        name: 'Antigua and Barbuda',
        zone: 'zone7'
      },
      {
        name: 'Argentina',
        zone: 'zone7'
      },
      {
        name: 'Armenia',
        zone: 'zone7'
      },
      {
        name: 'Aruba',
        zone: 'zone7'
      },
      {
        name: 'Australia',
        zone: 'zone3'
      },
      {
        name: 'Austria',
        zone: 'zone4'
      },
      {
        name: 'Azerbaijan',
        zone: 'zone7'
      },
      {
        name: 'Azores (Portugal)',
        zone: 'zone4'
      },
      {
        name: 'Bahamas',
        zone: 'zone7'
      },
      {
        name: 'Bahrain',
        zone: 'zone2'
      },
      {
        name: 'Bangladesh',
        zone: 'zone1'
      },
      {
        name: 'Barbados',
        zone: 'zone7'
      },
      {
        name: 'Belarus/ Byelorussia',
        zone: 'zone7'
      },
      {
        name: 'Belgium',
        zone: 'zone4'
      },
      {
        name: 'Belize',
        zone: 'zone7'
      },
      {
        name: 'Benin',
        zone: 'zone7'
      },
      {
        name: 'Bermuda',
        zone: 'zone7'
      },
      {
        name: 'Bhutan',
        zone: 'zone6'
      },
      {
        name: 'Bolivia',
        zone: 'zone7'
      },
      {
        name: 'Bonaire, St. Eustatius, Saba',
        zone: 'zone7'
      },
      {
        name: 'Bosnia and Herzegovina',
        zone: 'zone7'
      },
      {
        name: 'Botswana',
        zone: 'zone6'
      },
      {
        name: 'Brazil',
        zone: 'zone7'
      },
      {
        name: 'British Virgin Islands',
        zone: 'zone7'
      },
      {
        name: 'Brunei',
        zone: 'zone1'
      },
      {
        name: 'Buesingen (Germany)',
        zone: 'zone4'
      },
      {
        name: 'Bulgaria',
        zone: 'zone7'
      },
      {
        name: 'Burkina Faso',
        zone: 'zone6'
      },
      {
        name: 'Burundi',
        zone: 'zone6'
      },
      {
        name: 'Cambodia',
        zone: 'zone7'
      },
      {
        name: 'Cameroon',
        zone: 'zone6'
      },
      {
        name: 'Campione/ Lake Lugano (Italy)',
        zone: 'zone4'
      },
      {
        name: 'Canada',
        zone: 'zone5'
      },
      {
        name: 'Canary Islands (Spain)',
        zone: 'zone7'
      },
      {
        name: 'Cape Verde',
        zone: 'zone7'
      },
      {
        name: 'Cayman Islands',
        zone: 'zone7'
      },
      {
        name: 'Central African Republic',
        zone: 'zone6'
      },
      {
        name: 'Ceuta (Spain)',
        zone: 'zone4'
      },
      {
        name: 'Chad',
        zone: 'zone6'
      },
      {
        name: 'Chile',
        zone: 'zone7'
      },
      {
        name: 'China, People\'s Republic of',
        zone: 'zone3'
      },
      {
        name: 'Colombia',
        zone: 'zone7'
      },
      {
        name: 'Comoros',
        zone: 'zone7'
      },
      {
        name: 'Congo (Brazzaville)',
        zone: 'zone6'
      },
      {
        name: 'Congo, Democratic Republic of',
        zone: 'zone6'
      },
      {
        name: 'Cook Islands',
        zone: 'zone2'
      },
      {
        name: 'Costa Rica',
        zone: 'zone7'
      },
      {
        name: 'Cote d\'Ivoire (Ivory Coast)',
        zone: 'zone7'
      },
      {
        name: 'Croatia',
        zone: 'zone7'
      },
      {
        name: 'Cuba',
        zone: 'zone7'
      },
      {
        name: 'Curacao',
        zone: 'zone7'
      },
      {
        name: 'Cyprus',
        zone: 'zone2'
      },
      {
        name: 'Czech Republic',
        zone: 'zone4'
      },
      {
        name: 'Denmark',
        zone: 'zone4'
      },
      {
        name: 'Djibouti',
        zone: 'zone6'
      },
      {
        name: 'Dominica',
        zone: 'zone7'
      },
      {
        name: 'Dominican Republic',
        zone: 'zone7'
      },
      {
        name: 'Ecuador',
        zone: 'zone7'
      },
      {
        name: 'Egypt',
        zone: 'zone2'
      },
      {
        name: 'El Salvador',
        zone: 'zone7'
      },
      {
        name: 'England (United Kingdom)',
        zone: 'zone3'
      },
      {
        name: 'Equatorial Guinea',
        zone: 'zone7'
      },
      {
        name: 'Eritrea',
        zone: 'zone7'
      },
      {
        name: 'Estonia',
        zone: 'zone7'
      },
      {
        name: 'Ethiopia',
        zone: 'zone6'
      },
      {
        name: 'Faroe Islands',
        zone: 'zone7'
      },
      {
        name: 'Fiji',
        zone: 'zone2'
      },
      {
        name: 'Finland',
        zone: 'zone4'
      },
      {
        name: 'France',
        zone: 'zone4'
      },
      {
        name: 'French Guiana',
        zone: 'zone7'
      },
      {
        name: 'French Polynesia',
        zone: 'zone2'
      },
      {
        name: 'Gabon',
        zone: 'zone6'
      },
      {
        name: 'Gambia',
        zone: 'zone6'
      },
      {
        name: 'Georgia',
        zone: 'zone7'
      },
      {
        name: 'Germany',
        zone: 'zone4'
      },
      {
        name: 'Ghana',
        zone: 'zone6'
      },
      {
        name: 'Gibraltar',
        zone: 'zone4'
      },
      {
        name: 'Greece',
        zone: 'zone4'
      },
      {
        name: 'Greenland',
        zone: 'zone7'
      },
      {
        name: 'Grenada',
        zone: 'zone7'
      },
      {
        name: 'Guadeloupe',
        zone: 'zone7'
      },
      {
        name: 'Guam',
        zone: 'zone2'
      },
      {
        name: 'Guatemala',
        zone: 'zone7'
      },
      {
        name: 'Guernsey (Channel Islands)',
        zone: 'zone4'
      },
      {
        name: 'Guinea',
        zone: 'zone6'
      },
      {
        name: 'Guinea-Bissau',
        zone: 'zone6'
      },
      {
        name: 'Guyana',
        zone: 'zone7'
      },
      {
        name: 'Haiti',
        zone: 'zone7'
      },
      {
        name: 'Heligoland (Germany)',
        zone: 'zone4'
      },
      {
        name: 'Honduras',
        zone: 'zone7'
      },
      {
        name: 'Hong Kong',
        zone: 'zone1'
      },
      {
        name: 'Hungary',
        zone: 'zone4'
      },
      {
        name: 'Iceland',
        zone: 'zone4'
      },
      {
        name: 'India',
        zone: 'zone1'
      },
      {
        name: 'Indonesia',
        zone: 'zone1'
      },
      {
        name: 'Iraq',
        zone: 'zone2'
      },
      {
        name: 'Ireland, Republic of',
        zone: 'zone4'
      },
      {
        name: 'Israel',
        zone: 'zone2'
      },
      {
        name: 'Italy',
        zone: 'zone4'
      },
      {
        name: 'Jamaica',
        zone: 'zone7'
      },
      {
        name: 'Japan',
        zone: 'zone3'
      },
      {
        name: 'Jersey (Channel Islands)',
        zone: 'zone4'
      },
      {
        name: 'Jordan',
        zone: 'zone2'
      },
      {
        name: 'Kazakhstan',
        zone: 'zone7'
      },
      {
        name: 'Kenya',
        zone: 'zone6'
      },
      {
        name: 'Kirghizia (Kyrgyzstan)',
        zone: 'zone7'
      },
      {
        name: 'Kiribati',
        zone: 'zone7'
      },
      {
        name: 'Korea, South',
        zone: 'zone3'
      },
      {
        name: 'Kosrae (Micronesia, Federated States of)',
        zone: 'zone2'
      },
      {
        name: 'Kuwait',
        zone: 'zone2'
      },
      {
        name: 'Laos',
        zone: 'zone7'
      },
      {
        name: 'Latvia',
        zone: 'zone7'
      },
      {
        name: 'Lebanon',
        zone: 'zone6'
      },
      {
        name: 'Lesotho',
        zone: 'zone6'
      },
      {
        name: 'Liberia',
        zone: 'zone6'
      },
      {
        name: 'Libyan Arab Jamahiriya',
        zone: 'zone6'
      },
      {
        name: 'Liechtenstein',
        zone: 'zone4'
      },
      {
        name: 'Lithuania',
        zone: 'zone7'
      },
      {
        name: 'Livigno (Italy)',
        zone: 'zone4'
      },
      {
        name: 'Luxembourg',
        zone: 'zone4'
      },
      {
        name: 'Macau',
        zone: 'zone1'
      },
      {
        name: 'Macedonia (FYROM)',
        zone: 'zone7'
      },
      {
        name: 'Madagascar',
        zone: 'zone6'
      },
      {
        name: 'Madeira (Portugal)',
        zone: 'zone4'
      },
      {
        name: 'Malawi',
        zone: 'zone6'
      },
      {
        name: 'Malaysia',
        zone: 'zone1'
      },
      {
        name: 'Maldives',
        zone: 'zone1'
      },
      {
        name: 'Mali',
        zone: 'zone6'
      },
      {
        name: 'Malta',
        zone: 'zone4'
      },
      {
        name: 'Marshall Islands',
        zone: 'zone7'
      },
      {
        name: 'Martinique',
        zone: 'zone7'
      },
      {
        name: 'Mauritania',
        zone: 'zone6'
      },
      {
        name: 'Mauritius',
        zone: 'zone6'
      },
      {
        name: 'Mayotte',
        zone: 'zone7'
      },
      {
        name: 'Melilla (Spain)',
        zone: 'zone4'
      },
      {
        name: 'Mexico',
        zone: 'zone5'
      },
      {
        name: 'Micronesia, Federated States of',
        zone: 'zone2'
      },
      {
        name: 'Moldova',
        zone: 'zone7'
      },
      {
        name: 'Monaco (France)',
        zone: 'zone4'
      },
      {
        name: 'Mongolia',
        zone: 'zone4'
      },
      {
        name: 'Montenegro',
        zone: 'zone7'
      },
      {
        name: 'Montserrat',
        zone: 'zone7'
      },
      {
        name: 'Morocco',
        zone: 'zone6'
      },
      {
        name: 'Mount Athos ( Greece)',
        zone: 'zone4'
      },
      {
        name: 'Mozambique',
        zone: 'zone6'
      },
      {
        name: 'Myanmar',
        zone: 'zone7'
      },
      {
        name: 'Nepal',
        zone: 'zone1'
      },
      {
        name: 'Netherlands (Holland)',
        zone: 'zone4'
      },
      {
        name: 'New Caledonia',
        zone: 'zone2'
      },
      {
        name: 'New Zealand',
        zone: 'zone1'
      },
      {
        name: 'Nicaragua',
        zone: 'zone7'
      },
      {
        name: 'Niger',
        zone: 'zone6'
      },
      {
        name: 'Nigeria',
        zone: 'zone6'
      },
      {
        name: 'Norfolk Island (Australia)',
        zone: 'zone7'
      },
      {
        name: 'Northern Ireland (United Kingdom)',
        zone: 'zone3'
      },
      {
        name: 'Northern Mariana Islands',
        zone: 'zone2'
      },
      {
        name: 'Norway',
        zone: 'zone4'
      },
      {
        name: 'Oman',
        zone: 'zone2'
      },
      {
        name: 'Pakistan',
        zone: 'zone3'
      },
      {
        name: 'Palau',
        zone: 'zone2'
      },
      {
        name: 'Panama',
        zone: 'zone7'
      },
      {
        name: 'Papua New Guinea',
        zone: 'zone2'
      },
      {
        name: 'Paraguay',
        zone: 'zone7'
      },
      {
        name: 'Peru',
        zone: 'zone7'
      },
      {
        name: 'Philippines',
        zone: 'zone1'
      },
      {
        name: 'Poland',
        zone: 'zone4'
      },
      {
        name: 'Ponape (Micronesia, Federated States of)',
        zone: 'zone2'
      },
      {
        name: 'Portugal',
        zone: 'zone4'
      },
      {
        name: 'Puerto Rico',
        zone: 'zone4'
      },
      {
        name: 'Qatar',
        zone: 'zone2'
      },
      {
        name: 'Reunion Island',
        zone: 'zone6'
      },
      {
        name: 'Romania',
        zone: 'zone7'
      },
      {
        name: 'Rota (Northern Mariana Islands)',
        zone: 'zone2'
      },
      {
        name: 'Russia',
        zone: 'zone7'
      },
      {
        name: 'Rwanda',
        zone: 'zone6'
      },
      {
        name: 'Saipan (Northern Mariana Islands)',
        zone: 'zone2'
      },
      {
        name: 'Samoa',
        zone: 'zone2'
      },
      {
        name: 'San Marino',
        zone: 'zone4'
      },
      {
        name: 'Saudi Arabia',
        zone: 'zone2'
      },
      {
        name: 'Scotland (United Kingdom)',
        zone: 'zone3'
      },
      {
        name: 'Senegal',
        zone: 'zone6'
      },
      {
        name: 'Serbia',
        zone: 'zone7'
      },
      {
        name: 'Seychelles',
        zone: 'zone7'
      },
      {
        name: 'Sierra Leone',
        zone: 'zone6'
      },
      {
        name: 'Singapore',
        zone: 'zone1'
      },
      {
        name: 'Slovakia',
        zone: 'zone7'
      },
      {
        name: 'Slovenia',
        zone: 'zone7'
      },
      {
        name: 'Solomon Islands',
        zone: 'zone2'
      },
      {
        name: 'South Africa',
        zone: 'zone6'
      },
      {
        name: 'Spain',
        zone: 'zone4'
      },
      {
        name: 'Sri Lanka',
        zone: 'zone8'
      },
      {
        name: 'St. Barthelemy',
        zone: 'zone7'
      },
      {
        name: 'St. Christopher (St. Kitts)',
        zone: 'zone7'
      },
      {
        name: 'St. Croix (U.S. Virgin Islands)',
        zone: 'zone7'
      },
      {
        name: 'St. John (U.S. Virgin Islands)',
        zone: 'zone7'
      },
      {
        name: 'St. Kitts and Nevis',
        zone: 'zone7'
      },
      {
        name: 'St. Lucia',
        zone: 'zone7'
      },
      {
        name: 'St. Maarten, St. Martin',
        zone: 'zone7'
      },
      {
        name: 'St. Thomas (U.S. Virgin Islands)',
        zone: 'zone7'
      },
      {
        name: 'St. Vincent & the Grenadines',
        zone: 'zone7'
      },
      {
        name: 'Suriname',
        zone: 'zone7'
      },
      {
        name: 'Swaziland',
        zone: 'zone6'
      },
      {
        name: 'Sweden',
        zone: 'zone4'
      },
      {
        name: 'Switzerland',
        zone: 'zone4'
      },
      {
        name: 'Syrian Arab Republic',
        zone: 'zone7'
      },
      {
        name: 'Tahiti (French Polynesia)',
        zone: 'zone2'
      },
      {
        name: 'Taiwan',
        zone: 'zone3'
      },
      {
        name: 'Tajikistan',
        zone: 'zone7'
      },
      {
        name: 'Tanzania, United Republic of',
        zone: 'zone6'
      },
      {
        name: 'Thailand',
        zone: 'zone1'
      },
      {
        name: 'Timor-Leste',
        zone: 'zone2'
      },
      {
        name: 'Tinian (Northern Mariana Islands)',
        zone: 'zone2'
      },
      {
        name: 'Togo',
        zone: 'zone7'
      },
      {
        name: 'Tonga',
        zone: 'zone2'
      },
      {
        name: 'Tortola (British Virgin Islands)',
        zone: 'zone7'
      },
      {
        name: 'Trinidad & Tobago',
        zone: 'zone7'
      },
      {
        name: 'Truk (Micronesia, Federated States of)',
        zone: 'zone2'
      },
      {
        name: 'Tunisia',
        zone: 'zone6'
      },
      {
        name: 'Turkey',
        zone: 'zone4'
      },
      {
        name: 'Turks & Caicos Islands',
        zone: 'zone7'
      },
      {
        name: 'Tuvalu',
        zone: 'zone2'
      },
      {
        name: 'U.S. Virgin Islands',
        zone: 'zone7'
      },
      {
        name: 'Uganda',
        zone: 'zone6'
      },
      {
        name: 'Ukraine',
        zone: 'zone7'
      },
      {
        name: 'Union Islands (St. Vincent & the Grenadines)',
        zone: 'zone7'
      },
      {
        name: 'United Arab Emirates',
        zone: 'zone2'
      },
      {
        name: 'United Kingdom',
        zone: 'zone3'
      },
      {
        name: 'United States',
        zone: 'zone5'
      },
      {
        name: 'Uruguay',
        zone: 'zone7'
      },
      {
        name: 'Uzbekistan',
        zone: 'zone7'
      },
      {
        name: 'Vanuatu',
        zone: 'zone2'
      },
      {
        name: 'Vatican City (Italy)',
        zone: 'zone4'
      },
      {
        name: 'Venezuela',
        zone: 'zone7'
      },
      {
        name: 'Vietnam',
        zone: 'zone7'
      },
      {
        name: 'Virgin Gorda (British Virgin Islands)',
        zone: 'zone7'
      },
      {
        name: 'W allis & Futuna Islands',
        zone: 'zone2'
      },
      {
        name: 'Wales (United Kingdom)',
        zone: 'zone3'
      },
      {
        name: 'Yap (Micronesia, Federated States of)',
        zone: 'zone2'
      },
      {
        name: 'Yemen, Republic of',
        zone: 'zone6'
      },
      {
        name: 'Zambia',
        zone: 'zone6'
      },
      {
        name: 'Zimbabwe',
        zone: 'zone6'
      }
    ];

  prices =
    [
      {
        zone1: 14.04,
        zone2: 15.38,
        zone3: 16.07,
        zone4: 17.56,
        zone5: 18.27,
        zone6: 20.42,
        zone7: 25.61,
        Weight: 0.5
      },
      {
        zone1: 17.56,
        zone2: 20.42,
        zone3: 21.22,
        zone4: 22.63,
        zone5: 23.4,
        zone6: 26.29,
        zone7: 32.88,
        Weight: 1
      },
      {
        zone1: 21.94,
        zone2: 25.61,
        zone3: 26.29,
        zone4: 28.47,
        zone5: 28.47,
        zone6: 32.88,
        zone7: 40.1,
        Weight: 1.5
      },
      {
        zone1: 26.29,
        zone2: 30.71,
        zone3: 31.42,
        zone4: 34.29,
        zone5: 33.6,
        zone6: 39.38,
        zone7: 47.43,
        Weight: 2
      },
      {
        zone1: 30.71,
        zone2: 35.77,
        zone3: 36.55,
        zone4: 40.1,
        zone5: 38.7,
        zone6: 45.97,
        zone7: 54.74,
        Weight: 2.5
      },
      {
        zone1: 35.06,
        zone2: 40.93,
        zone3: 41.65,
        zone4: 45.26,
        zone5: 43.11,
        zone6: 52.56,
        zone7: 62.01,
        Weight: 3
      },
      {
        zone1: 39.38,
        zone2: 45.26,
        zone3: 45.97,
        zone4: 50.38,
        zone5: 47.43,
        zone6: 57.6,
        zone7: 67.86,
        Weight: 3.5
      },
      {
        zone1: 42.33,
        zone2: 49.64,
        zone3: 50.38,
        zone4: 55.42,
        zone5: 51.81,
        zone6: 62.79,
        zone7: 73.64,
        Weight: 4
      },
      {
        zone1: 45.26,
        zone2: 53.96,
        zone3: 54.74,
        zone4: 59.09,
        zone5: 56.11,
        zone6: 67.86,
        zone7: 79.48,
        Weight: 4.5
      },
      {
        zone1: 48.15,
        zone2: 58.4,
        zone3: 59.09,
        zone4: 62.79,
        zone5: 60.58,
        zone6: 72.95,
        zone7: 85.33,
        Weight: 5
      },
      {
        zone1: 67.35,
        zone2: 78.14,
        zone3: 76.42,
        zone4: 66.23,
        zone5: 77.43,
        zone6: 105.97,
        zone7: 108.78,
        Weight: 5.5
      },
      {
        zone1: 71.23,
        zone2: 82.83,
        zone3: 80.57,
        zone4: 69.43,
        zone5: 82,
        zone6: 111.41,
        zone7: 114.15,
        Weight: 6
      },
      {
        zone1: 73.54,
        zone2: 86.67,
        zone3: 84.05,
        zone4: 72.09,
        zone5: 85.9,
        zone6: 116.01,
        zone7: 118.82,
        Weight: 6.5
      },
      {
        zone1: 75.86,
        zone2: 90.52,
        zone3: 87.53,
        zone4: 74.8,
        zone5: 89.75,
        zone6: 120.63,
        zone7: 123.46,
        Weight: 7
      },
      {
        zone1: 78.14,
        zone2: 94.4,
        zone3: 90.91,
        zone4: 77.44,
        zone5: 93.6,
        zone6: 125.38,
        zone7: 128.18,
        Weight: 7.5
      },
      {
        zone1: 80.46,
        zone2: 97.46,
        zone3: 94.41,
        zone4: 80.16,
        zone5: 97.53,
        zone6: 129.14,
        zone7: 132.85,
        Weight: 8
      },
      {
        zone1: 82.83,
        zone2: 100.62,
        zone3: 97.92,
        zone4: 82.77,
        zone5: 101.35,
        zone6: 132.99,
        zone7: 137.5,
        Weight: 8.5
      },
      {
        zone1: 85.08,
        zone2: 104.46,
        zone3: 101.35,
        zone4: 85.43,
        zone5: 105.28,
        zone6: 137.64,
        zone7: 142.16,
        Weight: 9
      },
      {
        zone1: 87.4,
        zone2: 108.31,
        zone3: 104.85,
        zone4: 88.13,
        zone5: 109.07,
        zone6: 142.39,
        zone7: 146.81,
        Weight: 9.5
      },
      {
        zone1: 89.77,
        zone2: 112.13,
        zone3: 108.3,
        zone4: 90.82,
        zone5: 112.92,
        zone6: 146.96,
        zone7: 151.53,
        Weight: 10
      },
      {
        zone1: 115.33,
        zone2: 143.98,
        zone3: 149.5,
        zone4: 116.12,
        zone5: 146.28,
        zone6: 190.19,
        zone7: 205.26,
        Weight: 10.5
      },
      {
        zone1: 118.16,
        zone2: 147.78,
        zone3: 153.31,
        zone4: 118.82,
        zone5: 150.08,
        zone6: 194.99,
        zone7: 210.56,
        Weight: 11
      },
      {
        zone1: 121.02,
        zone2: 150.64,
        zone3: 156.07,
        zone4: 121.49,
        zone5: 153.91,
        zone6: 199.68,
        zone7: 215.9,
        Weight: 11.5
      },
      {
        zone1: 123.85,
        zone2: 153.54,
        zone3: 158.94,
        zone4: 124.16,
        zone5: 158.64,
        zone6: 204.52,
        zone7: 220.32,
        Weight: 12
      },
      {
        zone1: 126.75,
        zone2: 156.52,
        zone3: 161.7,
        zone4: 126.78,
        zone5: 163.47,
        zone6: 209.28,
        zone7: 224.64,
        Weight: 12.5
      },
      {
        zone1: 129.66,
        zone2: 160.28,
        zone3: 165.37,
        zone4: 129.51,
        zone5: 167.3,
        zone6: 214.12,
        zone7: 230.01,
        Weight: 13
      },
      {
        zone1: 132.49,
        zone2: 164.04,
        zone3: 169.14,
        zone4: 132.18,
        zone5: 171.07,
        zone6: 218.92,
        zone7: 235.21,
        Weight: 13.5
      },
      {
        zone1: 135.39,
        zone2: 167.01,
        zone3: 172.85,
        zone4: 134.85,
        zone5: 175.9,
        zone6: 223.72,
        zone7: 240.55,
        Weight: 14
      },
      {
        zone1: 138.32,
        zone2: 169.84,
        zone3: 175.68,
        zone4: 137.5,
        zone5: 180.7,
        zone6: 228.48,
        zone7: 245.76,
        Weight: 14.5
      },
      {
        zone1: 141.12,
        zone2: 172.7,
        zone3: 178.58,
        zone4: 140.19,
        zone5: 185.5,
        zone6: 233.21,
        zone7: 251.03,
        Weight: 15
      },
      {
        zone1: 143.98,
        zone2: 176.47,
        zone3: 182.22,
        zone4: 143.51,
        zone5: 189.26,
        zone6: 238.04,
        zone7: 256.37,
        Weight: 15.5
      },
      {
        zone1: 146.88,
        zone2: 180.37,
        zone3: 185.99,
        zone4: 146.81,
        zone5: 193.13,
        zone6: 242.8,
        zone7: 261.61,
        Weight: 16
      },
      {
        zone1: 149.75,
        zone2: 183.23,
        zone3: 188.79,
        zone4: 149.53,
        zone5: 196.96,
        zone6: 247.6,
        zone7: 266.92,
        Weight: 16.5
      },
      {
        zone1: 152.61,
        zone2: 186.1,
        zone3: 192.49,
        zone4: 152.18,
        zone5: 201.72,
        zone6: 252.37,
        zone7: 272.23,
        Weight: 17
      },
      {
        zone1: 155.48,
        zone2: 189,
        zone3: 196.27,
        zone4: 154.87,
        zone5: 206.56,
        zone6: 257.27,
        zone7: 277.53,
        Weight: 17.5
      },
      {
        zone1: 158.34,
        zone2: 192.83,
        zone3: 199.03,
        zone4: 157.47,
        zone5: 210.32,
        zone6: 261.93,
        zone7: 282.8,
        Weight: 18
      },
      {
        zone1: 161.17,
        zone2: 196.63,
        zone3: 201.83,
        zone4: 160.19,
        zone5: 214.19,
        zone6: 266.73,
        zone7: 288.11,
        Weight: 18.5
      },
      {
        zone1: 164.04,
        zone2: 199.46,
        zone3: 205.53,
        zone4: 162.81,
        zone5: 218.02,
        zone6: 271.53,
        zone7: 293.32,
        Weight: 19
      },
      {
        zone1: 167.01,
        zone2: 202.36,
        zone3: 208.43,
        zone4: 165.56,
        zone5: 222.75,
        zone6: 276.26,
        zone7: 298.62,
        Weight: 19.5
      },
      {
        zone1: 169.84,
        zone2: 205.19,
        zone3: 211.23,
        zone4: 168.21,
        zone5: 227.58,
        zone6: 281.05,
        zone7: 303.96,
        Weight: 20
      },
      {
        zone1: 205.8,
        zone2: 251.63,
        zone3: 255.02,
        zone4: 204.42,
        zone5: 282.56,
        zone6: 341.86,
        zone7: 343.1,
        Weight: 21
      },
      {
        zone1: 211.28,
        zone2: 260.68,
        zone3: 262.71,
        zone4: 212.12,
        zone5: 291.66,
        zone6: 353.17,
        zone7: 354.61,
        Weight: 22
      },
      {
        zone1: 217.88,
        zone2: 267.64,
        zone3: 270.33,
        zone4: 219.88,
        zone5: 300.88,
        zone6: 364.66,
        zone7: 366.11,
        Weight: 23
      },
      {
        zone1: 224.43,
        zone2: 274.52,
        zone3: 278.03,
        zone4: 226.02,
        zone5: 309.93,
        zone6: 376.02,
        zone7: 377.65,
        Weight: 24
      },
      {
        zone1: 230.98,
        zone2: 281.3,
        zone3: 285.77,
        zone4: 232.28,
        zone5: 319.07,
        zone6: 387.33,
        zone7: 389.19,
        Weight: 25
      },
      {
        zone1: 237.53,
        zone2: 289.38,
        zone3: 293.34,
        zone4: 238.48,
        zone5: 329.32,
        zone6: 398.86,
        zone7: 399.76,
        Weight: 26
      },
      {
        zone1: 244.17,
        zone2: 297.28,
        zone3: 301.08,
        zone4: 244.67,
        zone5: 339.7,
        zone6: 410.26,
        zone7: 410.33,
        Weight: 27
      },
      {
        zone1: 250.68,
        zone2: 305.35,
        zone3: 308.66,
        zone4: 250.81,
        zone5: 348.79,
        zone6: 421.58,
        zone7: 421.8,
        Weight: 28
      },
      {
        zone1: 257.23,
        zone2: 313.25,
        zone3: 316.31,
        zone4: 257.76,
        zone5: 357.93,
        zone6: 433.07,
        zone7: 433.38,
        Weight: 29
      },
      {
        zone1: 263.78,
        zone2: 321.23,
        zone3: 324.01,
        zone4: 264.8,
        zone5: 366.98,
        zone6: 444.47,
        zone7: 444.96,
        Weight: 30
      },
      {
        zone1: 270.33,
        zone2: 329.26,
        zone3: 331.63,
        zone4: 271,
        zone5: 377.27,
        zone6: 456,
        zone7: 456.5,
        Weight: 31
      },
      {
        zone1: 277,
        zone2: 337.2,
        zone3: 339.33,
        zone4: 277.11,
        zone5: 387.48,
        zone6: 467.27,
        zone7: 467.93,
        Weight: 32
      },
      {
        zone1: 283.56,
        zone2: 345.23,
        zone3: 346.94,
        zone4: 283.31,
        zone5: 396.7,
        zone6: 478.71,
        zone7: 479.54,
        Weight: 33
      },
      {
        zone1: 290.03,
        zone2: 353.17,
        zone3: 354.6,
        zone4: 290.32,
        zone5: 405.8,
        zone6: 490.16,
        zone7: 491.05,
        Weight: 34
      },
      {
        zone1: 296.58,
        zone2: 361.16,
        zone3: 362.26,
        zone4: 297.3,
        zone5: 414.94,
        zone6: 501.47,
        zone7: 502.63,
        Weight: 35
      },
      {
        zone1: 303.17,
        zone2: 369.14,
        zone3: 369.99,
        zone4: 303.44,
        zone5: 425.18,
        zone6: 511.81,
        zone7: 514.06,
        Weight: 36
      },
      {
        zone1: 309.8,
        zone2: 377.21,
        zone3: 377.65,
        zone4: 309.66,
        zone5: 435.52,
        zone6: 522.06,
        zone7: 525.64,
        Weight: 37
      },
      {
        zone1: 316.31,
        zone2: 385.2,
        zone3: 385.23,
        zone4: 315.86,
        zone5: 445.77,
        zone6: 532.31,
        zone7: 537.18,
        Weight: 38
      },
      {
        zone1: 322.91,
        zone2: 393.1,
        zone3: 392.92,
        zone4: 322.03,
        zone5: 454.91,
        zone6: 542.6,
        zone7: 548.65,
        Weight: 39
      },
      {
        zone1: 329.46,
        zone2: 398.97,
        zone3: 400.54,
        zone4: 328.2,
        zone5: 464,
        zone6: 552.89,
        zone7: 560.19,
        Weight: 40
      },
      {
        zone1: 391.65,
        zone2: 467.54,
        zone3: 477.94,
        zone4: 381.41,
        zone5: 543.34,
        zone6: 647.46,
        zone7: 635.25,
        Weight: 41
      },
      {
        zone1: 399.38,
        zone2: 476.74,
        zone3: 486.95,
        zone4: 389.39,
        zone5: 553.8,
        zone6: 660.46,
        zone7: 648.12,
        Weight: 42
      },
      {
        zone1: 407.2,
        zone2: 485.93,
        zone3: 495.87,
        zone4: 397.26,
        zone5: 565.64,
        zone6: 673.62,
        zone7: 660.82,
        Weight: 43
      },
      {
        zone1: 415.03,
        zone2: 495.03,
        zone3: 504.88,
        zone4: 405.14,
        zone5: 577.42,
        zone6: 686.72,
        zone7: 673.72,
        Weight: 44
      },
      {
        zone1: 422.9,
        zone2: 504.22,
        zone3: 513.75,
        zone4: 413.15,
        zone5: 589.11,
        zone6: 699.78,
        zone7: 686.55,
        Weight: 45
      },
      {
        zone1: 430.77,
        zone2: 513.27,
        zone3: 522.81,
        zone4: 420.17,
        zone5: 599.57,
        zone6: 712.84,
        zone7: 699.29,
        Weight: 46
      },
      {
        zone1: 438.65,
        zone2: 522.51,
        zone3: 531.77,
        zone4: 427.19,
        zone5: 610.09,
        zone6: 725.84,
        zone7: 712.11,
        Weight: 47
      },
      {
        zone1: 446.37,
        zone2: 531.65,
        zone3: 540.69,
        zone4: 434.24,
        zone5: 620.45,
        zone6: 739.05,
        zone7: 724.98,
        Weight: 48
      },
      {
        zone1: 454.25,
        zone2: 540.75,
        zone3: 549.75,
        zone4: 441.26,
        zone5: 630.97,
        zone6: 751.96,
        zone7: 737.8,
        Weight: 49
      },
      {
        zone1: 462.22,
        zone2: 549.99,
        zone3: 558.62,
        zone4: 448.32,
        zone5: 641.43,
        zone6: 765.01,
        zone7: 750.5,
        Weight: 50
      },
      {
        zone1: 468.62,
        zone2: 556.54,
        zone3: 566.38,
        zone4: 455.37,
        zone5: 651.9,
        zone6: 778.22,
        zone7: 762.29,
        Weight: 51
      },
      {
        zone1: 475.18,
        zone2: 562.9,
        zone3: 574.05,
        zone4: 462.45,
        zone5: 662.36,
        zone6: 791.13,
        zone7: 773.99,
        Weight: 52
      },
      {
        zone1: 482.95,
        zone2: 569.5,
        zone3: 581.72,
        zone4: 469.47,
        zone5: 672.83,
        zone6: 804.28,
        zone7: 785.78,
        Weight: 53
      },
      {
        zone1: 490.92,
        zone2: 576,
        zone3: 589.3,
        zone4: 476.53,
        zone5: 683.15,
        zone6: 817.39,
        zone7: 797.52,
        Weight: 54
      },
      {
        zone1: 498.75,
        zone2: 582.61,
        zone3: 597.06,
        zone4: 483.55,
        zone5: 693.56,
        zone6: 830.54,
        zone7: 809.19,
        Weight: 55
      },
      {
        zone1: 506.57,
        zone2: 591.85,
        zone3: 604.78,
        zone4: 490.6,
        zone5: 704.12,
        zone6: 842.18,
        zone7: 822.09,
        Weight: 56
      },
      {
        zone1: 514.35,
        zone2: 600.89,
        zone3: 612.45,
        zone4: 497.62,
        zone5: 714.54,
        zone6: 853.86,
        zone7: 834.87,
        Weight: 57
      },
      {
        zone1: 522.36,
        zone2: 610.09,
        zone3: 620.07,
        zone4: 504.74,
        zone5: 724.95,
        zone6: 865.7,
        zone7: 847.7,
        Weight: 58
      },
      {
        zone1: 530.14,
        zone2: 616.54,
        zone3: 627.79,
        zone4: 511.79,
        zone5: 735.42,
        zone6: 878.8,
        zone7: 860.48,
        Weight: 59
      },
      {
        zone1: 537.96,
        zone2: 623.05,
        zone3: 635.6,
        zone4: 518.81,
        zone5: 745.88,
        zone6: 891.81,
        zone7: 873.38,
        Weight: 60
      },
      {
        zone1: 545.69,
        zone2: 630.97,
        zone3: 643.18,
        zone4: 525.9,
        zone5: 756.4,
        zone6: 904.97,
        zone7: 886.09,
        Weight: 61
      },
      {
        zone1: 553.51,
        zone2: 638.84,
        zone3: 650.85,
        zone4: 532.88,
        zone5: 766.81,
        zone6: 917.92,
        zone7: 898.99,
        Weight: 62
      },
      {
        zone1: 561.49,
        zone2: 646.62,
        zone3: 658.52,
        zone4: 539.97,
        zone5: 777.28,
        zone6: 930.98,
        zone7: 911.73,
        Weight: 63
      },
      {
        zone1: 569.31,
        zone2: 654.39,
        zone3: 666.24,
        zone4: 547.02,
        zone5: 787.74,
        zone6: 944.09,
        zone7: 924.48,
        Weight: 64
      },
      {
        zone1: 577.08,
        zone2: 662.36,
        zone3: 673.95,
        zone4: 554.07,
        zone5: 798.11,
        zone6: 957.09,
        zone7: 937.38,
        Weight: 65
      },
      {
        zone1: 584.96,
        zone2: 670.19,
        zone3: 681.67,
        zone4: 561.09,
        zone5: 808.62,
        zone6: 970.25,
        zone7: 950.16,
        Weight: 66
      },
      {
        zone1: 592.93,
        zone2: 677.96,
        zone3: 689.29,
        zone4: 568.15,
        zone5: 819.14,
        zone6: 983.3,
        zone7: 963.03,
        Weight: 67
      },
      {
        zone1: 600.65,
        zone2: 685.79,
        zone3: 696.96,
        zone4: 575.23,
        zone5: 829.6,
        zone6: 996.41,
        zone7: 975.77,
        Weight: 68
      },
      {
        zone1: 608.48,
        zone2: 693.56,
        zone3: 704.68,
        zone4: 582.25,
        zone5: 840.02,
        zone6: 1009.42,
        zone7: 988.67,
        Weight: 69
      },
      {
        zone1: 616.3,
        zone2: 701.58,
        zone3: 712.4,
        zone4: 589.27,
        zone5: 850.48,
        zone6: 1022.57,
        zone7: 1002.06,
        Weight: 70
      }
    ];

  town =
    [
      { town: 'Colombo 1 - 15', zone: 'zoneA' },
      { town: 'Katunayake', zone: 'zoneA' },
      { town: 'Kotugoda', zone: 'zoneA' },
      { town: 'Raddolugama', zone: 'zoneA' },
      { town: 'Seeduwa', zone: 'zoneA' },
      { town: 'Akarawita', zone: 'zoneB' },
      { town: 'Alubomulla', zone: 'zoneB' },
      { town: 'Andiambalama', zone: 'zoneB' },
      { town: 'Angoda', zone: 'zoneB' },
      { town: 'Athurugiriya', zone: 'zoneB' },
      { town: 'Attanagalla', zone: 'zoneB' },
      { town: 'Banduragoda', zone: 'zoneB' },
      { town: 'Battaramulla', zone: 'zoneB' },
      { town: 'Batuwatta', zone: 'zoneB' },
      { town: 'Bemmulla', zone: 'zoneB' },
      { town: 'Biyagama', zone: 'zoneB' },
      { town: 'Boralesgamuwa', zone: 'zoneB' },
      { town: 'Dagonna', zone: 'zoneB' },
      { town: 'Dehiwala', zone: 'zoneB' },
      { town: 'Dekatana', zone: 'zoneB' },
      { town: 'Delgoda', zone: 'zoneB' },
      { town: 'Divulapitiya', zone: 'zoneB' },
      { town: 'Dunagaha', zone: 'zoneB' },
      { town: 'Ekala', zone: 'zoneB' },
      { town: 'Embuldeniya', zone: 'zoneB' },
      { town: 'Gampaha', zone: 'zoneB' },
      { town: 'Ganemulla', zone: 'zoneB' },
      { town: 'GonawalaWP', zone: 'zoneB' },
      { town: 'Hendala', zone: 'zoneB' },
      { town: 'Hokandara', zone: 'zoneB' },
      { town: 'Homagama', zone: 'zoneB' },
      { town: 'Imbulgoda', zone: 'zoneB' },
      { town: 'Ja-Ela', zone: 'zoneB' },
      { town: 'Kadawatha', zone: 'zoneB' },
      { town: 'Kaduwela', zone: 'zoneB' },
      { town: 'Kalagedihena', zone: 'zoneB' },
      { town: 'Kalubowila', zone: 'zoneB' },
      { town: 'Kandana', zone: 'zoneB' },
      { town: 'Kapuwatta', zone: 'zoneB' },
      { town: 'Katana', zone: 'zoneB' },
      { town: 'Kelaniya', zone: 'zoneB' },
      { town: 'Kesbewa', zone: 'zoneB' },
      { town: 'Kimbulapitiya', zone: 'zoneB' },
      { town: 'Kiribathgoda', zone: 'zoneB' },
      { town: 'Kirindiwela', zone: 'zoneB' },
      { town: 'Kiriwattuduwa', zone: 'zoneB' },
      { town: 'Kochchikade', zone: 'zoneB' },
      { town: 'Kohuwala', zone: 'zoneB' },
      { town: 'Kolonnawa', zone: 'zoneB' },
      { town: 'Kotikawatta', zone: 'zoneB' },
      { town: 'Kottawa', zone: 'zoneB' },
      { town: 'Madapatha', zone: 'zoneB' },
      { town: 'Maharagama', zone: 'zoneB' },
      { town: 'Makola', zone: 'zoneB' },
      { town: 'Malabe', zone: 'zoneB' },
      { town: 'Malwana', zone: 'zoneB' },
      { town: 'Meegoda', zone: 'zoneB' },
      { town: 'Minuwangoda', zone: 'zoneB' },
      { town: 'Moratuwa', zone: 'zoneB' },
      { town: 'Mount Lavinia', zone: 'zoneB' },
      { town: 'Mudungoda', zone: 'zoneB' },
      { town: 'Mulleriyawa', zone: 'zoneB' },
      { town: 'Negombo', zone: 'zoneB' },
      { town: 'Nittambuwa', zone: 'zoneB' },
      { town: 'Niwandama', zone: 'zoneB' },
      { town: 'Nugegoda', zone: 'zoneB' },
      { town: 'Padukka', zone: 'zoneB' },
      { town: 'Pamunugama', zone: 'zoneB' },
      { town: 'Panadura', zone: 'zoneB' },
      { town: 'Pannipitiya', zone: 'zoneB' },
      { town: 'Peliyagoda', zone: 'zoneB' },
      { town: 'Pethiyagoda', zone: 'zoneB' },
      { town: 'Piliyandala', zone: 'zoneB' },
      { town: 'Pita Kotte', zone: 'zoneB' },
      { town: 'Pitipana Homagama', zone: 'zoneB' },
      { town: 'Polgasowita', zone: 'zoneB' },
      { town: 'Ragama', zone: 'zoneB' },
      { town: 'Rajagiriya', zone: 'zoneB' },
      { town: 'Ranala', zone: 'zoneB' },
      { town: 'Ratmalana', zone: 'zoneB' },
      { town: 'Rukmale', zone: 'zoneB' },
      { town: 'Siyambalape', zone: 'zoneB' },
      { town: 'Sri Jayewardenepura', zone: 'zoneB' },
      { town: 'Talahena', zone: 'zoneB' },
      { town: 'Talawatugoda', zone: 'zoneB' },
      { town: 'Thimbirigaskatuwa', zone: 'zoneB' },
      { town: 'Udugampola', zone: 'zoneB' },
      { town: 'Uswetakeiyawa', zone: 'zoneB' },
      { town: 'Wattala', zone: 'zoneB' },
      { town: 'Weboda', zone: 'zoneB' },
      { town: 'Wegowwa', zone: 'zoneB' },
      { town: 'Weliveriya', zone: 'zoneB' },
      { town: 'Wellampitiya', zone: 'zoneB' },
      { town: 'Wijerama', zone: 'zoneB' },
      { town: 'Yakkala', zone: 'zoneB' },
      { town: 'Akaragama', zone: 'zoneC' },
      { town: 'Akmeemana', zone: 'zoneC' },
      { town: 'Alawala', zone: 'zoneC' },
      { town: 'Alutgama', zone: 'zoneC' },
      { town: 'Ambagaspitiya', zone: 'zoneC' },
      { town: 'Ambatenna', zone: 'zoneC' },
      { town: 'Ambepussa', zone: 'zoneC' },
      { town: 'Ampara', zone: 'zoneC' },
      { town: 'Ampitiya', zone: 'zoneC' },
      { town: 'Anamaduwa', zone: 'zoneC' },
      { town: 'Anuradhapura', zone: 'zoneC' },
      { town: 'Arangala', zone: 'zoneC' },
      { town: 'Avissawella', zone: 'zoneC' },
      { town: 'Badalgama', zone: 'zoneC' },
      { town: 'Badulla', zone: 'zoneC' },
      { town: 'Balangoda', zone: 'zoneC' },
      { town: 'Bandaragama', zone: 'zoneC' },
      { town: 'Bandarawela', zone: 'zoneC' },
      { town: 'Batawala', zone: 'zoneC' },
      { town: 'Batticaloa', zone: 'zoneC' },
      { town: 'Batugampola', zone: 'zoneC' },
      { town: 'Beruwala', zone: 'zoneC' },
      { town: 'Bokalagama', zone: 'zoneC' },
      { town: 'Bope', zone: 'zoneC' },
      { town: 'Buthpitiya', zone: 'zoneC' },
      { town: 'Chilaw', zone: 'zoneC' },
      { town: 'Dambulla', zone: 'zoneC' },
      { town: 'Dankotuwa', zone: 'zoneC' },
      { town: 'Danowita', zone: 'zoneC' },
      { town: 'Dedigamuwa', zone: 'zoneC' },
      { town: 'Deltara', zone: 'zoneC' },
      { town: 'Demalagama', zone: 'zoneC' },
      { town: 'Demanhandiya', zone: 'zoneC' },
      { town: 'Dewalapola', zone: 'zoneC' },
      { town: 'Dharga Town', zone: 'zoneC' },
      { town: 'Divuldeniya', zone: 'zoneC' },
      { town: 'Dompe', zone: 'zoneC' },
      { town: 'Embilipitiya', zone: 'zoneC' },
      { town: 'Galle', zone: 'zoneC' },
      { town: 'Habarakada', zone: 'zoneC' },
      { town: 'Hambantota', zone: 'zoneC' },
      { town: 'Handapangoda', zone: 'zoneC' },
      { town: 'Hanwella', zone: 'zoneC' },
      { town: 'Hatton', zone: 'zoneC' },
      { town: 'Heiyanthuduwa', zone: 'zoneC' },
      { town: 'Henegama', zone: 'zoneC' },
      { town: 'Hewainna', zone: 'zoneC' },
      { town: 'Hinatiyana Madawala', zone: 'zoneC' },
      { town: 'Hiripitya', zone: 'zoneC' },
      { town: 'Hiswella', zone: 'zoneC' },
      { town: 'Horampella', zone: 'zoneC' },
      { town: 'Horana', zone: 'zoneC' },
      { town: 'Hunumulla', zone: 'zoneC' },
      { town: 'Ihala Madampella', zone: 'zoneC' },
      { town: 'Jaffna', zone: 'zoneC' },
      { town: 'Kahatowita', zone: 'zoneC' },
      { town: 'Kahawala', zone: 'zoneC' },
      { town: 'Kalatuwawa', zone: 'zoneC' },
      { town: 'Kaleliya', zone: 'zoneC' },
      { town: 'Kaluaggala', zone: 'zoneC' },
      { town: 'Kalutara', zone: 'zoneC' },
      { town: 'Kandy', zone: 'zoneC' },
      { town: 'Kapugoda', zone: 'zoneC' },
      { town: 'Katugastota', zone: 'zoneC' },
      { town: 'Katuneriya', zone: 'zoneC' },
      { town: 'Katuwellegama', zone: 'zoneC' },
      { town: 'Kegalle', zone: 'zoneC' },
      { town: 'Kitalawalana', zone: 'zoneC' },
      { town: 'Kitulwala', zone: 'zoneC' },
      { town: 'Kosgama', zone: 'zoneC' },
      { town: 'Kotadeniyawa', zone: 'zoneC' },
      { town: 'Kurunegala', zone: 'zoneC' },
      { town: 'Loluwagoda', zone: 'zoneC' },
      { town: 'Lunugama', zone: 'zoneC' },
      { town: 'Mabodale', zone: 'zoneC' },
      { town: 'Madelgamuwa', zone: 'zoneC' },
      { town: 'Maggona', zone: 'zoneC' },
      { town: 'Makewita', zone: 'zoneC' },
      { town: 'Mandawala', zone: 'zoneC' },
      { town: 'Marandagahamula', zone: 'zoneC' },
      { town: 'Matara', zone: 'zoneC' },
      { town: 'Matugama', zone: 'zoneC' },
      { town: 'Mellawagedara', zone: 'zoneC' },
      { town: 'Millewa', zone: 'zoneC' },
      { town: 'Mirigama', zone: 'zoneC' },
      { town: 'Mithirigala', zone: 'zoneC' },
      { town: 'Monaragala', zone: 'zoneC' },
      { town: 'Muddaragama', zone: 'zoneC' },
      { town: 'Mullegama', zone: 'zoneC' },
      { town: 'Napawela', zone: 'zoneC' },
      { town: 'Naranwala', zone: 'zoneC' },
      { town: 'Nawana', zone: 'zoneC' },
      { town: 'Nedungamuwa', zone: 'zoneC' },
      { town: 'Nikahetikanda', zone: 'zoneC' },
      { town: 'Nuwara Eliya', zone: 'zoneC' },
      { town: 'Paiyagala', zone: 'zoneC' },
      { town: 'Pallewela', zone: 'zoneC' },
      { town: 'Pamunuwatta', zone: 'zoneC' },
      { town: 'Paranthan', zone: 'zoneC' },
      { town: 'Pasyala', zone: 'zoneC' },
      { town: 'Pepiliyawala', zone: 'zoneC' },
      { town: 'Peradeniya', zone: 'zoneC' },
      { town: 'Polonnaruwa', zone: 'zoneC' },
      { town: 'Polpithimukulana', zone: 'zoneC' },
      { town: 'Pugoda', zone: 'zoneC' },
      { town: 'Puttalam', zone: 'zoneC' },
      { town: 'Puttalam Cement Factory', zone: 'zoneC' },
      { town: 'Puwakpitiya', zone: 'zoneC' },
      { town: 'Radawadunna', zone: 'zoneC' },
      { town: 'Radawana', zone: 'zoneC' },
      { town: 'Ratnapura', zone: 'zoneC' },
      { town: 'Ruggahawila', zone: 'zoneC' },
      { town: 'Siddamulla', zone: 'zoneC' },
      { town: 'Sigiriya', zone: 'zoneC' },
      { town: 'Tangalla', zone: 'zoneC' },
      { town: 'Tennekumbura', zone: 'zoneC' },
      { town: 'Tittapattara', zone: 'zoneC' },
      { town: 'Tummodara', zone: 'zoneC' },
      { town: 'Udathuthiripitiya', zone: 'zoneC' },
      { town: 'Uggalboda', zone: 'zoneC' },
      { town: 'Unawatuna', zone: 'zoneC' },
      { town: 'Urapola', zone: 'zoneC' },
      { town: 'Vavuniya', zone: 'zoneC' },
      { town: 'Veyangoda', zone: 'zoneC' },
      { town: 'Wadduwa', zone: 'zoneC' },
      { town: 'Waga', zone: 'zoneC' },
      { town: 'Wakwella', zone: 'zoneC' },
      { town: 'Walgammulla', zone: 'zoneC' },
      { town: 'Walpita', zone: 'zoneC' },
      { town: 'Wanaluwewa', zone: 'zoneC' },
      { town: 'Watareka', zone: 'zoneC' },
      { town: 'Wathurugama', zone: 'zoneC' },
      { town: 'Watinapaha', zone: 'zoneC' },
      { town: 'Weweldeniya', zone: 'zoneC' },
      { town: 'Other', zone: 'zoneD' },
    ];

  shipcheck  = false;
  shipcheck1 = false;
  srilanka = false;
  product = [];
  x = [];
  sub = 0;
  subWeight = 0;
  check = false;
  disable = (!(!localStorage.getItem('email') || localStorage.getItem('email') === '' || localStorage.getItem('email') === null));
  order = {
    name: localStorage.getItem('name') ?? '',
    email: localStorage.getItem('email') ?? '',
    phone: '',
    contry: '',
    address1: '',
    address2: '',
    town: '',
    state: '',
    postalcode: '',
    total: 0,
    fixtotal: 0,
    coupon: '',
    items: [],
    status: 'pending',
  };
  forder = {
    name: '',
    email: '',
    phone: '',
    contry: '',
    address: '',
    postalcode: '',
    total: 0,
    fixtotal: 0,
    coupon: '',
    items: [],
    status: '',
    shiping: 0,
    profit: 0,
    lkr: 0,
  };
  cost = 0;
  shiping = 0;
  emailpatern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
  socket;
  coupons = [];
  discount = 0;
  accept = false;
  oldCountry = '-1';
  oldPostalcode = '';

  @ViewChild('submitForm') documentEditForm: FormGroupDirective;

  checkout = new FormGroup({
    name: new FormControl(localStorage.getItem('name') ?? '', Validators.required),
    email: new FormControl(localStorage.getItem('email') ?? '', [Validators.required, Validators.pattern(this.emailpatern)]),
    phone: new FormControl('', [Validators.required]),
    address1: new FormControl('', Validators.required),
    address2: new FormControl(''),
    state: new FormControl(''),
    coupon: new FormControl(''),
    postalcode: new FormControl('', Validators.required),
  });

  constructor(
    private eventEmitterService: EventEmitterService,
    private productService: ProductService,
    private orderService: OrderService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    public valueService: ValueService
  ) {
    this.shipcheck = false;
    this.shipcheck1 = false;
    // resultIndicator=98f6e3a797f54532&sessionVersion=86e4b66606
    if (!this.isValid()) {
      this.router.navigate(['/login']);
    }
    this.activeRoute.queryParams.subscribe((params) => {
      if (params.resultIndicator) {
        this.orderService.valid({ _id: localStorage.getItem('orderId'), resultIndicator: params.resultIndicator}).subscribe(
          data => {
            // @ts-ignore
            if (data['success']){
              Swal.fire(
                'Success!',
                'Place Order Success!',
                'success'
              );
              localStorage.removeItem('cart');
              localStorage.removeItem('anonymous');
              localStorage.removeItem('orderId');
              this.order.coupon = '';
              this.router.navigate(['/orders']);
            } else {
              console.log(data);
              Swal.fire(
                'Invlid!',
                'Error in place order!',
                // @ts-ignore
                data['data']
              );
            }
          },
          error => {
            Swal.fire(
              'Invlid!',
              'Error in place order!',
              error
            );
          }
        );
      }
    });
    this.socket = io.connect('https://ceylonstreet.herokuapp.com/');
    const newLocal = new Promise(() => {
      this.loadScript();
    });
    this.activeRoute.queryParamMap.subscribe(paramMap => {
      const filterKeys = ['resultIndicator', 'sessionVersion'];
      if (paramMap.get('resultIndicator') !== null) {
        this.removeParamFromUrl(paramMap, filterKeys);
      }
      // this.removeParamFromUrl(paramMap, filterKeys);
    });
    $('html, body').animate({
      scrollTop: 0
    }, 600);
    this.showCart();
    this.productService.getCoupon().subscribe(
      data => {
        // @ts-ignore
        this.coupons = data['data'];
      }
    );
    $('html, body').animate({
      scrollTop: 0
    }, 600);
  }

  private removeParamFromUrl(paramMap: ParamMap, keysToRemove: string[]) {
    const queryParams = {};
    const keysToKeep = paramMap.keys.filter(k => !keysToRemove.includes(k));
    keysToKeep.forEach(k => (queryParams[k] = paramMap.get(k)));

    this.router.navigate([], { queryParams, replaceUrl: true, relativeTo: this.activeRoute });
  }

  getproduct() {
    this.product = [];
    this.productService.getProducts().subscribe(
        data => {
        // tslint:disable-next-line: no-string-literal
        // tslint:disable-next-line: no-string-literal
        data['data'].forEach(e => {
            if ((+e.qty) > 0) {
              this.product.push(e);
            }
          });
        this.showCart();
        },
        error => {
          console.log(error);
        });
  }

  ngOnInit(): void {

    this.call();
    this.cancelCallbackScript();
    this.errorCallbackScript();
    this.completeCallbackScript();

    this.socket.on('add-order', () => {
      setTimeout(() => {
        this.getproduct();
      }, 2000);
    });
    this.productService.getProducts().subscribe(
      data => {
        // tslint:disable-next-line: no-string-literal
        data['data'].forEach(e => {
          if ((+e.qty) > 0) {
            this.product.push(e);
          }
        });
        this.showCart();
      },
      error => {
        console.log(error);
      });
    if (this.eventEmitterService.subsVar === undefined) {
      this.eventEmitterService.
        invokeFirstComponentFunction.subscribe(() => {
          this.showCart();
        });
    }
    this.eventEmitterService.
      invokeFirstComponentFunction1.subscribe(() => {
        this.showCart();
      });
    document.getElementsByTagName('body')[0].setAttribute('style', 'overflow:visible');
  }

  public loadScript() {
    // const node = document.createElement('script');
    // node.src = 'assets/js/script.js';
    // node.type = 'text/javascript';
    // node.async = true;
    // // tslint:disable-next-line: deprecation
    // node.charset = 'utf-8';
    // document.getElementsByTagName('head')[0].appendChild(node);
  }

  showCart() {
    this.x = [];
    this.sub = 0;
    this.subWeight = 0;
    this.order.total = 0;
    this.order.fixtotal = 0;
    this.cost = 0;
    this.order.items = [];
    let product = JSON.parse(localStorage.getItem('cart'));
    if (product !== null) {
      product.forEach(element => {
        this.product.forEach(e => {
          // if (e._id === element._id) {
          //   if (e.qty >= element.qty) {
          //     this.x.push({
          //       picUrl: e.picUrl,
          //       _id: e._id,
          //       name: e.name,
          //       price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
          //       qty: element.qty
          //     });
          //     if (e.productType === 'fashion') {
          //       element.size.forEach( e4 => {
          //         e.sizeArrayQty.forEach(e3 => {
          //           if (e3.size === e4.size) {
          //             if (e3.qty <= e4.qty) {
          //               element.qty = element.qty - e4.qty + e3.qty;
          //               e4.qty = e3.qty;
          //             }
          //           }
          //         });
          //       });
          //       this.order.items.push({
          //         picUrl: e.picUrl[0],
          //         size: element.size ?? e.size,
          //         item_id: e._id,
          //         item_name: e.name,
          //         price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
          //         qty: element.qty
          //       });
          //     } else {
          //       this.order.items.push({
          //         picUrl: e.picUrl[0],
          //         item_id: e._id,
          //         item_name: e.name,
          //         price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
          //         qty: element.qty
          //       });
          //     }
          //   } else {
          //     this.x.push({
          //       picUrl: e.picUrl,
          //       _id: e._id,
          //       name: e.name,
          //       price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
          //       qty: e.qty
          //     });
          //     if (e.productType === 'fashion') {
          //       element.size.forEach(e4 => {
          //         e.sizeArrayQty.forEach(e3 => {
          //           if (e3.size === e4.size) {
          //             if (e3.qty <= e4.qty) {
          //               element.qty = element.qty - e4.qty + e3.qty;
          //               e4.qty = e3.qty;
          //             }
          //           }
          //         });
          //       });
          //       this.order.items.push({
          //         picUrl: e.picUrl[0],
          //         size: element.size ?? e.size,
          //         item_id: e._id,
          //         item_name: e.name,
          //         price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
          //         qty: element.qty
          //       });
          //     } else {
          //       this.order.items.push({
          //         picUrl: e.picUrl[0],
          //         item_id: e._id,
          //         item_name: e.name,
          //         price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
          //         qty: e.qty
          //       });
          //       element.qty = e.qty;
          //     }
          //   }
          //   localStorage.setItem('cart', JSON.stringify(product));
          //   // console.log(this.order.items)
          //   this.cost += (+e.productCostPrice) * element.qty ;
          //   this.subWeight += (+e.Weight) * element.qty ;
          //   this.sub += element.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
          //   this.order.total = this.sub;
          //   this.order.fixtotal = this.sub;
          // }





          if (e._id === element._id) {
            if (!element.size) {
              if (e.qty >= element.qty) {
                if (element.qty === 0) {
                  product = product.filter(item => item._id !== element._id);
                  this.sub -= element.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                } else {
                  this.x.push({
                    picUrl: e.picUrl,
                    _id: e._id,
                    size: '',
                    name: e.name,
                    price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                    qty: element.qty,
                    pqty: e.qty,
                    weight: e.weight
                  });
                  this.order.items.push({
                    picUrl: e.picUrl[0],
                    item_id: e._id,
                    item_name: e.name,
                    price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                    qty: e.qty
                  });
                }
              } else {
                if (e.qty === 0) {
                  product = product.filter(item => item._id !== element._id);
                  this.sub -= e.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                } else {
                  this.x.push({
                    picUrl: e.picUrl,
                    _id: e._id,
                    size: '',
                    name: e.name,
                    price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                    qty: e.qty,
                    pqty: e.qty,
                    weight: e.weight
                  });
                  this.order.items.push({
                    picUrl: e.picUrl[0],
                    item_id: e._id,
                    item_name: e.name,
                    price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                    qty: e.qty
                  });
                  element.qty = e.qty;
                }
              }
            } else {
              if (e.qty === 0 || element.qty === 0) {
                product = product.filter(item => item._id !== element._id);
                this.sub -= element.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
              } else {
                this.order.items.push({
                  picUrl: e.picUrl[0],
                  size: element.size ?? e.size,
                  item_id: e._id,
                  item_name: e.name,
                  price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                  qty: element.qty
                });
                element.size.forEach(element1 => {
                  let val = 0;
                  e.sizeArrayQty.forEach(element2 => {
                    if (element1.size === element2.size) {
                      val = element2.qty;
                    }
                  });
                  if (val >= element1.qty) {
                    if (element1.qty === 0) {
                      element.size = element.size.filter(item => item.size !== element1.size);
                      this.sub -= element1.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                    } else {
                      this.x.push({
                        picUrl: e.picUrl,
                        _id: e._id,
                        size: element1.size,
                        name: e.name,
                        price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                        qty: element1.qty,
                        pqty: val,
                        weight: e.weight
                      });
                    }
                  } else {
                    if (val === 0) {
                      element.size = element.size.filter(item => item.size !== element1.size);
                      this.sub -= element1.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                    } else {
                      this.x.push({
                        picUrl: e.picUrl,
                        _id: e._id,
                        size: element1.size,
                        name: e.name,
                        price: e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice,
                        qty: val,
                        pqty: val,
                        weight: e.weight
                      });
                      this.sub -= (element1.qty - val) * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
                      element.qty += (val - element1.qty);
                      element1.qty = val;
                    }
                  }
                });
              }
            }
            localStorage.setItem('cart', JSON.stringify(product));
            this.sub += element.qty * (e.saleOfferPrice === '0' ? e.productPrice : e.saleOfferPrice);
            this.cost += (+e.productCostPrice) * element.qty;
            this.subWeight += (+e.itemWeight) * element.qty ;
            this.order.total = this.sub;
            this.order.fixtotal = this.sub;
          }
        });

      });

    }

    // console.log(this.order.items);
  }

  roundHalf(num) {
    return Math.ceil(num * 2) / 2;
  }

  shippingPrice(x) {
    if (x !== '') {
      this.checkout.value.town = '';
      let weightClass: number;
      if (this.subWeight / 1000 <= 20) {
        weightClass = this.roundHalf(this.subWeight / 1000) * 2 - 1;
      } else {
        weightClass = Math.ceil(this.subWeight / 1000) + 39;
      }
      if (this.countrys[+x].name === 'Sri Lanka') {
        this.oldCountry = '198';
        this.srilanka = true;
        this.order.town = '';
      } else {
        // console.log(weightClass);
        if (this.srilanka === true) {
          this.order.town = '';
        }
        this.srilanka = false;
        this.shipcheck = false;
        this.shipcheck1 = true;
        // this.order.total = this.order.fixtotal + this.prices[weightClass][this.countrys[x].zone];
        this.shiping = this.prices[weightClass][this.countrys[+x].zone];

      }
    }
    // if (this.order.coupon !==  '') {
    //   this.order.total = this.order.fixtotal * (100 - (+this.order.coupon)) / 100 + this.shiping;
    // } else {
    //
    // }
  }

  async shippingLKPrice(i) {
    const x = this.town.findIndex((e) => e.town === i);
    if ( x !== -1) {
      this.order.town = i;
      const dr = await this.valueService.getValue();
      let val = 0;
      if (this.subWeight / 1000 <= 1) {
        val = this.town[x].zone === 'zoneA' ? 150 :
          this.town[x].zone === 'zoneB' ?
            200 :
            this.town[x].zone === 'zoneC' ? 275 : 350;
      } else {
        const num = Math.ceil(this.subWeight / 1000);
        val = this.town[x].zone === 'zoneA' ? (150 + 40 * (num - 1)) :
          this.town[x].zone === 'zoneB' ?
            (200 + 45 * (num - 1)) :
            this.town[x].zone === 'zoneC' ? (275 + 50 * (num - 1)) : (350 + 60 * (num - 1));
      }
      this.shiping = Math.ceil((1 / dr ) * val);
      this.shipcheck = false;
      this.shipcheck1 = true;
    }
  }


  submit() {
    // console.log(this.order);
    this.check = true;
    this.shippingPrice(this.order.contry);
    this.forder.name = this.order.name;
    this.forder.email = this.order.email;
    this.forder.phone = this.order.phone;
    this.forder.contry = this.countrys[this.order.contry].name;
    this.forder.address = this.order.address1 +
     ', ' + (this.order.address2 !== '' ? this.order.address2 + ', ' : '' ) +
      this.order.town + ', ' + (this.order.state !== '' ? this.order.state + ', ' : '')  + this.forder.contry +
      ', ' + this.order.postalcode;
    this.forder.postalcode = this.order.postalcode;
    this.forder.total = ((this.order.total) * (100 - (this.discount)) / 100) + this.shiping;
    this.forder.fixtotal = this.order.fixtotal;
    this.forder.coupon = '' + this.discount;
    this.forder.items = this.order.items;
    this.forder.status = this.order.status;
    this.forder.shiping = this.shiping;
    this.forder.profit = this.order.total - this.cost - ((this.order.total) * ((this.discount)) / 100);
    this.forder.lkr = (((this.order.total) * (100 - (this.discount)) / 100) + this.shiping) * (+this.valueService.getValue());
    // console.log(this.order);
    // console.log(this.forder);
    this.orderService.setorrder(this.forder).subscribe(
      data => {
      //   Swal.fire(
      //     'Success!',
      //     'Place Order Success!',
      //     'success'
      //   );
      //   localStorage.removeItem('cart');
      //   localStorage.removeItem('anonymous');
        this.order.coupon = '';
      //   this.router.navigate(['/orders']);
        localStorage.setItem('orderId', data['orderID']);
        this.pay(data['sesionID']);
      },
      error => {
        this.check = false;
        Swal.fire(
          'Invlid!',
          'Error in place order!',
          'error'
        );
      }
    );
  }

  isValid() {
    const token = localStorage.getItem('token');
    if (token) {
      const payload = this.payload(token);

      if (payload) {
        if (Date.now() >= payload.exp * 1000) {
          Swal.fire(
            'Time Out!',
            'Please login again!',
            'error'
          );
          localStorage.removeItem('name');
          localStorage.removeItem('pic');
          localStorage.removeItem('email');
          localStorage.removeItem('token');
          return false;
        }
        return (Date.now() <= payload.exp * 1000);
      }
    } else {
      const ll = localStorage.getItem('anonymous');
      return ll === 'true';
    }
  }

  payload(token) {
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload) {
    const base64 = payload.replace(/-/g, '+').replace(/_/g, '/');
    return JSON.parse(window.atob(base64));
  }

  toProduct(id) {
    this.router.navigate(['/shop']).then(() => {
      this.router.navigate(['/product'], { skipLocationChange: true, queryParams: { id } });
    });
  }

  call() {
    if (!window.document.getElementById('stripe-script')) {
      const s = window.document.createElement('script');
      s.id = 'stripe-script';
      s.type = 'text/javascript';
      s.src = 'https://cbcmpgs.gateway.mastercard.com/checkout/version/56/checkout.js';
      s.setAttribute('data-cancel', 'cancelCallback');
      s.setAttribute('data-error', 'errorCallback');
      s.setAttribute('data-complete', 'completeCallback');
      window.document.getElementsByTagName('head')[0].appendChild(s);
    }
  }

  setCoupns(val) {
    let found = false;
    this.coupons.forEach(e => {
      if (e.couponName === val) {
        if ((e.minLimit <= this.order.fixtotal) && (this.order.fixtotal <= e.maxLimit)) {
          this.order.coupon = val;
          // this.order.total = this.order.fixtotal * (100 - (+e.discount)) / 100;
          this.discount = (+e.discount);
          console.log(this.order.total);
          found = true;
          Swal.fire(
            'Valid!',
            'Coupon added for your order!',
            'success'
          );

        }
      }
    });
    if (found === false) {
      this.order.coupon = '';
      this.discount = 0;
      Swal.fire(
        'Invalid!',
        'Insert valid coupon again!',
        'error'
      );
    }
  }


  pay(id) {

    const handler = (window as any).Checkout.configure({
      session: {
        id
      },
      interaction: {
        // displayControl: {       // you may change these settings as you prefer
        //   // billingAddress: 'OPTIONAL',
        //   customerEmail: 'HIDE',
        //   orderSummary: 'SHOW',
        //   // shipping: 'HIDE'
        // }
        displayControl: {
            billingAddress  : 'HIDE',
            customerEmail: 'HIDE',
            orderSummary    : 'SHOW',
            shipping        : 'HIDE'
        }
      }
    });
    (window as any).Checkout.showLightbox();
    // resultIndicator=85abd9b16cdb45f3&sessionVersion=45ed5b2408
  }

  public cancelCallbackScript() {
    const body =  document.body as HTMLDivElement;
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.innerHTML = 'function cancelCallback() {' +
      ' console.log(\'Payment cancelled\');' +
      ' this.check = false;' +
      // ' alert(\'dddddddddddddd\'); '+
      '}';
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  public errorCallbackScript() {
    const body =  document.body as HTMLDivElement;
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.innerHTML = 'function errorCallback(error) {' +
      ' this.check = false;' +
      ' console.log(JSON.stringify(error));}';
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  public completeCallbackScript() {
    const body = document.body as HTMLDivElement;
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.innerHTML = 'function completeCallback(resultIndicator, sessionVersion) {' +
      ' console.log(\'resultIndicator: \' +resultIndicator);' +
      ' console.log(\'sessionVersion:\' +sessionVersion);' +
      ' this.check = false;' +
      ' Swal.fire({icon: \'success\',title: \'Pago exitoso!!!\',text: \'Puedes descargar tu pago en la sección de recibos\',showConfirmButton: false,timer: 1500});}';
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  validShip(x){
    if (this.oldCountry !== this.order.contry || this.oldPostalcode !== this.order.postalcode){
      let weightClass: number;
      if (this.subWeight / 1000 <= 20) {
        weightClass = this.roundHalf(this.subWeight / 1000) * 2 - 1;
      } else {
        weightClass = Math.ceil(this.subWeight / 1000) + 39;
      }
      // if (+this.order.contry === 198) {  this.oldCountry = '198'; }
      if (x.form.valid && this.shipcheck === false) {
        this.oldCountry = this.order.contry;
        console.log(this.order.contry);
        this.oldPostalcode = this.order.postalcode;
        this.shipcheck1 = false;
        this.shipcheck = true;
        if (this.countrys[this.order.contry].name === 'Sri Lanka') {
          if (!this.srilanka) {
            this.srilanka = true;
          } else {
            this.shipcheck = false;
            this.shipcheck1 = true;
            // this.order.total = this.order.fixtotal + this.shiping;
          }
        } else {
          if (this.countrys[this.order.contry].name !== 'Sri Lanka') {
            this.shipcheck = false;
            this.shipcheck1 = true;
            // this.order.total = this.order.fixtotal + this.prices[weightClass][this.countrys[this.order.contry].zone];
          }
        }

      }
    }
  }

}

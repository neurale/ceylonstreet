import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../service/product.service';
import { BlogService } from '../service/blog.service';
import { EventEmitterService } from '../service/event-emitter.service';
import { UiService } from '../service/ui.service';
import { CollectionService } from '../service/collection.service';

declare function InstagramFeed({}): void;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  collections;
  items = [];
  term = '';
  blogs;
  video = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: '',
    youtubeLink: '',
  };
  video1 = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: '',
    youtubeLink: '',
  };

  constructor(
    private prodService: ProductService,
    private blogService: BlogService,
    private eventEmitterService: EventEmitterService,
    private uiService: UiService,
    private router: Router
  ) {
    this.prodService.getProducts().subscribe((data) => {
      this.items = data['data'];
    });
    uiService.getUI().subscribe(
      (data) => {
        // tslint:disable-next-line: no-string-literal
        this.video = data['data'][0].videoBanner;
        this.video1 = data['data'][0].videoBannerMobile;
      },
      (error) => {
        // Do something with error
        console.error(error);
      }
    );
    blogService.getBlogs().subscribe(
      (data) => {
        // tslint:disable-next-line: no-string-literal
        this.blogs = data['data'];
      },
      (error) => {
        // Do something with error
        console.error(error);
      }
    );
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      600
    );
    // (function () {
    //   new InstagramFeed({
    //     'username': 'ceylon_st',
    //     'container': document.getElementById("instafeed"),
    //     'display_profile': false,
    //     'display_biography': false,
    //     'display_gallery': true,
    //     'display_captions': true,
    //     'max_tries': 3,
    //     'callback': null,
    //     'styling': true,
    //     'items': 3,
    //     'items_per_row': 3,
    //     'margin': 1,
    //     'lazy_load': true,
    //     'on_error': console.error
    //   });
    // })();
  }

  public loadScript() {
    const node1 = document.createElement('script');
    node1.src = 'assets/js/scripts.js';
    node1.type = 'text/javascript';
    node1.async = true;
    node1.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node1);
    const node = document.createElement('script');
    node.src = 'assets/js/main.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  ngOnInit(): void {
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      600
    );
    setTimeout(() => {
      new InstagramFeed({
        accessToken: 'your-token',
        username: 'ceylon_st',
        container: document.getElementById('instafeed'),
        display_profile: false,
        display_biography: false,
        display_gallery: true,
        display_captions: true,
        max_tries: 3,
        callback: null,
        styling: true,
        items: 3,
        items_per_row: 3,
        margin: 1,
        lazy_load: false,
        on_error: console.error
      });
    }, 3500);
    setTimeout(() => {
      $('.xxxxx').css({ 'padding-top': 0 + 'px' });
    }, 500);
    setTimeout(() => {
      document.getElementsByTagName('body')[0].setAttribute('style', 'overflow:visible');
    }, 500);

    setTimeout(() => {
      document.getElementById('loader').setAttribute('style', 'display:none');
      document.getElementsByClassName('loader')[0].setAttribute('style', 'display:none');
      $('html, body').animate(
        {
          scrollTop: 0,
        },
        200
      );
    }, 3100);
  }

  toBlog(blog) {
    this.router.navigate(['/blog']).then(() => {
      this.router.navigate(['/blog'], {
        skipLocationChange: true,
        queryParams: { content: blog.content },
      });
    });
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }
  isMobileMenu1() {
    if ($(window).width() > 1050) {
      return false;
    }
    return true;
  }
  toProduct(id) {
    this.router.navigate(['/shop']).then(() => {
      this.router.navigate(['/product'], {
        skipLocationChange: true,
        queryParams: { id },
      });
    });
  }
}

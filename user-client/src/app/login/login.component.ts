import { UserService } from './../service/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  response;
  user;
  loggedIn;
  service;

  constructor(
    private authService: SocialAuthService,
    private router: Router,
    private userService: UserService
  ) {
    const newLocal = new Promise(() => {
      this.loadScript();
    });
    if (localStorage.getItem('token')){
      this.router.navigate(['/orders']);
    }
    $('html, body').animate({
      scrollTop: 0
    }, 600);
    document.getElementsByTagName('body')[0].setAttribute('style', 'overflow:visible');
  }

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      if (user !== null) {
        this.user = user;
        this.loggedIn = (user != null);
        this.userService.signup({
          lastName: user.lastName,
          firstName: user.firstName,
          email: user.email,
          token: user.id,
          serive: this.service
        }).subscribe(
          data => {
            if (data.success){
              localStorage.setItem('email', user.email);
              localStorage.setItem('name', user.name);
              localStorage.setItem('pic', user.photoUrl);
              localStorage.setItem('token', data.userToken);
              const cart = localStorage.getItem('cart');
              if (JSON.parse(cart) === [] || !cart){
                this.router.navigate(['/orders']);
              } else {
                this.router.navigate(['/checkout']);
              }
            } else {
              this.signOut();
            }
          },
          error => {
            this.signOut();
            // console.log(error);
          }
        );
      }
    });
  }

  public loadScript() {
    const node = document.createElement('script');
    // node.src = 'assets/js/script.js';
    // node.type = 'text/javascript';
    // node.async = true;
    // node.charset = 'utf-8';
    // document.getElementsByTagName('head')[0].appendChild(node);

    node.src = 'assets/js/tilt.jquery.min.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }



  signInWithGoogle(): void {
    this.service = 'google'
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.service = 'FACEBOOK'
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  anonymous(){
    localStorage.setItem('anonymous', 'true');
    this.router.navigate(['/checkout']);
  }

  signOut(): void {
    this.authService.signOut();
  }

}

import { LookbookService } from './../service/lookbook.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, HostListener, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-look-book',
  templateUrl: './look-book.component.html',
  styleUrls: ['./look-book.component.scss']
})
export class LookBookComponent implements OnInit {

  revapi;
  lookbook;
  showP = true;
  previousScroll = 0;
  constructor(
    private lookBookService: LookbookService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activeRoute.queryParams
      .subscribe(params => {
        if (params.id !== null) {
          this.lookBookService.getLookBookById(params.id).subscribe((data) => {
            this.lookbook = data.data.sliders;
            setTimeout(() => {
            // this.loadScript();
            $('.tp-fullscreen1').revolution({
                delay: 250000,
                startwidth: 1170,
                startheight: 750,
                hideThumbs: 0,
                hideArrowsOnMobile: 'false',
                hideBulletsOnMobile: 'false',
                fullWidth: 'on',
                fullScreen: 'on',
                scrollOnMobile: 'true',
                soloArrowLeftHOffset: 0,
                soloArrowRightHOffset: 0,
              });
            }, 10);
            setTimeout(() => {
              document.getElementsByClassName('tparrows')[0].setAttribute('style', 'color: black; font-weight: bold; font-size: xxx-large; position: absolute; top: 209px; margin-top: -20px; left: 0px;');
              document.getElementsByClassName('tparrows')[1].setAttribute('style', 'color: black; font-weight: bold; font-size: xxx-large; position: absolute; top: 209px; margin-top: -20px; right: 0px;');
             
            }, 100);
            
          });
        } else {
          this.router.navigate(['/shop']);
        }
      });
    const newLocal = new Promise(() => {
      this.loadScript();
    });

    document.getElementsByTagName('body')[0].setAttribute('style', 'overflow:visible');
  }

  ngOnInit(): void {
    $('html, body').animate({
      scrollTop: 0
    }, 600);
    setTimeout(() => {
      const cw = $('.img-fluid').width();
      $('.img-fluid').css({ height: cw + 'px' });
    }, 400);
  }

  @HostListener('scroll', ['$event'])
  onScroll(event: any) {
    if (event.target.offsetHeight + event.target.scrollTop - 50 >= document.documentElement.offsetHeight) {
      // //Do your action here
      // alert('down');
      document.getElementsByClassName('tparrows')[0].setAttribute('style', 'display:none');
      document.getElementsByClassName('tparrows')[1].setAttribute('style', 'display:none');
    } else {
      document.getElementsByClassName('tparrows')[0].setAttribute('style', 'color: black; font-weight: bold; font-size: xxx-large; position: absolute; top: 209px; margin-top: -20px; left: 0px;');
      document.getElementsByClassName('tparrows')[1].setAttribute('style', 'color: black; font-weight: bold; font-size: xxx-large; position: absolute; top: 209px; margin-top: -20px; right: 0px;');

    }
  }

  public loadScript() {
    const node = document.createElement('script');
    node.src = 'assets/js/scripts.js';
    node.type = 'text/javascript';
    node.async = true;
    // tslint:disable-next-line: deprecation
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  show(){
    this.showP = !this.showP
  }

  externalFunction(e){
    // console.log(e);
  }

  call(i){
    this.router.navigate(['shop']).then(() => {
      this.router.navigate(['product'], { skipLocationChange: true, queryParams: { id: i.id } });
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-one-blog',
  templateUrl: './one-blog.component.html',
  styleUrls: ['./one-blog.component.scss']
})
export class OneBlogComponent implements OnInit {

  content;
  sub;
  constructor(private Activatedroute: ActivatedRoute,
              private router: Router){
    $('html, body').animate({
      scrollTop: 0
    }, 600);
  }

  ngOnInit(): void {
    this.sub = this.Activatedroute.queryParamMap
      .subscribe(params => {
        this.content = params.get('content');
      });
  }

}

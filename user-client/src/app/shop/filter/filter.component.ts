import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnInit,
  OnChanges,
} from '@angular/core';
import { BrandsService } from 'src/app/service/brands.service';
import { CollectionService } from 'src/app/service/collection.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  @Input() routerCollection;
  @Output() filter: EventEmitter<any> = new EventEmitter();
  allCollection = ([] = []);
  allBrands = ([] = []);
  fashion = false;
  accessories = false;
  homeAndLiving = false;
  kitchenware = false;
  art = false;
  bathAndBody = false;
  spice = false;
  jewelry = false;

  selectedProducts = new Set();
  selectedGenders = new Set();
  selectedCollections = new Set();
  selectedBrands = new Set();
  selectedSizes = new Set();

  man = false;
  woman = false;
  kid = false;

  xs = false;
  s = false;
  m = false;
  l = false;
  xl = false;
  xxl = false;

  ngOnInit() {
    // if (this.routerCollection != '') {
      // this.selectedCollections.add(this.routerCollection)
      // this.apply()
    // }
  }
  constructor(
    private collectionService: CollectionService,
    private brandCollection: BrandsService
  ) {
    this.collectionService.getCollection().subscribe((data) => {
      // tslint:disable-next-line: no-string-literal
      data['data'].forEach((element) => this.allCollection.push(element.name));
    });
    this.brandCollection.getBrands().subscribe((data) => {
      // tslint:disable-next-line: no-string-literal
      data['data'].forEach((element) => this.allBrands.push(element.name));
    });
  }

  productFilter() {
    if (this.fashion) {
      this.selectedProducts.add('fashion');
    } else {
      this.selectedProducts.delete('fashion');
    }

    if (this.accessories) {
      this.selectedProducts.add('accessories');
    } else {
      this.selectedProducts.delete('accessories');
    }
    if (this.homeAndLiving) {
      this.selectedProducts.add('homeAndLiving');
    } else {
      this.selectedProducts.delete('homeAndLiving');
    }
    if (this.kitchenware) {
      this.selectedProducts.add('kitchenware');
    } else {
      this.selectedProducts.delete('kitchenware');
    }
    if (this.art) {
      this.selectedProducts.add('art');
    } else {
      this.selectedProducts.delete('art');
    }
    if (this.bathAndBody) {
      this.selectedProducts.add('bathAndBody');
    } else {
      this.selectedProducts.delete('bathAndBody');
    }
    if (this.spice) {
      this.selectedProducts.add('spice');
    } else {
      this.selectedProducts.delete('spice');
    }
    if (this.jewelry) {
      this.selectedProducts.add('jewelry');
    } else {
      this.selectedProducts.delete('jewelry');
    }
    // console.log(this.selectedProducts);
    this.apply();
  }
  genderFilter() {
    if (this.man) {
      this.selectedGenders.add('man');
    } else {
      this.selectedGenders.delete('man');
    }

    if (this.woman) {
      this.selectedGenders.add('woman');
    } else {
      this.selectedGenders.delete('woman');
    }
    if (this.kid) {
      this.selectedGenders.add('kid');
    } else {
      this.selectedGenders.delete('kid');
    }

    // console.log(this.selectedGenders);
    this.apply();
  }

  collectionFilter(collection) {
    if (this.selectedCollections.has(collection)) {
      this.selectedCollections.delete(collection);
    } else {
      this.selectedCollections.add(collection);
    }
    // console.log(this.selectedCollections);
    this.apply();
  }
  brandFilter(brand) {
    if (this.selectedBrands.has(brand)) {
      this.selectedBrands.delete(brand);
    } else {
      this.selectedBrands.add(brand);
    }
    // console.log(this.selectedBrands);
    this.apply();
  }

  filterSize() {
    if (this.xs) {
      this.selectedSizes.add('xs');
    } else {
      this.selectedSizes.delete('xs');
    }
    if (this.s) {
      this.selectedSizes.add('s');
    } else {
      this.selectedSizes.delete('s');
    }
    if (this.m) {
      this.selectedSizes.add('m');
    } else {
      this.selectedSizes.delete('m');
    }
    this.apply();
  }

  apply() {
    const data = [
      this.selectedProducts,
      this.selectedCollections,
      this.selectedGenders,
      this.selectedBrands,
      this.selectedSizes,
    ];


    this.filter.emit(data);
  }

  clear(){
    const data = [[], [], [], [], []];
    this.filter.emit(data);
  }
}

// export class FilterComponent implements OnChanges {
//   @Output() filter: EventEmitter<any> = new EventEmitter();
//   @Input() products = ([] = []);

//   //dynamically view brnads and collections in filter component
//   brands = new Set();

//   collections = new Set();

//   constructor(private brandService: BrandsService) {}

//   ngOnChanges(changes: import('@angular/core').SimpleChanges) {
//     this.brands.clear();
//     this.collections.clear();

//     // if (this.productArray.length > 0) {
//     this.products.forEach((element) => {
//       this.brands.add(element.itemBrand);
//     });
//     // } else {
//     // this.brandService.getBrands().subscribe((data) => {
//     //   data['data'].forEach((element) => {
//     //   });
//     // });
//     // }

//     this.products.forEach((element) => {
//       this.collections.add(element.itemCollection);
//     });

//     // console.log(this.brands);
//   }

//   fashion = false;
//   accessories = false;
//   homeAndLiving = false;
//   kitchenware = false;
//   art = false;
//   bathAndBody = false;
//   spice = false;
//   jewelry = false;

//   productArray = ([] = []);
//   categoryArray = ([] = []);
//   collectionArray = new Set();
//   brandArray = new Set();

//   addSelectedCollection(collection) {
//     if (this.collectionArray.has(collection)) {
//       this.collectionArray.delete(collection);
//     } else {
//       this.collectionArray.add(collection);
//     }

//     this.callParentFilter();
//   }

//   addSelectedBrand(brand) {
//     if (this.brandArray.has(brand)) {
//       this.brandArray.delete(brand);
//     } else {
//       this.brandArray.add(brand);
//     }

//     this.callParentFilter();
//   }

//   callParentFilter(): void {
//     console.log('parent');
//     this.productArray = [];

//     if (this.fashion) {
//       this.productArray.push('fashion');
//     } else {
//       this.man = false;
//       this.woman = false;
//       this.kid = false;
//       this.categoryArray = [];
//     }
//     if (this.accessories) this.productArray.push('accessories');
//     if (this.homeAndLiving) this.productArray.push('homeAndLiving');
//     if (this.kitchenware) this.productArray.push('kitchenware');
//     if (this.art) this.productArray.push('art');
//     if (this.bathAndBody) this.productArray.push('bathAndBody');
//     if (this.spice) this.productArray.push('spice');
//     if (this.jewelry) this.productArray.push('jewelry');

//     const metaDataArray = [
//       this.productArray,
//       this.collectionArray,
//       this.categoryArray,
//       this.brandArray,
//       this.brands,
//       this.collections,
//     ];
//     this.clear();

//     // console.log(this.productArray);
//     // console.log(this.collectionArray);
//     // console.log(metaDataArray);
//     this.filter.emit(metaDataArray);
//     // this.filter.next('somePhone');
//   }

//   clear() {
//     this.collectionArray.forEach((element) => {
//       if (!this.collections.has(element)) {
//         // console.log(element);
//         this.collectionArray.delete(element);
//       }
//     });
//     this.brandArray.forEach((element) => {
//       if (!this.brands.has(element)) {
//         this.brandArray.delete(element);
//       }
//     });
//   }

//   man = false;
//   woman = false;
//   kid = false;
//   callCategoryFilter() {
//     this.categoryArray = [];

//     if (this.man) this.categoryArray.push('man');
//     if (this.woman) this.categoryArray.push('woman');
//     if (this.kid) this.categoryArray.push('kid');

//     this.callParentFilter();
//     // console.log(this.categoryArray);
//   }
// }

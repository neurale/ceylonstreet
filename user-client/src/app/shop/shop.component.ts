import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { EventEmitterService } from '../service/event-emitter.service';
import { CollectionService } from '../service/collection.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  socket;
  collection = [];
  jewellery = false;
  colors = false;
  meteltype = false;
  birthstone = false;
  gemstone = false;
  qty = 1;
  selectProduct = {
    birthstone: '',
    colors: '',
    cut: '',
    description: '',
    discountRate: '',
    gemstone: '',
    gemstoneCut: '',
    gemstoneWeight: '',
    itemCollection: '',
    measurement: '',
    metal: '',
    name: '',
    necklaceLength: '',
    picUrl: [''],
    productCostPrice: '',
    productPrice: '',
    productType: '',
    qty: '',
    rodiumPlated: '',
    saleOfferPrice: '',
    species: '',
    treatments: '',
    variety: '',
    weight: '',
    __v: 0,
    _id: '',
  };

  // collection
  selectCol = [];
  page = 0;

  products = ([] = []);
  vproducts = [];
  filteredArray: any[] = [];

  tabId = 4;
  active: any;
  greenQuartz: boolean;

  sidebarToggler;
  body;
  routerCollection = "";
  constructor(
    private productService: ProductService,
    private eventEmitterService: EventEmitterService,
    private collectionService: CollectionService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {
    this.activeRoute.queryParams.subscribe((params) => {
      if (params.collection) {
        // this.routerCollection = params.collection;
        this.collectionfilter([params.collection]);
        // this.newFilter([[], [params.collection], [], [], []]);
      } else {
        this.getAllProducts();
      }
    });
    // this.socket = io('');
    const newLocal = new Promise(() => {
      this.loadScript();
    });
    collectionService.getCollection().subscribe(
      (data) => {
        // tslint:disable-next-line: no-string-literal
        data['data'].forEach((element) => {
          this.collection.push({ name: element.name, val: false });
        });
        this.activeRoute.queryParams.subscribe((params) => {
          if (params.x !== null && params.y !== null) {
            this.filter(params.x, params.y);
          }
        });
      },
      (error) => {
        // Do something with error
        console.error(error);
      }
    );
    $('html, body').animate(
      {
        scrollTop: 0,
      },
      600
    );
    document.getElementsByTagName('body')[0].setAttribute('style', 'overflow:visible');
  }

  ngOnInit() {
    this.loadScript();
    // Sidebar Filter - Show & Hide
    this.sidebarToggler = $('.sidebar-toggler');
    (this.body = $('body')),
      this.sidebarToggler.on('click', (e) => {
        this.body.toggleClass('sidebar-filter-active');
        $(this).toggleClass('active');
        e.preventDefault();
      });

    $('.sidebar-filter-overlay').on('click', (e) => {
      this.body.removeClass('sidebar-filter-active');
      this.sidebarToggler.removeClass('active');
      e.preventDefault();
    });
    // this.socket.on('add-product', () => {
    //   this.getAllProducts();
    // });
    // this.socket.on('edit-product', () => {
    //   this.getAllProducts();
    // });
  }

  ismobile() {
    if ($(window).width() > 577) {
      return false;
    }
    return true;
  }

  public loadScript() {
    const node = document.createElement('script');
    node.src = 'assets/js/main.js';
    node.type = 'text/javascript';
    node.async = true;
    // tslint:disable-next-line: deprecation
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }
  getAllProducts() {
    this.productService.getProducts().subscribe((data) => {
      this.products = [];
      // tslint:disable-next-line: no-string-literal
      this.products = data['data'];
      this.filter('', '');
      this.filteredArray = this.vproducts.slice(0, 24);
    });
  }

  onPageChange($event) {
    this.page = $event.pageIndex;
    this.filteredArray = this.vproducts.slice(
      $event.pageIndex * $event.pageSize,
      $event.pageIndex * $event.pageSize + $event.pageSize
    );
  }

  increaseValue() {
    this.qty++;
    this.eventEmitterService.onFirstComponentButtonClick();
  }

  decreaseValue() {
    if (this.qty > 1) {
      this.qty--;
    }

    this.eventEmitterService.onFirstComponentButtonClick();
  }

  filter(x, y) {
    let e = [];

    e = this.products;
    this.vproducts = e;
    this.filteredArray = this.vproducts.slice(0, 24);
  }

  procductChangedHandler(x) {
    this.selectProduct = x;
  }

  newFilter(metaDataArray) {
    const selectedProducts: Set<any> = metaDataArray[0];
    const selectedCollections: Set<any> = metaDataArray[1];
    const selectedGenders: Set<any> = metaDataArray[2];
    const selectedBrands: Set<any> = metaDataArray[3];
    const selectedSizes: Set<any> = metaDataArray[4];

    const newProductList = new Set();

    this.productService.getProducts().subscribe((data) => {
      // tslint:disable-next-line: no-string-literal
      const res = data['data'];
      // tslint:disable-next-line: no-string-literal
      this.products = data['data'];

      // if (selectedGenders.size > 0) {
      //   var temp = res.filter((product) => selectedGenders.has(product.category));
      //   temp.forEach((element) => {
      //     newProductList.add(element);
      //   });
      // }
      if (selectedProducts.size > 0) {
        let temp = res.filter((product) =>
          selectedProducts.has(product.productType)
        );
        if (selectedGenders.size > 0) {
          temp = temp.filter((product) =>
            selectedGenders.has(product.category)
          );
        }
        if (selectedSizes.size > 0) {
          temp = temp.filter((product) => selectedSizes.has(product.size));
        }
        temp.forEach((element) => {
          newProductList.add(element);
        });
      }
      if (selectedCollections.size > 0) {
        const temp = res.filter((product) =>
          selectedCollections.has(product.itemCollection)
        );
        temp.forEach((element) => {
          newProductList.add(element);
        });
      }

      if (selectedBrands.size > 0) {
        const temp = res.filter((product) =>
          selectedBrands.has(product.itemBrand)
        );
        temp.forEach((element) => {
          newProductList.add(element);
        });
      }
      // if (selectedSizes.size > 0) {
      //   var temp = res.filter((product) =>
      //     selectedCollections.has(product.itemCollection)
      //   );
      //   temp.forEach((element) => {
      //     newProductList.add(element);
      //   });
      // }
      if (newProductList.size > 0) {
        this.products = [];

        newProductList.forEach((element) => {
          this.products.push(element);
        });
      }
      this.filter('', '');

    });

    // console.log(collection);
    // if (productArray.length > 0) {

    // ##
    // this.productService.getProducts().subscribe((data) => {
    //   this.products = [];
    //   var res = data['data'];

    //   if (productArray.length > 0) {
    // res = res.filter((product) =>
    //   productArray.includes(product.productType)
    // );
    //   }
    // if (collectionArray.size > 0) {
    //   res = res.filter((product) =>
    //     collectionArray.has(product.itemCollection)
    //   );
    // }
    //   if (categoryArray.length > 0) {
    //     res = res.filter((product) => categoryArray.includes(product.category));
    //   }
    //   if (brandArray.size > 0) {
    //     res = res.filter((product) => brandArray.has(product.itemBrand));
    //   }
    //   this.products = res;
    //   console.log(res);
    //   // data['data'].forEach((element) => {
    //   //   productArray.forEach((type) => {
    //   //     var flag = false;
    //   //     if (element.productType == type) this.products.push(element);
    //   //   });
    //   // });
    //   this.filter('', '');
    //   this.filteredArray = this.vproducts.slice(0, 24);
    // });

    // ##

    // } else {
    //   this.productService.getProducts().subscribe((data) => {
    //     this.products = [];
    //     data['data'].forEach((element) => {
    //       this.products.push(element);
    //     });
    //     this.filter('', '');
    //     this.filteredArray = this.vproducts.slice(0, 24);
    //   });
    // }
  }


  collectionfilter(selectedCollections){
    this.productService.getProducts().subscribe((data) => {
      // tslint:disable-next-line: no-string-literal
      const res = data['data'];
      // tslint:disable-next-line: no-string-literal
      this.products = data['data'];
      if (selectedCollections.length > 0) {
        this.products = [];
        res.forEach(element => {
          if (element.itemCollection === selectedCollections[0]){
            this.products.push(element);
          }
        });
        this.filter('', '');
        this.filteredArray = this.vproducts.slice(0, 24);

      }
    });
  }
}

import { LookBookComponent } from './look-book/look-book.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ShopComponent } from './shop/shop.component';
import { CartComponent } from './cart/cart.component';
import { ProductComponent } from './product/product.component';
import { QuickViewComponent } from './quick-view/quick-view.component';
import { LoginComponent } from './login/login.component';
import { GalleryComponent } from './gallery/gallery.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OneBlogComponent } from './one-blog/one-blog.component';
import { OrderComponent } from './order/order.component';
import {PrivacyComponent} from './privacy/privacy.component';
import {TermsCondishonComponent} from './terms-condishon/terms-condishon.component';
import {FaqComponent} from './faq/faq.component';
import {AboutUsComponent} from './about-us/about-us.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'shop', component: ShopComponent
  },
  {
    path: 'cart', component: CartComponent
  },
  {
    path: 'blog', component: OneBlogComponent
  },
  {
    path: 'product', component: ProductComponent
  },
  {
    path: 'quick-view', component: QuickViewComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'look-book', component: GalleryComponent
  },
  {
    path: 'one-look-book', component: LookBookComponent
  },
  {
    path: 'checkout', component: CheckoutComponent
  },
  {
    path: 'privacy', component: PrivacyComponent
  },
  {
    path: 'terms', component: TermsCondishonComponent
  },
  {
    path: 'about', component: AboutUsComponent
  },
  {
    path: 'faq', component: FaqComponent
  },
  {
    path: 'orders',
    component: OrderComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsCondishonComponent } from './terms-condishon.component';

describe('TermsCondishonComponent', () => {
  let component: TermsCondishonComponent;
  let fixture: ComponentFixture<TermsCondishonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsCondishonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsCondishonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

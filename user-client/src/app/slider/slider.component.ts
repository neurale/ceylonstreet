import { Component, OnInit } from '@angular/core';
import { UiService } from '../service/ui.service';
declare var $: any;

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  homeBanner1 = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };
  homeBanner2 = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };
  homeBanner3 = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };
  homeBanner4 = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };

  homeBanner1Mobile = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };
  homeBanner2Mobile = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };
  homeBanner3Mobile = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };
  homeBanner4Mobile = {
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    picUrl: ''
  };

  

  constructor(private uiService: UiService) {
    uiService.getUI().subscribe(
      data => {
        console.log(data)
        // tslint:disable-next-line: no-string-literal
        this.homeBanner1 = data['data'][0].homeBanner1;
        // tslint:disable-next-line: no-string-literal
        this.homeBanner2 = data['data'][0].homeBanner2;
        // tslint:disable-next-line: no-string-literal
        this.homeBanner3 = data['data'][0].homeBanner3;
        // tslint:disable-next-line: no-string-literal
        this.homeBanner4 = data['data'][0].homeBanner4;
        // tslint:disable-next-line: no-string-literal
        this.homeBanner1Mobile = data['data'][0].homeBanner1Mobile;
        // tslint:disable-next-line: no-string-literal
        this.homeBanner2Mobile = data['data'][0].homeBanner2Mobile;
        // tslint:disable-next-line: no-string-literal
        this.homeBanner3Mobile = data['data'][0].homeBanner3Mobile;
        // tslint:disable-next-line: no-string-literal
        this.homeBanner4Mobile = data['data'][0].homeBanner4Mobile;
        // this.productBanner1 = data['data'][0]['productBanner1'];
        // this.productBanner2 = data['data'][0]['productBanner2'];
        // this.productBanner3 = data['data'][0]['productBanner3'];
        // this.productBanner4 = data['data'][0]['productBanner4'];
        // this.productBanner5 = data['data'][0]['productBanner5'];
        // this.productBanner6 = data['data'][0]['productBanner6'];
        // this.carasole = data['data'][0]['carousel'];
        // tslint:disable-next-line: no-unused-expression
        setTimeout(() => {
          // this.loadScript();
          $('.tp-fullscreen').revolution({
            delay: 4000,
            startwidth: 1170,
            startheight: 750,
            hideThumbs: 0,
            hideArrowsOnMobile: 'on',
            hideBulletsOnMobile: 'on',
            fullWidth: 'on',
            fullScreen: 'on',
            soloArrowLeftHOffset: 0,
            soloArrowRightHOffset: 0
          });
        }, 100);
      },
      error => {
        // Do something with error
        console.error(error);
      }
    );
  }

  public loadScript() {
    const node1 = document.createElement('script');
    node1.src = 'assets/js/scripts.js';
    node1.type = 'text/javascript';
    node1.async = true;
    node1.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node1);
    const node = document.createElement('script');
    node.src = 'assets/js/main.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  ngOnInit(): void {
    const newLocal = new Promise(() => {

    });
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

}

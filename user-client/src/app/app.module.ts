import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { GalleryComponent } from './gallery/gallery.component';
import { CartComponent } from './cart/cart.component';
import { ShopComponent } from './shop/shop.component';
import { ProductComponent } from './product/product.component';
import { SliderComponent } from './slider/slider.component';
import { HomeProductComponent } from './home-product/home-product.component';
import { HomeCollectionComponent } from './home-collection/home-collection.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderComponent } from './order/order.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { TermsCondishonComponent } from './terms-condishon/terms-condishon.component';
import { QuickViewComponent } from './quick-view/quick-view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { ProductBoxComponent } from './product-box/product-box.component';
import { EventEmitterService } from './service/event-emitter.service';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatPaginatorModule } from '@angular/material/paginator';
import { OneBlogComponent } from './one-blog/one-blog.component';
import { FilterComponent } from './shop/filter/filter.component';
import { LookBookComponent } from './look-book/look-book.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ShipingService } from './service/shiping.service';
import { PrivacyComponent } from './privacy/privacy.component';
import { FaqComponent } from './faq/faq.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    BannerComponent,
    GalleryComponent,
    CartComponent,
    ShopComponent,
    ProductComponent,
    SliderComponent,
    HomeProductComponent,
    HomeCollectionComponent,
    LoginComponent,
    SignupComponent,
    CheckoutComponent,
    OrderComponent,
    AboutUsComponent,
    TermsCondishonComponent,
    QuickViewComponent,
    ProductBoxComponent,
    OneBlogComponent,
    FilterComponent,
    LookBookComponent,
    PrivacyComponent,
    FaqComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    SocialLoginModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    Ng2SearchPipeModule,
  ],
  providers: [
    EventEmitterService,
    ShipingService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '1000745867203-gs7av4fmul7k72iiim4dr3066iekuqfs.apps.googleusercontent.com'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('143955024171724'),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [QuickViewComponent],
})
export class AppModule {}

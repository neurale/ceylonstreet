import { SocialAuthService } from 'angularx-social-login';
import { OrderService } from './../service/order.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  email;
  name;
  pic;
  orders;
  items = [];
  items1 = [];

  constructor(private router: Router, private orderSrvice: OrderService, private authService: SocialAuthService,) {
    const newLocal = new Promise(() => {
      this.loadScript();
    });
    $('html, body').animate({
      scrollTop: 0
    }, 600);
    this.email = localStorage.getItem('email');
    this.name = localStorage.getItem('name');
    this.pic = localStorage.getItem('pic');
    if (!this.isValid()){
      localStorage.removeItem('token');
      this.router.navigate(['/']);
    }
    this.items = [];
    this.items1 = [];

  }

  ngOnInit(): void {
    const val = this.orderSrvice.getOrdersByEmail(this.email).subscribe(
      data => {
        this.orders = data.data;
        data.data.forEach(e => {
          e.items.forEach(e1 => {
            if (e.status !== 'deilverd') {
              if (e1.size.length === 0) {
                this.items.push({
                  picUrl: e1.picUrl,
                  name: e1.item_name,
                  qty: e1.qty,
                  _id: e._id,
                  date: e.date,
                  status: e.status
                });
              } else {
                e1.size.forEach(element => {
                  this.items.push({
                    picUrl: e1.picUrl,
                    name: e1.item_name + '  ' + element.size,
                    qty: element.qty,
                    _id: e._id,
                    date: e.date,
                    status: e.status
                  });
                });
              }
            } else {
              if (e1.size.length === 0) {
                this.items1.push({
                  picUrl: e1.picUrl,
                  name: e1.item_name,
                  qty: e1.qty,
                  _id: e._id,
                  date: e.date,
                  status: e.status
                });
              } else {
                e1.size.forEach(element => {
                  this.items1.push({
                    picUrl: e1.picUrl,
                    name: e1.item_name + '  ' + element.size,
                    qty: element.qty,
                    _id: e._id,
                    date: e.date,
                    status: e.status
                  });
                });
              }
            }
          });
        });
      }
    );
  }

  public loadScript() {
    const node = document.createElement('script');
    node.src = 'assets/js/script.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    // document.getElementsByTagName('head')[0].appendChild(node);
  }


  isValid() {
    const token = localStorage.getItem('token');
    if (token) {
      const payload = this.payload(token);

      if (payload) {
        if (Date.now() >= payload.exp * 1000) {
          Swal.fire(
            'Time Out!',
            'Please login again!',
            'error'
          );
          localStorage.removeItem('name');
          localStorage.removeItem('pic');
          localStorage.removeItem('email');
          localStorage.removeItem('token');
          return false;
        }
        return (Date.now() <= payload.exp * 1000 ) ? true : false;
      }
    }
    return false;
  }

  payload(token) {
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload) {
    const base64 = payload.replace(/-/g, '+').replace(/_/g, '/');
    return JSON.parse(window.atob(base64));
  }

  logout(){
    this.authService.signOut();
    localStorage.removeItem('email');
    localStorage.removeItem('name');
    localStorage.removeItem('pic');
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

}

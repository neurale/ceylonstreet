import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CollectionService } from '../service/collection.service';
declare var $: any;



@Component({
  selector: 'app-home-collection',
  templateUrl: './home-collection.component.html',
  styleUrls: ['./home-collection.component.scss']
})
export class HomeCollectionComponent implements OnInit {
  collections: any;

  constructor(private collectionService: CollectionService, private router: Router) {
    this.collectionService.getCollection().subscribe(
      data => {
        // tslint:disable-next-line: no-string-literal
        this.collections = data['data'];
        setTimeout(() => {
          // this.loadScript();
          $('.carousel-boxed3').owlCarousel({
            nav: false,
            dots: true,
            autoplay: true,
            loop: true,
            autoplayTimeout: 5000,
            responsive: {
              0: {
                items: 2
              },
              480: {
                items: 2
              },
              768: {
                items: 3
              },
              991: {
                items: 3
              },
              1200: {
                items: 3,
                nav: true,
                dots: false
              }
            }
          });
          $('.carousel-boxed').owlCarousel({
            nav: false,
            dots: true,
            autoplay: true,
            loop: true,
            autoplayTimeout: 5000,
            margin: 30,
            navText: ['', ''],
            responsive: {
              0: {
                items: 1
              },
              768: {
                items: 2,
                nav: true,
              },
              991: {
                items: 3,
                nav: true,
              }
            }
          });
        }, 500);
        this.loadScript();
      },
      error => {
        // Do something with error
        console.error(error);

      }
    );
  }

  ngOnInit(): void {
  }

  public loadScript() {
    const node1 = document.createElement('script');
    node1.src = 'assets/js/scripts.js';
    node1.type = 'text/javascript';
    node1.async = true;
    // tslint:disable-next-line: deprecation
    node1.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node1);
    // const node = document.createElement('script');
    // node.src = 'assets/js/main.js';
    // node.type = 'text/javascript';
    // node.async = true;
    // node.charset = 'utf-8';
    // document.getElementsByTagName('head')[0].appendChild(node);
  }

  toCollection(x){
    this.router.navigate(['shop'], {skipLocationChange: true, queryParams: {collection: x.name}} );
  }

}

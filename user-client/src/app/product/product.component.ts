import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { EventEmitterService } from '../service/event-emitter.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ValueService } from '../service/value.service';
declare var $: any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  dataimage;
  product = {
    birthstone: '',
    colors: '',
    cut: '',
    description: '',
    discountRate: '',
    gemstone: '',
    gemstoneCut: '',
    gemstoneWeight: '',
    itemCollection: '',
    itemBrand: '',
    measurement: '',
    metal: '',
    name: '',
    necklaceLength: '',
    picUrl: [''],
    productCostPrice: '',
    productPrice: '0',
    productType: '',
    qty: '',
    rodiumPlated: '',
    saleOfferPrice: '0',
    size: '',
    sizeArray: [],
    sizeArrayQty: [],
    species: '',
    treatments: '',
    variety: '',
    weight: '',
    grams: '',
    subProductType: '',
    __v: 0,
    _id: '',
  };
  products = [];
  qty = 1;
  selectProduct = {
    birthstone: '',
    colors: '',
    cut: '',
    description: '',
    discountRate: '',
    gemstone: '',
    gemstoneCut: '',
    gemstoneWeight: '',
    itemCollection: '',
    itemBrand: '',
    measurement: '',
    metal: '',
    name: '',
    necklaceLength: '',
    picUrl: [''],
    productCostPrice: '',
    productPrice: '',
    productType: '',
    qty: '',
    rodiumPlated: '',
    saleOfferPrice: '',
    size: '',
    sizeArray: [],
    sizeArrayQty: [],
    species: '',
    treatments: '',
    variety: '',
    weight: '',
    subProductType: '',
    grams: '',

    __v: 0,
    _id: '',
  };
  size = '';
  offer: number;
  count = 0;
  id;
  pqty = 0;
  pqty1 = 0;
  val = [];

  constructor(
    private productService: ProductService,
    private eventEmitterService: EventEmitterService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    public valueService: ValueService
  ) {
    this.activeRoute.queryParams
      .subscribe(params => {
        if (params.id !== null) {
          this.id = params.id;
          this.productService.getProductById(params.id).subscribe((data) => {
            this.product = data.data;
            if (this.product.productType === 'fashion') {
              let cart;
              let myMap = new Map();
              if (localStorage.getItem('cart')) {
                cart = JSON.parse(localStorage.getItem('cart'));
                cart.forEach(element => {
                  if (element._id === this.product._id) {
                    element.size.forEach(e1 => {
                      myMap.set(e1.size, e1.qty);
                    });
                  }
                });
                for (const element1 of this.product.sizeArray) {
                  for (const index of this.product.sizeArrayQty) {
                    if (element1 === index.size) {
                      const qty = myMap.get(element1) ?? 0;
                      if (index.qty > qty) {
                        this.count += myMap.get(element1) ?? 0;
                        this.val.push(index.size);
                      }
                    }
                  }
                }
                if (this.val === []) {
                  this.pqty = 0;
                } else {
                  this.pqty = +this.product.qty;
                }
              } else {
                for (const index of this.product.sizeArrayQty) {
                  if (index.qty > 0) {
                    this.val.push(index.size);
                  }
                }
                this.pqty = +this.product.qty;
              }
            } else {
              if (!localStorage.getItem('cart')) {
                this.count = 0;

              } else {
                let check = false;
                const cart = JSON.parse(localStorage.getItem('cart'));
                cart.forEach(element => {
                  if (element._id === this.product._id) {
                    this.count = element.qty;
                    check = true;
                  }
                });
                if (!check) {
                  this.count = 0;
                }
              }
              this.pqty = +this.product.qty;
            }
            this.pqty1 = +this.product.qty;
            this.offer = 100 - ((+this.product.saleOfferPrice) / (+this.product.productPrice)) * 100;
            // tslint:disable-next-line: no-shadowed-variable
            this.productService.getProducts().subscribe((data) => {
              this.products = [];
              let i = 0;
              // tslint:disable-next-line: prefer-for-of
              for (let index = 0; index < data['data'].length; index++) {
                // tslint:disable-next-line: no-string-literal
                if (data['data'][index].itemCollection === this.product.itemCollection) {
                  // tslint:disable-next-line: no-string-literal
                  this.products.push(data['data'][index]);
                  i++;
                }
                if (i === 10) {
                  break;
                }
              }
              const newLocal = new Promise(() => {
                this.loadScript();
              });
            });
          });
        } else {
          this.router.navigate(['/shop']);
        }
      });
    $('html, body').animate({
        scrollTop: 0
      }, 600);
  }

  ngOnInit(): void {}

  public loadScript() {
    setTimeout(() => {
      // this.loadScript();
      $('.carousel-boxed3').owlCarousel({
        nav: false,
        dots: true,
        autoplay: true,
        loop: true,
        autoplayTimeout: 5000,
        responsive: {
          0: {
            items: 2
          },
          480: {
            items: 2
          },
          768: {
            items: 3
          },
          991: {
            items: 4
          },
          1200: {
            items: 4,
            nav: true,
            dots: false
          }
        }
      });
    }, 50);
    const node1 = document.createElement('script');
    node1.src = 'assets/js/main.js';
    node1.type = 'text/javascript';
    node1.async = true;
    // tslint:disable-next-line: deprecation
    node1.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node1);
  }

  ismobile() {
    if ($(window).width() > 577) {
      return false;
    }
    return true;
  }

  change(x){
    return (+x) * (+this.valueService.getValue());
  }

  increaseValue() {
    this.count = 0;
    if (this.product.productType === 'fashion') {
      let cart;
      const myMap = new Map();
      const myMap1 = new Map();
      if (localStorage.getItem('cart')) {
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === this.product._id) {
            element.size.forEach(e1 => {
              myMap.set(e1.size, e1.qty);
            });
          }
        });
        if (this.val === []) {
          this.pqty = 0;
        } else {
          if (this.size !== '') {
            for (const index of this.product.sizeArrayQty) {
              if (this.size === index.size) {
                const qty = myMap.get(this.size) ?? 0;
                if (index.qty > qty) {
                  this.count += myMap.get(this.size) ?? 0;
                  this.pqty = index.qty;
                }
              }
            }
          } else {
            for (const index of this.product.sizeArrayQty) {
              if (this.val[0] === index.size) {
                const qty = myMap.get(this.val[0]) ?? 0;
                if (index.qty > qty) {
                  this.count += myMap.get(this.val[0]) ?? 0;
                  this.pqty = index.qty;
                }
              }
            }

          }
        }
      } else {
        this.product.sizeArrayQty.forEach(e1 => {
          myMap1.set(e1.size, e1.qty);
        });
        if (this.size !== '') {
          this.count = 0;
          this.pqty = myMap1.get(this.size) ?? 0;
        } else {
          this.count = 0;
          this.pqty = myMap1.get(this.val[0]) ?? 0;
        }
      }
    }
    // if (this.size !== '') {
    //   this.product.sizeArrayQty.forEach(element => {
    //     if (element.size === this.size) {
    //       this.pqty = element.qty;
    //     }
    //   });
    // }

    if (this.pqty > (this.qty + this.count)){
      this.qty++;
    } else {
      Swal.fire({
        title: 'Out Of Stock',
        text: 'You cant\'t add anymore!',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok'
      });
    }
    this.eventEmitterService.onFirstComponentButtonClick();
  }

  decreaseValue() {
    if (this.qty > 1) {
      this.qty--;
    }

    this.eventEmitterService.onFirstComponentButtonClick();
  }

  addToCart(id) {
    if (this.size !== '') {
      this.product.sizeArrayQty.forEach(element => {
        if (element.size === this.size) {
          if (this.qty > element.qty) {
            this.qty = element.qty;
          }
        }
      });
    }
    let cart;

    if (this.product.productType === 'fashion'){
      if (!localStorage.getItem('cart')) {
        cart = [{ _id: id, qty: this.qty, size: [{ size: this.size, qty: this.qty}] }];

      } else {
        let check = false;
        let check1 = false;
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === id) {
            element.size.forEach(element1 => {
              if (element1.size === this.size) {
                element.qty = element.qty + this.qty;
                element1.qty = element1.qty + this.qty;
                check1 = true;
              }
            });
            if (!check1) {
              element.qty = element.qty + this.qty;
              element.size.push({ size: this.size, qty: this.qty });
            }
            check = true;
          }
        });
        if (!check) {
          cart.push({ _id: id, qty: this.qty, size: [{ size: this.size, qty: this.qty }]});
        }
      }
    } else {
      if (!localStorage.getItem('cart')) {
        cart = [{ _id: id, qty: this.qty }];

      } else {
        let check = false;
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === id) {
            element.qty = element.qty + this.qty;
            check = true;
          }
        });
        if (!check) {
          cart.push({ _id: id, qty: this.qty });
        }
      }
    }

    localStorage.setItem('cart', JSON.stringify(cart));
    this.eventEmitterService.onFirstComponentButtonClick();
    Swal.fire({
      title: 'Checkout?',
      text: '',
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Cart!'
    }).then((result) => {
      if (result.value) {
        this.router.navigate(['/cart']);
      }
    });
  }

  procductChangedHandler(x) {
    this.selectProduct = x;
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy(): void {
    $('.zoomContainer').remove();

  }

  disable(){
    return this.product.productType === 'fashion' ? this.size === '' ? false : true : true;
  }
}

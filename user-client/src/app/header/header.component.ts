import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  islogin = false;
  items = [];
  term = '';

  constructor(private productService: ProductService, private router: Router) {
    this.productService.getProducts().subscribe((data) => {
      this.items = data['data'];
    });

    if (!localStorage.getItem('token')) {
      this.islogin = false;
    } else {
      this.islogin = true;
    }
  }

  ngOnInit(): void {}

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }
  toProduct(id) {
    this.router.navigate(['/shop']).then(() => {
      this.router.navigate(['/product'], {
        skipLocationChange: true,
        queryParams: { id },
      });
    });
  }
}

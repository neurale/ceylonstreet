import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUri = 'https://ceylonstreet.herokuapp.com/api/users';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {}

  signup(data): Observable<any> {
    const url = `${this.baseUri}/signup`;
    return this.http.post(url, data, { headers: this.headers });
  }
}

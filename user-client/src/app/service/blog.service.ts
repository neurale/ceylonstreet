import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  baseUri = 'https://ceylonstreet.herokuapp.com/api/blogs';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  // Get all blogs
  getBlogs() {
    return this.http.get(`${this.baseUri}/blog`);
  }

  // Get blog by id
  getBlogById(id): Observable<any> {
    const url = `${this.baseUri}/blogbyid/${id}`;
    return this.http.get(url, { headers: this.headers });
  }

  // Get blog by name
  getBlogByName(name): Observable<any> {
    const url = `${this.baseUri}/blogbyname/${name}`;
    return this.http.get(url, { headers: this.headers });
  }



}

import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  invokeFirstComponentFunction = new EventEmitter();
  invokeFirstComponentFunction1 = new EventEmitter();
  subsVar: Subscription;

  constructor() { }

  onFirstComponentButtonClick() {
    this.invokeFirstComponentFunction.emit();
  }

  onFirstComponentButtonClick1() {
    this.invokeFirstComponentFunction1.emit();
  }

}

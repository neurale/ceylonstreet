import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValueService {

  value = 0;
  load = false;

  constructor(public http: HttpClient) {
    if (this.value === 0 || this.value === undefined || this.value === null) {
      this.http.get('https://free.currconv.com/api/v7/convert?q=USD_LKR&compact=ultra&apiKey=442b2a39d4c829f4eba8').subscribe(
        e => {
          this.value = (Math.ceil(e['USD_LKR'] * 100)) / 100;
        });
    }
  }
  getValue(){
    if((this.value === 0 || this.value === undefined || this.value === null) && this.load === true){
      this.load = true;
      this.http.get('https://free.currconv.com/api/v7/convert?q=USD_LKR&compact=ultra&apiKey=442b2a39d4c829f4eba8').subscribe(
        e => {
          this.value = (Math.ceil(e['USD_LKR'] * 100)) / 100;
        });
    }
    return this.value;
  }
}

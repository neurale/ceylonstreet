import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LookbookService {
  baseUri = 'https://ceylonstreet.herokuapp.com/api/lookBooks';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }


  // Get all lookBooks
  getLookBooks() {
    return this.http.get(`${this.baseUri}/lookBook`);
  }

  // Get lookBook by id
  getLookBookById(id): Observable<any> {
    const url = `${this.baseUri}/lookBookbyid/${id}`;
    return this.http.get(url, { headers: this.headers });
  }

  // Get lookBook by name
  getLookBookByName(name): Observable<any> {
    const url = `${this.baseUri}/lookBookbyname/${name}`;
    return this.http.get(url, { headers: this.headers });
  }


}

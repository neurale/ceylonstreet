import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
baseUri = 'https://ceylonstreet.herokuapp.com/api/orders';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {}

  setorrder(data) {
    const url = `${this.baseUri}/add`;
    return this.http.post(url, data, { headers: this.headers });
  }

  valid(data) {
    const url = `${this.baseUri}/valid`;
    return this.http.post(url, data, { headers: this.headers });
  }

  getOrdersByEmail(email): Observable<any> {
    const header = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set(
        'Authorization',
        'Bearer ' + localStorage.getItem('token').split('JWT')[1]
      );
    const url = `${this.baseUri}/orderbyname/${email}`;
    return this.http.get(url, { headers: header });
  }
}

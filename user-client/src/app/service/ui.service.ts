import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  baseUri = 'https://ceylonstreet.herokuapp.com/api/homeMeta';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }


  // Get all ui
  getUI() {
    return this.http.get(`${this.baseUri}/homeMetaSchema`);
  }

}

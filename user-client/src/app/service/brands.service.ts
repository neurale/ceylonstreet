import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class BrandsService {
  baseUri = 'https://ceylonstreet.herokuapp.com/api/brands';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {}

  // Get all blogs
  getBrands() {
    return this.http.get(`${this.baseUri}/brand`);
  }


}

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {
  baseUri = 'https://ceylonstreet.herokuapp.com/api/collections';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  getCollection() {
    return this.http.get(`${this.baseUri}/collection`);
  }
}

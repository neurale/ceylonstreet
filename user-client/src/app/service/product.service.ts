import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  baseUri = 'https://ceylonstreet.herokuapp.com/api/products';
  baseUri2 = 'https://ceylonstreet.herokuapp.com/api/promoCode';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }


  // Get all products
  getProducts() {
    return this.http.get(`${this.baseUri}/product`);
  }

  // Get product by id
  getProductById(id): Observable<any> {
    const url = `${this.baseUri}/productbyid/${id}`;
    return this.http.get(url, { headers: this.headers });
  }

  // Get product by name
  getProductByName(name): Observable<any> {
    const url = `${this.baseUri}/productbyname/${name}`;
    return this.http.get(url, { headers: this.headers });
  }

  getCoupon() {
    return this.http.get(`${this.baseUri2}/promoCode`);
  }


}

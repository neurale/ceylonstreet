import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShipingService {

  baseUri = 'https://ceylonstreet.herokuapp.com/api/products';
  baseUri2 = 'https://ceylonstreet.herokuapp.com/api/promoCode';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }


  // Get all products
  getLocalrice() {
    return this.http.get(`${this.baseUri}/product`);
  }

  getPrice() {
    return this.http.get(`${this.baseUri}/product`);
  }
}
// EAACC7SQUUswBAAmZC07w165OfVeVPOkCTkOL0JMZBKbFdsh6YNSTaiPAGIJp8ujOuXv9Hvz7lTIkeSvPF7Vrk8lFcHKJLC8dLYYGYpaznsHwBZBR7wbyZBeEsnyyZCbsDz42lOj3wZCz8dxYoZAbCpbZAZA4gy6nXCFZCpPu8SP6RmqR9P8tH2my7NEtm1DDkloLjPHOsNzVecIAZDZD


// curl - i - X GET \
// "https://graph.facebook.com/v10.0/17841431513178246?fields=media&access_token=EAACC7SQUUswBAOjVdyegpQqv4wADpxhkqdn7XCqHMUIWIVnnFKFFBu7hOkZBfLxGqSbEiRTRmmju3TQ8aw4P2w9WNSM8VwRha5O3vP75KEG1MkNnKngm2LajKARXm2CEVAT9Peg6xITv7FZClZCqQuFGnxS98rFQtT3l01ww86melyNW4ThDOsA2bnvOKECH6tXcg7kYAZDZD"

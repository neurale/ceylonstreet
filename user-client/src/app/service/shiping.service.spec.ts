import { TestBed } from '@angular/core/testing';

import { ShipingService } from './shiping.service';

describe('ShipingService', () => {
  let service: ShipingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShipingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { UiService } from '../service/ui.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  productBanner1 = {
    bannerType: '',
    buttonText: '',
    collection: '',
    line1: '',
    line2: '',
    navigationType: '',
    picUrl: '',
    product: ''
  };
  productBanner2 = {
    bannerType: '',
    buttonText: '',
    collection: '',
    line1: '',
    line2: '',
    navigationType: '',
    picUrl: '',
    product: ''
  };
  productBanner3 = {
    bannerType: '',
    buttonText: '',
    collection: '',
    line1: '',
    line2: '',
    navigationType: '',
    picUrl: '',
    product: ''
  };
  productBanner4 = {
    bannerType: '',
    buttonText: '',
    collection: '',
    line1: '',
    line2: '',
    navigationType: '',
    picUrl: '',
    product: ''
  };

  constructor(private uiService: UiService, private router: Router) {
    uiService.getUI().subscribe(
      data => {
        // tslint:disable-next-line: no-string-literal
        this.productBanner1 = data['data'][0].productBanner1;
        // tslint:disable-next-line: no-string-literal
        this.productBanner2 = data['data'][0].productBanner2;
        // tslint:disable-next-line: no-string-literal
        this.productBanner3 = data['data'][0].productBanner3;
        // tslint:disable-next-line: no-string-literal
        this.productBanner4 = data['data'][0].productBanner4;
      },
      error => {
        // Do something with error
        console.error(error);
      }
    );
  }

  ngOnInit(): void {
  }

  call(x){
    if (x.navigationType === 'collection') {
      this.router.navigate(['shop'], { skipLocationChange: true, queryParams: { collection: x.collection } });
    } else {
      this.router.navigate(['shop']).then(() => {
        this.router.navigate(['product'], { skipLocationChange: true, queryParams: { id: x.product } });
      });
    }
  }


  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }


}

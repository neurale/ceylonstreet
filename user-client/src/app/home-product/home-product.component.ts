import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from '../service/event-emitter.service';
import { ProductService } from '../service/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-product',
  templateUrl: './home-product.component.html',
  styleUrls: ['./home-product.component.scss']
})
export class HomeProductComponent implements OnInit {

  newProduct = [];
  demandProduct = [];
  product = [];

  constructor(private eventEmitterService: EventEmitterService, private productService: ProductService, private router: Router) {
    this.getProduct();
  }

  ngOnInit(): void {
  }

  getProduct(){
    this.newProduct = [];
    this.demandProduct = [];
    let x = 0;
    let y = 0;
    this.productService.getProducts().subscribe(
      data => {
        // tslint:disable-next-line: no-string-literal
        this.product = data['data'];
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.product.length; i++){
          if (this.product[i]?.newArrival === true && x < 8){
            x++;
            this.newProduct.push(this.product[i]);
          }
          if (this.product[i]?.inDemand === true && y < 8){
            y++;
            this.demandProduct.push(this.product[i]);
          }
          if (x === 8 && y === 8){
            break;
          }

        }
      },
      error => {
        console.log(error);
      });
  }

}

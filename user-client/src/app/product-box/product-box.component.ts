import { Component, OnInit, Input } from '@angular/core';
import { EventEmitterService } from '../service/event-emitter.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { map } from 'jquery';
import { ValueService } from '../service/value.service';

@Component({
  selector: 'app-product-box',
  templateUrl: './product-box.component.html',
  styleUrls: ['./product-box.component.scss']
})
export class ProductBoxComponent implements OnInit {

  @Input()
  product;
  link;
  count = 0;
  pqty = 0;
  val = '';


  constructor(private eventEmitterService: EventEmitterService, private router: Router, public valueService: ValueService) {
  }

  ngOnInit(): void {

    this.link = 'quick-view?id=' + this.product._id;
    if (this.product.productType === 'fashion') {
      let cart;
      let myMap = new Map();
      if (localStorage.getItem('cart')) {
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === this.product._id) {
            element.size.forEach(e1 => {
              myMap.set(e1.size, e1.qty);
            });
          }
        });
        for (const element1 of this.product.sizeArray) {
          for (const index of this.product.sizeArrayQty) {
            if (element1 === index.size) {
              this.count += myMap.get(element1) ?? 0;
              const qty = myMap.get(element1) ?? 0;
              if (index.qty > qty) {
                this.val = index.size;
                break;
              }
            }
          }
        }
        if (this.val === ''){
          this.pqty = 0;
        } else {
          this.pqty = this.product.qty;
        }
      } else {
        for (const index of this.product.sizeArrayQty) {
          if (index.qty > 0) {
            this.val = index.size;
            break;
          }
        }
        this.pqty = this.product.qty;
      }
    } else {
      if (!localStorage.getItem('cart')) {
        this.count = 0;

      } else {
        let check = false;
        const cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === this.product._id) {
            this.count = element.qty;
            check = true;
          }
        });
        if (!check) {
          this.count = 0;
        }
      }
      this.pqty = this.product.qty;
    }
  }

  addToCart(id) {
    let cart;

    if (this.product.productType === 'fashion') {
      let cart1;
      const myMap = new Map();
      const cart4 = localStorage.getItem('cart');
      if (localStorage.getItem('cart')) {
        cart1 = JSON.parse(localStorage.getItem('cart'));
        cart1.forEach(element => {
          if (element._id === this.product._id) {
            element.size.forEach(e1 => {
              myMap.set(e1.size, e1.qty);
            });
          }
        });
        let check = false;
        for (const element1 of this.product.sizeArray) {
          for (const index of this.product.sizeArrayQty) {
            if (element1 === index.size) {
              const qty = myMap.get(element1) ?? 0;
              if (index.qty > qty && !check) {
                this.val = index.size;
                this.count++;
                check = true;
                break;
              }
            }
          }
        }
        if (this.val === '') {
          this.pqty = 0;
        } else {
          this.pqty = this.product.qty;
        }
      } else {
        for (const index of this.product.sizeArrayQty) {
          if (index.qty > 0) {
            this.val = index.size;
            this.count++;
            break;
          }
        }
        this.pqty = this.product.qty;
      }
    } else {
      if (!localStorage.getItem('cart')) {
        this.count = 0;

      } else {
        let check = false;
        const cart2 = JSON.parse(localStorage.getItem('cart'));
        cart2.forEach(element => {
          if (element._id === this.product._id) {
            this.count = element.qty;
            check = true;
          }
        });
        if (!check) {
          this.count = 0;
        }
      }
      this.pqty = this.product.qty;
    }

    if (this.product.productType === 'fashion') {
      if (!localStorage.getItem('cart')) {
        cart = [{ _id: id, qty: 1, size: [{ size: this.val, qty: 1 }] }];

      } else {
        let check = false;
        let check1 = false;
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === id) {
            element.size.forEach(element1 => {
              if (element1.size === this.val) {
                element.qty = element.qty + 1;
                element1.qty = element1.qty + 1;
                check1 = true;
              }
            });
            if (!check1) {
              element.qty = element.qty + 1;
              element.size.push({ size: this.val, qty: 1 });
            }
            check = true;
          }
        });
        if (!check) {
          cart.push({ _id: id, qty: 1, size: [{ size: this.val, qty: 1 }] });
        }
      }
    } else {
      if (!localStorage.getItem('cart')) {
        cart = [{ _id: id, qty: 1 }];

      } else {
        let check = false;
        cart = JSON.parse(localStorage.getItem('cart'));
        cart.forEach(element => {
          if (element._id === id) {
            element.qty = element.qty + 1;
            check = true;
          }
        });
        if (!check) {
          cart.push({ _id: id, qty: 1 });
        }
      }
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    this.eventEmitterService.onFirstComponentButtonClick();
    Swal.fire({
      title: 'Checkout?',
      text: '',
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Cart!'
    }).then((result) => {
      if (result.value) {
        this.router.navigate(['/cart']);
      }
    });
  }

  toProduct(id) {
    this.router.navigate(['/shop']).then(() => {
      this.router.navigate(['/product'], { skipLocationChange: true, queryParams: { id } });
    });
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

}

import { LookbookService } from './../service/lookbook.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  lookBooks = [];

  constructor(private router: Router, private lookBookService: LookbookService) {
    const newLocal = new Promise(() => {
      this.loadScript();
    });
    this.getAllLookBooks();
    document.getElementsByTagName('body')[0].setAttribute('style', 'overflow:visible');
  }

  ngOnInit(): void {
    $('html, body').animate({
      scrollTop: 0
    }, 600);
  }

  public loadScript() {
    const node = document.createElement('script');
    node.src = 'assets/js/scripts.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  getLookbook(id){
    // tslint:disable-next-line: object-literal-shorthand
    this.router.navigate(['one-look-book'], {queryParams: { id: id }, skipLocationChange: true});

  }

  getAllLookBooks() {
    this.lookBookService.getLookBooks().subscribe((data) => {
      this.lookBooks = [];
      // tslint:disable-next-line: no-string-literal
      this.lookBooks = data['data'];
      setTimeout(() => {
        const cw = $('.img-fluid').width();
        // tslint:disable-next-line: no-unused-expression
        cw !== 0 ? $('.img-fluid').css({ height: cw + 'px' }) : '';
      }, 200);
      setTimeout(() => {
        const cw = $('.img-fluid').width();
        // tslint:disable-next-line: no-unused-expression
        cw !== 0 ? $('.img-fluid').css({ height: cw + 'px' }) : '';
      }, 600);
      setTimeout(() => {
        const cw = $('.img-fluid').width();
        // tslint:disable-next-line: no-unused-expression
        cw !== 0 ? $('.img-fluid').css({ height: cw + 'px' }) : '';
      }, 1000);
      setTimeout(() => {
        const cw = $('.img-fluid').width();
        // tslint:disable-next-line: no-unused-expression
        cw !== 0 ? $('.img-fluid').css({ height: cw + 'px' }) : '';
      }, 1500);
      setTimeout(() => {
        const cw = $('.img-fluid').width();
        // tslint:disable-next-line: no-unused-expression
        cw !== 0 ? $('.img-fluid').css({ height: cw + 'px' }) : '';
      }, 2000);
    });
  }

}

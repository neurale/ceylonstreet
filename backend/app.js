const express = require("express");
const path = require("path");
var compression = require('compression');
const bodyprser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const config = require("./config/database");

//socket.io
const socketIo = require("socket.io");
const http = require("http");

// conect database
mongoose.connect(config.uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on("connected", () => {
  console.log("connected to database" + config.uri);
});

mongoose.connection.on("erorr", (err) => {
  console.log("erorr");
});

const app = express();

// compress all responses
app.use(compression())

const users = require("./routes/users");
const products = require("./routes/product");
const collections = require("./routes/collection");
const orders = require("./routes/order");
const categories = require("./routes/category");
const blogs = require("./routes/blog");
const homeMeta = require("./routes/home-page-meta");
const brands = require("./routes/brand");
const promoCode = require("./routes/promo-code");
const lookBooks = require("./routes/look-book");

const port = process.env.PORT || 3000;

//cors middlware
app.use(cors());

// set static folder
app.use(express.static(path.join(__dirname, "public")));

//bodypreser middlware
app.use(bodyprser.json());

//passport middlware
app.use(passport.initialize());
app.use(passport.session());

require("./config/passport")(passport);

app.use("/api/users", users);
app.use("/api/products", products);
app.use("/api/collections", collections);
app.use("/api/categories", categories);
app.use("/api/blogs", blogs);
app.use("/api/orders", orders);
app.use("/api/homeMeta", homeMeta);
app.use("/api/brands", brands);
app.use("/api/promoCode", promoCode);
app.use("/api/lookBooks", lookBooks);


app.get("/", (req, res) => {
  res.send("hi");
});

app.get('*', function (req, res) {
    res.sendfile('./public/index.html');
});

const server = app.listen(port, () => {
  console.log("server start on " + port);
});

var io = require("socket.io").listen(server);

app.set("io", io);

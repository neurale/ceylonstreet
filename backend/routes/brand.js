const express = require("express");
const router = express.Router();
const Brand = require("../models/brand");

router.post("/add", (req, res) => {
  const io = req.app.get("io");

  let newBrand = new Brand({
    name: req.body.name,
  });

  Brand.addBrand(newBrand, (err, brand) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    } else {
      res.json({ data: brand, success: true, msg: "Success" });
      io.emit("add-brands");
    }
  });
});

router.get("/brand", (req, res) => {
  Brand.getAllBrands((err, brands) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: brands,
        success: true,
        msg: "all brands",
      });
    }
  });
});

router.delete("/brand/:_id", (req, res) => {
  const io = req.app.get("io");
  Brand.deleteBrand(req.params._id, (err, brand) => {
    if (err) {
      res.json({
        data: err, 
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: brand,
        success: true,
        msg: "delete brand",
      });
      io.emit("delete-brand");
    }
  });
});

router.post("/brand", (req, res) => {
  const io = req.app.get("io");
  let editBrand = Brand({
    _id: req.body._id,
    name: req.body.name,
    __v: req.body.__v,
  });
  Brand.editBrand(editBrand, (err, brand) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: brand,
        success: true,
        msg: "edit brand",
      });
      io.emit("edit-brand");
    }
  });
});

module.exports = router;

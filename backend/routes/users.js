const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const config = require("../config/database");
const User = require("../models/user");

//signup
router.post("/signup", (req, res) => {
  let newUser;
  if (req.body.serive === "FACEBOOK") {
    newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      fbtoken: req.body.token,
    });
  } else {
    newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      googletoken: req.body.token,
    });
  }
  const email = req.body.email;
  const token = req.body.token;

  User.getUserBYEmail(email, (err, user) => {
    if (err) {
      throw err;
    }
    if (user == null) {
      User.addUser(newUser, req.body.serive, (err, nuser) => {
        if (err) {
          return res.json({
            data: err,
            success: false,
            msg: "Faild to register user",
          });
        } else {
          const userToken = jwt.sign(
            {
              _id: nuser._id,
              firstName: nuser.firstName,
              lastName: nuser.lastName,
              email: nuser.email,
            },
            config.secret,
            {
              expiresIn: 604500,
            }
          );

          return res.json({
            success: true,
            userToken: "JWT" + userToken,
            user: {
              email: nuser.email,
              firstName: nuser.firstName,
              lastName: nuser.lastName,
            },
          });
        }
      });
    } else {
      if (
        (req.body.serive === "FACEBOOK" && user.fbtoken !== undefined) ||
        (req.body.serive !== "FACEBOOK" && user.googletoken !== undefined)
      ) {
        let dbtoken =
          req.body.serive === "FACEBOOK" ? user.fbtoken : user.googletoken;
        User.compareToken(token, dbtoken, (err, isMatch) => {
          if (err) {
            throw err;
          }
          if (isMatch) {
            const userToken = jwt.sign(
              {
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
              },
              config.secret,
              {
                expiresIn: 604500,
              }
            );

            return res.json({
              success: true,
              userToken: "JWT" + userToken,
              user: {
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
              },
            });
          } else {
            return res.json({ success: false, msg: "token is invalid" });
          }
        });
      } else if (req.body.serive === "FACEBOOK" && user.fbtoken === undefined) {
        User.updateUserfb(email, token, (err, user) => {
          if (err) {
            return res.json({
              data: err,
              success: false,
              msg: "Faild to register user",
            });
          } else {
            const userToken = jwt.sign(
              {
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
              },
              config.secret,
              {
                expiresIn: 604500,
              }
            );

            return res.json({
              success: true,
              userToken: "JWT" + userToken,
              user: {
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
              },
            });
          }
        });
      } else if (
        req.body.serive !== "FACEBOOK" &&
        user.googletoken === undefined
      ) {
        User.updateUsergoogle(email, token, (err, user) => {
          if (err) {
            return res.json({
              data: err,
              success: false,
              msg: "Faild to register user",
            });
          } else {
            const userToken = jwt.sign(
              {
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
              },
              config.secret,
              {
                expiresIn: 604500,
              }
            );

            return res.json({
              success: true,
              userToken: "JWT" + userToken,
              user: {
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
              },
            });
          }
        });
      }
    }
  });
});

// authenticate person
router.post("/authenticate", (req, res) => {
  const email = req.body.email;
  const token = req.body.token;
  User.getUserBYEmail(email, (err, user) => {
    if (err) {
      throw err;
    }
    if (!user) {
      return res.json({ success: false, msg: "User not found" });
    }
    User.compareToken(token, user.token, (err, isMatch) => {
      if (err) {
        throw err;
      }
      if (isMatch) {
        const userToken = jwt.sign(
          {
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
          },
          config.secret,
          {
            expiresIn: 604500,
          }
        );

        return res.json({
          success: true,
          userToken: "JWT" + userToken,
          user: {
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
          },
        });
      } else {
        return res.json({ success: false, msg: "token is invalid" });
      }
    });
  });
});

router.get("/profile",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({ user: req.user });
  }
);

router.get("/validate", (req, res) => {
  res.send("validate");
});

router.get("/user", (req, res) => {
  User.getAllUsers((err, users) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: users,
        success: true,
        msg: "all users",
      });
    }
  });
});

module.exports = router;

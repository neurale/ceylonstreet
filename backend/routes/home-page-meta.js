const express = require("express");
const router = express.Router();
const HomeMeta = require("../models/home-page-meta");

//add
router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newMetaData = new HomeMeta({
    homeBanner1: req.body.homeBanner1,
    homeBanner2: req.body.homeBanner2,
    homeBanner3: req.body.homeBanner3,
    homeBanner4: req.body.homeBanner4,
    videoBanner: req.body.videoBanner,
    productBanner1: req.body.productBanner1,
    productBanner2: req.body.productBanner2,
    productBanner3: req.body.productBanner3,
    productBanner4: req.body.productBanner4,
    homeBanner1Mobile: req.body.homeBanner1Mobile,
    homeBanner2Mobile: req.body.homeBanner2Mobile,
    homeBanner3Mobile: req.body.homeBanner3Mobile,
    homeBanner4Mobile: req.body.homeBanner4Mobile,
  });

  HomeMeta.addMetaData(newMetaData, (err, metaData) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    } else {
      res.json({ data: metaData, success: true, msg: "Success" });
      io.emit("add-meta-data");
    }
  });
});

router.get("/homeMetaSchema", (req, res) => {

  HomeMeta.getMetaData((err, homeMeta) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: homeMeta,
        success: true,
        msg: "home meta data",
      });
    }
  });
});

router.post("/homeMetaSchema", (req, res) => {
  let editMetaData = HomeMeta({
    _id: req.body._id,
    homeBanner1: req.body.homeBanner1,
    homeBanner2: req.body.homeBanner2,
    homeBanner3: req.body.homeBanner3,
    homeBanner4: req.body.homeBanner4,
    videoBanner: req.body.videoBanner,
    productBanner1: req.body.productBanner1,
    productBanner2: req.body.productBanner2,
    productBanner3: req.body.productBanner3,
    productBanner4: req.body.productBanner4,
    homeBanner1Mobile: req.body.homeBanner1Mobile,
    homeBanner2Mobile: req.body.homeBanner2Mobile,
    homeBanner3Mobile: req.body.homeBanner3Mobile,
    homeBanner4Mobile: req.body.homeBanner4Mobile,
    __v: req.body.__v,
  });
  HomeMeta.editMetaData(editMetaData, (err, homemeta) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: homemeta,
        success: true,
        msg: "edit meta data",
      });
    }
  });
});

module.exports = router;

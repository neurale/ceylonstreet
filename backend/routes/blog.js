const express = require("express");
const router = express.Router();
const Blog = require("../models/blog");

//Add blog

router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newBlog = new Blog({
    title: req.body.title,
    description: req.body.description,
    content: req.body.content,
    picUrl: req.body.picUrl,
  });

  Blog.addBlog(newBlog, (err, blog) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    }
    {
      res.json({ data: blog, success: true, msg: "Success" });
      io.emit("add-blog");
    }
  });
});

//View all blogs

router.get("/blog", (req, res) => {
  Blog.getAllBlogs((err, blogs) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: blogs,
        success: true,
        msg: "all blog",
      });
    }
  });
});

//delete blog
router.delete("/delete", (req, res) => {
  const io = req.app.get("io");

  Blog.deleteBlog(req.body._id, (err, blog) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: blog,
        success: true,
        msg: "delete blog",
      });
      io.emit("delete-blog");
    }
  });
});

//blog get by id
router.get("/blogbyid", (req, res) => {
  Blog.getBlogById(req.body._id, (err, blog) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: blog,
        success: true,
        msg: "get blog",
      });
    }
  });
});

//blog get by name
router.get("/blogbyname", (req, res) => {
  Blog.getBlogBYName(req.body.name, (err, blog) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: blog,
        success: true,
        msg: "get blog",
      });
    }
  });
});

// edit blog
router.post("/blog", (req, res) => {
  const io = req.app.get("io");
  let editBlog = Blog({
    _id: req.body._id,
    name: req.body.name,
    __v: req.body.__v,
  });
  Blog.editProduct(editBlog, (err, blog) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: blog,
        success: true,
        msg: "edit blog",
      });
      io.emit("edit-blog");
    }
  });
});

module.exports = router;

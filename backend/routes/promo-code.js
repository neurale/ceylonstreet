const express = require("express");
const router = express.Router();
const PromoCode = require("../models/promo-code");

//Add PromoCode

router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newPromoCode = new PromoCode({
    couponName: req.body.couponName,
    discount: req.body.discount,
    minLimit: req.body.minLimit,
    maxLimit: req.body.maxLimit,
  });

  PromoCode.addPromoCode(newPromoCode, (err, PromoCode) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    } else {
      res.json({ data: PromoCode, success: true, msg: "Success" });
      io.emit("add-PromoCode");
    }
  });
});

//view all PromoCodes

router.get("/promoCode", (req, res) => {
  PromoCode.getAllPromoCodes((err, promoCodes) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: promoCodes,
        success: true,
        msg: "all PromoCode",
      });
    }
  });
});

router.get("/promoCodeById/:_id", (req, res) => {
  PromoCode.getPromoCodeById(req.params._id, (err, promoCode) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: promoCode,
        success: true,
        msg: "get PromoCode",
      });
    }
  });
});

router.get("/promoCodeByName/:name", (req, res) => {
  PromoCode.getPromoCodeBYName(req.params.name, (err, promoCode) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: promoCode,
        success: true,
        msg: "get PromoCode",
      });
    }
  });
});

router.delete("/promoCode/:_id", (req, res) => {
  const io = req.app.get("io");
  PromoCode.deletePromoCode(req.params._id, (err, promoCode) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: promoCode,
        success: true,
        msg: "delete PromoCode",
      });
      io.emit("delete-PromoCode");
    }
  });
});

router.post("/promoCode", (req, res) => {
  const io = req.app.get("io");
  let editPromoCode = PromoCode({
    _id: req.body._id,
    couponName: req.body.couponName,
    discount: req.body.discount,
    minLimit: req.body.minLimit,
    maxLimit: req.body.maxLimit,
    __v: req.body.__v,
  });
  PromoCode.editPromoCode(editPromoCode, (err, promoCode) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: promoCode,
        success: true,
        msg: "edit PromoCode",
      });
      io.emit("edit-PromoCode");
    }
  });
});

module.exports = router;

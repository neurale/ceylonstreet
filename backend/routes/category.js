const express = require("express");
const router = express.Router();
const Category = require("../models/category");

// Add category


router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newCategory = new Category({
    name: req.body.name,
  });

  Category.addCategory(newCategory, (err, category) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    }
    {
      res.json({ data: category, success: true, msg: "Success" });
      io.emit("add-category");
    }
  });
});

//View all products

router.get("/category", (req, res) => {
  Category.getAllCategorys((err, categorys) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: categorys,
        success: true,
        msg: "all category",
      });
    }
  });
});

router.delete("/delete", (req, res) => {
  const io = req.app.get("io");

  Category.deleteCategory(req.body._id, (err, category) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: category,
        success: true,
        msg: "delete category",
      });
      io.emit("delete-category");
    }
  });
});

router.get("/categorybyid", (req, res) => {
  Category.getCategoryById(req.body._id, (err, category) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: category,
        success: true,
        msg: "get category",
      });
    }
  });
});

router.get("/categorybyname", (req, res) => {
  Category.getCategoryBYName(req.body.name, (err, category) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: category,
        success: true,
        msg: "get category",
      });
    }
  });
});

router.post("/category", (req, res) => {
  const io = req.app.get("io");
  let editProduct = Category({
    _id: req.body._id,
    name: req.body.name,
    __v: req.body.__v,
  });
  Category.editProduct(editCategory, (err, category) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: category,
        success: true,
        msg: "edit category",
      });
      io.emit("edit-category");
    }
  });
});

module.exports = router;

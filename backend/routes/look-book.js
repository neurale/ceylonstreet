const express = require("express");
const router = express.Router();
const LookBook = require("../models/look-book");

//Add lookBook

router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newLookBook = new LookBook({
    picUrl: req.body.picUrl,
    sliders: req.body.sliders,
  });

  LookBook.addLookBook(newLookBook, (err, lookBook) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    }
    {
      res.json({ data: lookBook, success: true, msg: "Success" });
      io.emit("add-lookBook");
    }
  });
});

//View all lookBooks

router.get("/lookBook", (req, res) => {
  LookBook.getAllLookBooks((err, lookBooks) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: lookBooks,
        success: true,
        msg: "all lookBook",
      });
    }
  });
});

//delete lookBook
router.delete("/delete", (req, res) => {
  const io = req.app.get("io");

  LookBook.deleteLookBook(req.body._id, (err, lookBook) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: lookBook,
        success: true,
        msg: "delete lookBook",
      });
      io.emit("delete-lookBook");
    }
  });
});

//lookBook get by id
router.get("/lookBookbyid/:_id", (req, res) => {
  LookBook.getLookBookById(req.params._id, (err, lookBook) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: lookBook,
        success: true,
        msg: "get lookBook",
      });
    }
  });
});

//lookBook get by name
router.get("/lookBookbyname", (req, res) => {
  LookBook.getLookBookBYName(req.body.name, (err, lookBook) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: lookBook,
        success: true,
        msg: "get lookBook",
      });
    }
  });
});

// edit lookBook
router.post("/lookBook", (req, res) => {
  const io = req.app.get("io");
  let editLookBook = LookBook({
    _id: req.body._id,
    picUrl: req.body.picUrl,
    sliders: req.body.sliders,
    __v: req.body.__v,
  });
  LookBook.editProduct(editLookBook, (err, lookBook) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: lookBook,
        success: true,
        msg: "edit lookBook",
      });
      io.emit("edit-lookBook");
    }
  });
});

module.exports = router;

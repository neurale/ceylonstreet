const express = require("express");
const router = express.Router();
const Product = require("../models/product");

//Add product

router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    productPrice: req.body.productPrice,
    productCostPrice: req.body.productCostPrice,
    saleOfferPrice: req.body.saleOfferPrice,
    picUrl: req.body.picUrl,
    productType: req.body.productType,
    itemCollection: req.body.itemCollection,
    itemBrand: req.body.itemBrand,
    qty: req.body.qty,
    // colors: req.body.colors,
    size: req.body.size,
    itemWeight: req.body.itemWeight,
    sizeArrayQty: req.body.sizeArrayQty,
    sizeArray: req.body.sizeArray,

    subProductType: req.body.subProductType,
    category: req.body.category,

    gemstone: req.body.gemstone,
    birthstone: req.body.birthstone,

    metal: req.body.metal,
    gemstoneWeight: req.body.gemstoneWeight,
    gemstoneCut: req.body.gemstoneCut,
    rodiumPlated: req.body.rodiumPlated,
    measurement: req.body.measurement,

    necklaceLength: req.body.necklaceLength,

    species: req.body.species,
    variety: req.body.variety,
    weight: req.body.weight,
    cut: req.body.cut,
    treatments: req.body.treatments,

    grams: req.body.grams,



    inDemand: req.body.inDemand,
    newArrival: req.body.newArrival,
  });

  Product.addProduct(newProduct, (err, product) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    } else {
      res.json({ data: product, success: true, msg: "Success" });
      io.emit("add-product");
    }
  });
});

//view all products

router.get("/product", (req, res) => {
  Product.getAllProducts((err, products) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: products,
        success: true,
        msg: "all product",
      });
    }
  });
});

router.get("/productbyid/:_id", (req, res) => {
  Product.getProductById(req.params._id, (err, product) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: product,
        success: true,
        msg: "get product",
      });
    }
  });
});

router.get("/productbyname/:name", (req, res) => {
  Product.getProductBYName(req.params.name, (err, product) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: product,
        success: true,
        msg: "get product",
      });
    }
  });
});

router.delete("/product/:_id", (req, res) => {
  const io = req.app.get("io");
  Product.deleteProduct(req.params._id, (err, product) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: product,
        success: true,
        msg: "delete product",
      });
      io.emit("delete-product");
    }
  });
});

router.post("/product", (req, res) => {
  const io = req.app.get("io");
  let editProduct = Product({
    _id: req.body._id,
    name: req.body.name,
    description: req.body.description,
    productPrice: req.body.productPrice,
    productCostPrice: req.body.productCostPrice,
    saleOfferPrice: req.body.saleOfferPrice,
    picUrl: req.body.picUrl,
    productType: req.body.productType,
    itemCollection: req.body.itemCollection,
    itemBrand: req.body.itemBrand,
    qty: req.body.qty,
    itemWeight: req.body.itemWeight,
    sizeArrayQty: req.body.sizeArrayQty,
    // colors: req.body.colors,
    size: req.body.size,
    sizeArray: req.body.sizeArray,

    subProductType: req.body.subProductType,
    category: req.body.category,

    gemstone: req.body.gemstone,
    birthstone: req.body.birthstone,

    metal: req.body.metal,
    gemstoneWeight: req.body.gemstoneWeight,
    gemstoneCut: req.body.gemstoneCut,
    rodiumPlated: req.body.rodiumPlated,
    measurement: req.body.measurement,

    necklaceLength: req.body.necklaceLength,

    species: req.body.species,
    variety: req.body.variety,
    weight: req.body.weight,
    cut: req.body.cut,
    treatments: req.body.treatments,

    grams: req.body.grams,

    inDemand: req.body.inDemand,
    newArrival: req.body.newArrival,
    __v: req.body.__v,
  });
  Product.editProduct(editProduct, (err, product) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: product,
        success: true,
        msg: "edit product",
      });
      io.emit("edit-product");
    }
  });
});

module.exports = router;

const express = require("express");
const router = express.Router();
const Order = require("../models/order");
const Product = require("../models/product");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const config = require("../config/database");
const sgMail = require("@sendgrid/mail");
const axios = require("axios");
const fetch = require('node-fetch');

//Add order

// router.post("/add", (req, res) => {
//   const io = req.app.get("io");
//   let newOrder = new Order({
//     name: req.body.name,
//     email: req.body.email,
//     phone: req.body.phone,
//     country: req.body.country,
//     address: req.body.address,
//     postalcode: req.body.postalcode,
//     total: req.body.total,
//     fixtotal: req.body.fixtotal,
//     coupon: req.body.coupon,
//     items: req.body.items,
//     status: req.body.status,
//     date: new Date(),
//     profit: req.body.profit,
//   });

//   Order.addOrder(newOrder, (err, order) => {
//     if (err) {
//       res.json({ data: err, success: false, msg: "Error" });
//     } else {
//     //   sgMail.setApiKey(
//     //     `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`
//     //   );
//     //   const msg = {
//     //     to: req.body.email, // Change to your recipient
//     //     from: "support@azeiya.com", // Change to your verified sender
//     //     subject: "Order Placed",
//     //     templateId: "d-df37f737172a4be0904d3930155e3c84",
//     //     dynamic_template_data: {
//     //       subject: "Azeiya Order Placed",
//     //       name: req.body.name,
//     //       items: req.body.items,
//     //       total: req.body.total,
//     //       discount:
//     //         ((req.body.fixtotal - req.body.total) / req.body.fixtotal) * 100,
//     //       orderId: order._id,
//     //     },
//     //   };
//     //   sgMail.send(msg);


//       // .then(() => {
//       //   console.log("Email sent");
//       // })
//       // .catch((error) => {
//       //   console.error(error);
//       // });

//       axios.post("https://app.notify.lk/api/v1/send", {
//         user_id: "12812",
//         api_key: "VZrpZeIq0iTRwDziKopG",
//         sender_id: "NotifyDEMO",
//         to: req.body.phone,
//         message:
//           "Your new order placed. Order values is " +
//           req.body.total +
//           " and Order Id is " +
//           order._id +
//           ". Thank you!",
//       });

//       axios.post("https://app.notify.lk/api/v1/send", {
//         user_id: "12812",
//         api_key: "VZrpZeIq0iTRwDziKopG",
//         sender_id: "NotifyDEMO",
//         to: "94774048045",
//         message:
//           "New order placed. Order values is " +
//           req.body.total +
//           " and Order Id is " +
//           order._id +
//           ".",
//       });

//       // .then((res) => {
//       //   console.log(`statusCode: ${res.statusCode}`);
//       //   console.log(res);
//       // })
//       // .catch((error) => {
//       //   console.error(error);
//       // });
//       Product.updateQtyProduct(newOrder.items, () => {
//         io.emit("add-order");
//       });
//       res.json({ data: order, success: true, msg: "Success" });
//     }
//   });
// });


router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newOrder = new Order({
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone,
    country: req.body.country,
    address: req.body.address,
    postalcode: req.body.postalcode,
    total: req.body.total,
    fixtotal: req.body.fixtotal,
    shiping: req.body.shiping,
    coupon: req.body.coupon,
    items: req.body.items,
    status: req.body.status,
    date: new Date(),
    profit: req.body.profit,
    valid: '',
    successIndicator:'',
  });

  Order.addOrder(newOrder, (err, order) => {
    if (err) {
      console.log(err);
      res.json({ data: err, success: false, msg: "Error" });
    } else {

      urlencoded = new URLSearchParams();
      url = "https://cbcmpgs.gateway.mastercard.com/api/nvp/version/56"
      urlencoded.append("apiOperation", "CREATE_CHECKOUT_SESSION");
      urlencoded.append("apiPassword", "2b3c582ca302ab1f36e172463c90465d");
      urlencoded.append("apiUsername", "merchant.CEYLONSTRLKR");
      urlencoded.append("merchant", "CEYLONSTRLKR");
      urlencoded.append("order.id", order._id);
      urlencoded.append("order.amount", req.body.lkr);
      urlencoded.append("order.currency", "LKR");
      urlencoded.append("order.description", "order_" + order._id);
      urlencoded.append("interaction.operation", "PURCHASE");
      urlencoded.append("interaction.merchant.name", "CEYLONST");
      urlencoded.append("interaction.returnUrl", "https://ceylonstreet.com/checkout");
      
        fetch(url, {
            method: 'POST',
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            body: urlencoded,
            redirect: 'follow'
          })
          .then(response => response.text())
          .then(result => {
            let searchIndex = result.indexOf('SESSION');
            let searchSubStr = result.substr(searchIndex, 31);
            let searchIndex1 = result.indexOf('successIndicator');
            let searchSubStr1 = result.substr(searchIndex1, 33).split('=')[1];
            let searchIndex2 = result.indexOf('session.version');
            let searchSubStr2 = result.substr(searchIndex2, 26).split('=')[1];
            Order.editValid(order._id, searchSubStr1, (err, order) => {
              if (err) {
                res.json({
                  data: err,
                  success: false,
                  msg: "failed",
                });
              } else {
                res.json({ data: order, sesionID: searchSubStr,orderID: order._id,success: true, msg: "Success" });
                io.emit("add-order");
              }
            });
          })
          .catch(error => console.log('error', error));
    }
  });
});


//view all orders

router.get("/order", (req, res) => {
  Order.getAllOrders((err, orders) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: orders,
        success: true,
        msg: "all order",
      });
    }
  });
});

router.post("/valid", (req, res) => {
  const io = req.app.get("io");
  Order.getOrderById(req.body._id, (err, order) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      if(order.successIndicator === req.body.resultIndicator){
        Order.validate(req.body._id,true,(err, order1) => {
          if (err) {
            res.json({
              data: err,
              success: false,
              msg: "failed",
            });
          } else {
            //   sgMail.setApiKey(
            //     `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`
            //   );
            //   const msg = {
            //     to: req.body.email, // Change to your recipient
            //     from: "support@azeiya.com", // Change to your verified sender
            //     subject: "Order Placed",
            //     templateId: "d-df37f737172a4be0904d3930155e3c84",
            //     dynamic_template_data: {
            //       subject: "Azeiya Order Placed",
            //       name: req.body.name,
            //       items: req.body.items,
            //       total: req.body.total,
            //       discount:
            //         ((req.body.fixtotal - req.body.total) / req.body.fixtotal) * 100,
            //       orderId: order._id,
            //     },
            //   };
            //   sgMail.send(msg);


              // .then(() => {
              //   console.log("Email sent");
              // })
              // .catch((error) => {
              //   console.error(error);
              // });
            try {
              axios.post("https://app.notify.lk/api/v1/send", {
                user_id: "12812",
                api_key: "VZrpZeIq0iTRwDziKopG",
                sender_id: "NotifyDEMO",
                to: "94774048045",
                message:
                  "New order placed. Order values is " +
                  req.body.total +
                  " and Order Id is " +
                  order._id +
                  ".",
              });
            } catch (t) {
              console.log(t)
            }

              // axios.post("https://app.notify.lk/api/v1/send", {
              //   user_id: "12812",
              //   api_key: "VZrpZeIq0iTRwDziKopG",
              //   sender_id: "NotifyDEMO",
              //   to: req.body.phone,
              //   message:
              //     "Your new order placed. Order values is " +
              //     req.body.total +
              //     " and Order Id is " +
              //     order._id +
              //     ". Thank you!",
              // });

              

              // .then((res) => {
              //   console.log(`statusCode: ${res.statusCode}`);
              //   console.log(res);
              // })
              // .catch((error) => {
              //   console.error(error);
              // });
              Product.updateQtyProduct(order.items, () => {
                io.emit("add-order");
              });
            res.json({
              data: order,
              success: true,
              msg: "get order",
            });
          }
        })
      } else {
        Order.validate(req.body._id,false,(err, order1) => {
          if (err) {
            res.json({
              data: err,
              success: false,
              msg: "failed",
            });
          } else {
            res.json({
              data: '',
              success: false,
              msg: "order not valid",
            });
          }
        })
      }
      
    }
  });
});




router.get("/orderbyid/:_id", (req, res) => {
  Order.getOrderById(req.params._id, (err, order) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: order,
        success: true,
        msg: "get order",
      });
    }
  });
});

router.get(
  "/orderbyname/:email",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Order.getOrderBYEmail(req.params.email, (err, order) => {
      if (err) {
        res.json({
          data: err,
          success: false,
          msg: "failed",
        });
      } else {
        res.json({
          data: order,
          success: true,
          msg: "get order",
        });
      }
    });
  }
);

router.delete("/order/:_id", (req, res) => {
  const io = req.app.get("io");
  Order.deleteOrder(req.params._id, (err, order) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: order,
        success: true,
        msg: "delete order",
      });
      io.emit("delete-order");
    }
  });
});

router.post("/order", (req, res) => {
  const io = req.app.get("io");
  
  Order.editOrder(req.body._id, req.body.status, (err, order) => {
    if (err) {
      
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      
      if (req.body.status == "shipped") {
        axios.post("https://app.notify.lk/api/v1/send", {
          user_id: "12812",
          api_key: "VZrpZeIq0iTRwDziKopG",
          sender_id: "NotifyDEMO",
          to: req.body.phone,
          message: "Your item has been shipped. Order id is " + req.body._id,
        });
      }

      res.json({
        data: order,
        success: true,
        msg: "edit order",
      });
      io.emit("edit-order");
    }
  });
});

module.exports = router;

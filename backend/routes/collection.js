const express = require("express");
const router = express.Router();
const Collection = require("../models/collection");

//Add collection

router.post("/add", (req, res) => {
  const io = req.app.get("io");
  let newCollection = new Collection({
    name: req.body.name,
    // description: req.body.description,
    picUrl: req.body.picUrl,
  });

  Collection.addCollection(newCollection, (err, collection) => {
    if (err) {
      res.json({ data: err, success: false, msg: "Error" });
    }
    {
      res.json({ data: collection, success: true, msg: "Success" });
      io.emit("add-collection");
    }
  });
});

//View all products

router.get("/collection", (req, res) => {
  Collection.getAllCollections((err, collections) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: collections,
        success: true,
        msg: "all collection",
      });
    }
  });
});

router.delete("/delete/:_id", (req, res) => {
  const io = req.app.get("io");

  Collection.deleteCollection(req.params._id, (err, collection) => {
    if (err) { 
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: collection,
        success: true,
        msg: "delete collection",
      });
      io.emit("delete-collection");
    }
  });
});

router.get("/collectionbyid", (req, res) => {
  Collection.getCollectionById(req.body._id, (err, collection) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: collection,
        success: true,
        msg: "get collection",
      });
    }
  });
});

router.get("/collectionbyname", (req, res) => {
  Collection.getCollectionBYName(req.body.name, (err, collection) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: collection,
        success: true,
        msg: "get collection",
      });
    }
  });
});

router.post("/collection", (req, res) => {
  const io = req.app.get("io");
  let editCollection = Collection({
    _id: req.body._id,
    name: req.body.name,
    __v: req.body.__v,
  });
  Collection.editCollection(editCollection, (err, collection) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      }); 
    } else {
      res.json({
        data: collection,
        success: true,
        msg: "edit collection",
      });
      io.emit("edit-collection");
    }
  });
});

module.exports = router;

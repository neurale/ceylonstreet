const mongoose = require("mongoose");
const config = require("../config/database");

const OrderSchema = mongoose.Schema({
  name: { type: String },
  email: { type: String },
  phone: { type: String },
  country: { type: String },
  address: { type: String },
  postalcode: { type: String },
  total: { type: String },
  fixtotal: { type: String },
  shiping: { type: String },
  coupon: { type: String },
  items: [
    {
      item_id: {
        type: String,
      },
      item_name: {
        type: String,
      },
      qty: {
        type: String,
      },
      size: {
        type: [{
          size:{
            type: String,
          },
          qty: {
            type: String,
          },
        }],
        default: [],
      },
      price: {
        type: String,
      },
      picUrl: {
        type: String,
      },
    },
  ],
  sessionVersion:{ type: String },
  successIndicator:{ type: String },
  valid:{ type: String },
  status: { type: String },
  date: { type: String },
  profit: { type: String },
});

const Order = (module.exports = mongoose.model("Order", OrderSchema));

module.exports.getAllOrders = function (callback) {
  Order.find(callback);
};

module.exports.getOrderById = function (id, callback) {
  Order.findById(id, callback);
};

module.exports.getOrderBYEmail = function (email, callback) {
  const query = {
    email: email,
    valid:'true'
  };
  Order.find(query, callback);
};

module.exports.addOrder = function (newOrder, callback) {
  newOrder.save(callback);
};

module.exports.deleteOrder = function (id, callback) {
  Order.findByIdAndRemove(id, callback);
};

module.exports.editOrder = function (id, status, callback) {
  Order.findByIdAndUpdate(id, { status: status }, callback);
};

module.exports.editValid = function (id, successIndicator, callback) {
  Order.findByIdAndUpdate(id, { successIndicator: successIndicator  }, callback);
};

module.exports.validate = function (id, valid, callback) {
  Order.findByIdAndUpdate(id, { valid: valid+'' }, callback);
};


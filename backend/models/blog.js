const mongoose = require("mongoose");
const config = require("../config/database");

const BlogSchema = mongoose.Schema({
  title: { type: String },
  description: { type: String },
  content: { type: String },
  picUrl: [{ type: String }],
});

const Blog = (module.exports = mongoose.model("Blog", BlogSchema));

module.exports.addBlog = function (newBlog, callback) {
  newBlog.save(callback);
};

module.exports.getAllBlogs = function (callback) {
  Blog.find(callback);
};

module.exports.deleteBlog = function (id, callback) {
  Blog.findByIdAndRemove(id, callback);
};

module.exports.getCollectonById = function (id, callback) {
  Blog.findById(id, callback);
};
module.exports.getBlogBYName = function (name, callback) {
  const query = {
    name: name,
  };
  Blog.findOne(query, callback);
};

module.exports.editBlog = function (Blog, callback) {
  Blog.findByIdAndUpdate(Blog._id, { $set: Blog }, callback);
};

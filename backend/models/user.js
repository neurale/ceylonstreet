const mongoose = require("mongoose");
const bcrpt = require("bcryptjs");
const config = require("../config/database");
const e = require("express");

// user schema
const UserSchhema = mongoose.Schema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  fbtoken: {
    type: String,
  },
  googletoken: {
    type: String,
  },
});

const User = (module.exports = mongoose.model("User", UserSchhema));

module.exports.getUserById = function (id, callback) {
  User.findById(id, callback);
};

module.exports.getAllUsers = function (callback) {
  User.find(callback);
};

module.exports.getUserBYEmail = function (email, callback) {
  const query = { email: email };
  User.findOne(query, callback);
};

module.exports.addUser = function (newUser, service, callback) {
  let token = service === "FACEBOOK" ? newUser.fbtoken : newUser.googletoken;
  bcrpt.genSalt(10, (err, salt) => {
    if (err) {
      throw err;
    }
    bcrpt.hash(token, salt, (err, hash) => {
      if (err) throw err;
      if (service === "FACEBOOK") {
        newUser.fbtoken = hash;
      } else {
        newUser.googletoken = hash;
      }

      newUser.save(callback);
    });
  });
};

module.exports.updateUserfb = function (email, token, callback) {
  bcrpt.genSalt(10, (err, salt) => {
    if (err) {
      throw err;
    }
    bcrpt.hash(token, salt, (err, hash) => {
      if (err) throw err;
      token = hash;
      User.findOneAndUpdate({ email: email }, { fbtoken: token }, callback);
    });
  });
};

module.exports.updateUsergoogle = function (email, token, callback) {
  bcrpt.genSalt(10, (err, salt) => {
    if (err) {
      throw err;
    }
    bcrpt.hash(token, salt, (err, hash) => {
      if (err) throw err;
      token = hash;
      User.findOneAndUpdate({ email: email }, { googletoken: token }, callback);
    });
  });
};

module.exports.compareToken = function (candidateToken, hash, callback) {
  bcrpt.compare(candidateToken, hash, (err, isMatch) => {
    if (err) {
      throw err;
    }
    callback(null, isMatch);
  });
};

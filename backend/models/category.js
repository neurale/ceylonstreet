const mongoose = require("mongoose");
const config = require("../config/database");

const CategorySchema = mongoose.Schema({
  name: { type: String },
});

const Category = (module.exports = mongoose.model("Category", CategorySchema));

module.exports.addCategory = function (newCategory, callback) {
  newCategory.save(callback);
};

module.exports.getAllCategorys = function (callback) {
  Category.find(callback);
};

module.exports.deleteCategory = function (id, callback) {
  Category.findByIdAndRemove(id, callback);
};

module.exports.getCollectonById = function (id, callback) {
  Category.findById(id, callback);
};
module.exports.getCategoryBYName = function (name, callback) {
  const query = {
    name: name,
  };
  Category.findOne(query, callback);
};

module.exports.editCategory = function (Category, callback) {
  Category.findByIdAndUpdate(Category._id, { $set: Category }, callback);
};

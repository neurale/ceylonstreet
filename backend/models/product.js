const mongoose = require("mongoose");
const config = require("../config/database");

const ProductSchema = mongoose.Schema({
  name: { type: String },
  description: { type: String },
  productPrice: { type: String },
  productCostPrice: { type: String },
  saleOfferPrice: { type: String },
  picUrl: [{ type: String }],
  productType: { type: String },
  itemCollection: { type: String },
  itemBrand: { type: String },
  qty: { type: String },
  // colors: { type: String },
  size: { type: String },
  itemWeight: { type: String },
  sizeArrayQty: [{ type: Object }],
  sizeArray: [{ type: String }],

  subProductType: { type: String },
  category: { type: String },

  //#########################

  gemstone: { type: String },
  birthstone: { type: String },
  //all except neckless and gemstones
  metal: { type: String },
  gemstoneWeight: { type: String },
  gemstoneCut: { type: String },
  rodiumPlated: { type: String },
  measurement: { type: String },

  //Necklace Details

  necklaceLength: { type: String },

  //Gemstone Details
  species: { type: String },
  variety: { type: String },
  weight: { type: String },
  cut: { type: String },
  treatments: { type: String },

  grams: { type: String },

  inDemand: { type: Boolean },
  newArrival: { type: Boolean },
});

const Product = (module.exports = mongoose.model("Product", ProductSchema));

module.exports.getAllProducts = function (callback) {
  Product.find(callback);
};

module.exports.getProductById = function (id, callback) {
  Product.findById(id, callback);
};

module.exports.getProductBYName = function (name, callback) {
  const query = {
    name: name,
  };
  Product.findOne(query, callback);
};

module.exports.addProduct = function (newProduct, callback) {
  newProduct.save(callback);
};

module.exports.deleteProduct = function (id, callback) {
  Product.findByIdAndRemove(id, callback);
};

module.exports.editProduct = function (product, callback) {
  Product.findByIdAndUpdate(product._id, { $set: product }, callback);
};

module.exports.updateQtyProduct = function (items, callback) {
  items.forEach(e => {
    Product.findOne({ _id: e.item_id }, async (err, res) => {

      if(e.size.length !== 0){
        await e.size.forEach(e2 => {
          res.sizeArrayQty.forEach( e1 => {
            if(e1.size === e2.size) {
              e1.qty -= e2.qty
            }
          })
        })
      }
      console.log(res.sizeArrayQty)
      
      Product.findByIdAndUpdate(e.item_id, { qty: ((+res.qty) - (+e.qty)), sizeArrayQty: res.sizeArrayQty }, ()=>{});

    })
  })
  callback(null,null);
  
};
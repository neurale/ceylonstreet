const mongoose = require("mongoose");
const config = require("../config/database");

const PromoCodeSchema = mongoose.Schema({
  couponName: { type: String },
  discount: { type: String },
  minLimit: { type: String },
  maxLimit: { type: String },
});

const PromoCode = (module.exports = mongoose.model("PromoCode", PromoCodeSchema));

module.exports.addPromoCode = function (newPromoCode, callback) {
  newPromoCode.save(callback);
};

module.exports.getAllPromoCodes = function (callback) {
  PromoCode.find(callback);
};

module.exports.deletePromoCode = function (id, callback) {
  PromoCode.findByIdAndRemove(id, callback);
};

module.exports.getPromoCodeById = function (id, callback) {
  PromoCode.findById(id, callback);
};
module.exports.getPromoCodeBYName = function (name, callback) {
  const query = {
    name: name,
  };
  PromoCode.findOne(query, callback);
};

module.exports.editPromoCode = function (coupon, callback) {
  PromoCode.findByIdAndUpdate(coupon._id, { $set: coupon }, callback);
};

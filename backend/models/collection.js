const mongoose = require("mongoose");
const config = require("../config/database");

const CollectionSchema = mongoose.Schema({
  name: { type: String },
  // description: { type: String },
  picUrl: [{ type: String }],
});

const Collection = (module.exports = mongoose.model(
  "Collection",
  CollectionSchema
));

module.exports.addCollection = function (newCollection, callback) {
  newCollection.save(callback);
};

module.exports.getAllCollections = function (callback) {
  Collection.find(callback);
};
 
module.exports.deleteCollection = function (id, callback) {
  Collection.findByIdAndRemove(id, callback);
}; 

module.exports.getCollectonById = function (id, callback) {
  Collection.findById(id, callback);
};
module.exports.getCollectionBYName = function (name, callback) {
  const query = {
    name: name,
  };
  Collection.findOne(query, callback);
};

module.exports.editCollection = function (collection, callback) {
  Collection.findByIdAndUpdate(collection._id, { $set: collection }, callback);
};

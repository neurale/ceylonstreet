const mongoose = require("mongoose");

const HomeMetaSchema = mongoose.Schema({
  homeBanner1: { type: Object },
  homeBanner2: { type: Object },
  homeBanner3: { type: Object },
  homeBanner4: { type: Object },
  videoBanner: { type: Object },
  productBanner1: { type: Object },
  productBanner2: { type: Object },
  productBanner3: { type: Object },
  productBanner4: { type: Object },
  homeBanner1Mobile: { type: Object },
  homeBanner2Mobile: { type: Object },
  homeBanner3Mobile: { type: Object },
  homeBanner4Mobile: { type: Object },
});

const HomeMeta = (module.exports = mongoose.model("HomeMeta", HomeMetaSchema));

module.exports.getMetaData = function (callback) {
  HomeMeta.find(callback);
};

module.exports.addMetaData = function (newMetaData, callback) {
  newMetaData.save(callback);
};

module.exports.editMetaData = function (homemeta, callback) {
  HomeMeta.findByIdAndUpdate(homemeta._id, { $set: homemeta }, callback);
};

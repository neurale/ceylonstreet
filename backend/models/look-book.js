const mongoose = require("mongoose");
const config = require("../config/database");

const LookBookSchema = mongoose.Schema({
  picUrl: { type: String },
  sliders: [
    {
      picUrl: { type: String },
      products: [
        {
          name: { type: String },
          id: { type: String },
          picUrl: { type: String },
        },
      ],
    },
  ],
});

const LookBook = (module.exports = mongoose.model("LookBook", LookBookSchema));

module.exports.addLookBook = function (newLookBook, callback) {
  newLookBook.save(callback);
};

module.exports.getAllLookBooks = function (callback) {
  LookBook.find(callback);
};

module.exports.deleteLookBook = function (id, callback) {
  LookBook.findByIdAndRemove(id, callback);
};

module.exports.getLookBookById = function (id, callback) {
  LookBook.findById(id, callback);
};
module.exports.getLookBookBYName = function (name, callback) {
  const query = {
    name: name,
  };
  LookBook.findOne(query, callback);
};

module.exports.editLookBook = function (LookBook, callback) {
  LookBook.findByIdAndUpdate(LookBook._id, { $set: LookBook }, callback);
};

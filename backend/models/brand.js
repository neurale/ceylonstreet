const mongoose = require('mongoose')


const BrandSchema = mongoose.Schema({
    name:{type: String}
})

const Brand = (module.exports= mongoose.model("Brand",BrandSchema))

module.exports.addBrand = function(newBrand, callback){
    newBrand.save(callback);
}

module.exports.getAllBrands = function(callback){
    Brand.find(callback)
}

module.exports.editBrand = function(brand, callback){
    Brand.findByIdAndUpdate(brand._id,{$set:brand},callback)
}

module.exports.deleteBrand = function(id,callback){
    Brand.findByIdAndRemove(id,callback)
}
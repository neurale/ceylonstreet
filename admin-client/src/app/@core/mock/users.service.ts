import { of as observableOf, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { Contacts, RecentUsers, UserData } from "../data/users";

@Injectable()
export class UserService extends UserData {
  private time: Date = new Date();

  private users = {
    nick: {
      name: "Atheeq Ifthikar",
      picture:
        "https://scontent.fcmb4-1.fna.fbcdn.net/v/t1.0-9/p960x960/90084321_1110078902665237_3953259070865014784_o.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=d2NkAe6mz6oAX_CiusX&_nc_ht=scontent.fcmb4-1.fna&_nc_tp=6&oh=25405a0e0c24342cb506a8e99c572f18&oe=5F2ECA69",
    },
    eva: { name: "Eva Moor", picture: "assets/images/eva.png" },
    jack: { name: "Jack Williams", picture: "assets/images/jack.png" },
    lee: { name: "Lee Wong", picture: "assets/images/lee.png" },
    alan: { name: "Alan Thompson", picture: "assets/images/alan.png" },
    kate: { name: "Kate Martinez", picture: "assets/images/kate.png" },
  };
  private types = {
    mobile: "mobile",
    home: "home",
    work: "work",
  };
  private contacts: Contacts[] = [
    { user: this.users.nick, type: this.types.mobile },
    { user: this.users.eva, type: this.types.home },
    { user: this.users.jack, type: this.types.mobile },
    { user: this.users.lee, type: this.types.mobile },
    { user: this.users.alan, type: this.types.home },
    { user: this.users.kate, type: this.types.work },
  ];
  private recentUsers: RecentUsers[] = [
    {
      user: this.users.alan,
      type: this.types.home,
      time: this.time.setHours(21, 12),
    },
    {
      user: this.users.eva,
      type: this.types.home,
      time: this.time.setHours(17, 45),
    },
    {
      user: this.users.nick,
      type: this.types.mobile,
      time: this.time.setHours(5, 29),
    },
    {
      user: this.users.lee,
      type: this.types.mobile,
      time: this.time.setHours(11, 24),
    },
    {
      user: this.users.jack,
      type: this.types.mobile,
      time: this.time.setHours(10, 45),
    },
    {
      user: this.users.kate,
      type: this.types.work,
      time: this.time.setHours(9, 42),
    },
    {
      user: this.users.kate,
      type: this.types.work,
      time: this.time.setHours(9, 31),
    },
    {
      user: this.users.jack,
      type: this.types.mobile,
      time: this.time.setHours(8, 0),
    },
  ];

  getUsers(): Observable<any> {
    return observableOf(this.users);
  }

  getContacts(): Observable<Contacts[]> {
    return observableOf(this.contacts);
  }

  getRecentUsers(): Observable<RecentUsers[]> {
    return observableOf(this.recentUsers);
  }
}

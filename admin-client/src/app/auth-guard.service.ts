import { Injectable } from "@angular/core";
import { LoginService } from "./login/login.service";
import { CanActivate, Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class AuthGuardService implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) {}

  canActivate() {
    if (this.loginService.isAuthenticated()) {
      console.log("user is  authenticated");
      return true;
    } else {
      console.log("user is not authenticated");

      // return true;
      this.router.navigate(["/login"]);
      return false;
    }
  }
}

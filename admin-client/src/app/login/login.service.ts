import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth";
// import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: "root",
})
export class LoginService {
  userData: Observable<firebase.User>;
  loggedIn: boolean;
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router // private toast: ToastrService
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });
  }

  signIn(email: string, password: string) {
    this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.loggedIn = true;
        // this.toast.success(
        //   "You have successfully logged in",
        //   "Login Successful!"
        // );

        this.router.navigate(["/pages"]);
      })
      .catch((error) => {
        alert(error.message);
        // this.toast.warning("Something is wrong", error.message);
      });
  }

  signOut() {
    this.loggedIn = false;
    this.afAuth.signOut();
  }

  isAuthenticated() {
    if (this.loggedIn) {
      return true;
    }
    return false;
  }
}

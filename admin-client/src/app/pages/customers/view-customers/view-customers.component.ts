import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-view-customers',
  templateUrl: './view-customers.component.html',
  styleUrls: ['./view-customers.component.scss'],
})
export class ViewCustomersComponent implements OnInit {
  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },

    // edit: {
    //   editButtonContent: '<i class="nb-edit"></i>',
    //   saveButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    //   confirmSave: true
    // },
    // delete: {
    //   deleteButtonContent: '<i class="nb-trash"></i>',
    //   confirmDelete: true
    // },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      // },
      userName: {
        title: 'User Name',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      phone: {
        title: 'Contact Number',
        type: 'string',
      },
    },
  };

  source;

  constructor() {}

  ngOnInit(): void {}
  create() {
    // this.mechanicService.create(this.form.value);
    // this.form.reset();
    this.makeToast();
  }

  makeToast() {
    // this.showToast(this.status, this.title, this.content);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      // event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    if (window.confirm('Are you sure you want to edit this?')) {
      // event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}

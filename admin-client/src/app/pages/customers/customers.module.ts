import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbTreeGridModule,
  NbAlertModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule } from '@angular/forms';
// import { AgmCoreModule } from "@agm/core";
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ViewCustomersComponent } from './view-customers/view-customers.component';
import { CustomersComponent } from './customers.component';
import { CustomerRoutingModule } from './customers-routing.module';

@NgModule({
  imports: [
    CustomerRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbAlertModule,
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbTreeGridModule,
    // AgmCoreModule,
    Ng2SmartTableModule,
  ],
  declarations: [CustomersComponent, ViewCustomersComponent],
  providers: [],
})
export class CustomersModule {}

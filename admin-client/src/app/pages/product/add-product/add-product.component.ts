import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { ProductService } from "../product.service";
import { CollectionsService } from "../../collections/collections.service";
import { BrandsService } from "../../collections/brands.service";

@Component({
  selector: "ngx-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.scss"],
})
export class AddProductComponent implements OnInit {
  pictureUrlArray = ([] = []);
  collections;
  brands;

  xs: any = true;
  s: any = true;
  m: any = true;
  l: any = true;
  xl: any = true;
  xxl: any = true;

  toggleXS(xs) {
    this.xs = xs;
  }
  toggleS(s) {
    this.s = s;
  }
  toggleM(m) {
    this.m = m;
  }
  toggleL(l) {
    this.l = l;
  }
  toggleXL(xl) {
    this.xl = xl;
  }
  toggleXXL(xxl) {
    this.xxl = xxl;
  }

  constructor(
    private productService: ProductService,
    private collectionService: CollectionsService,
    private brandService: BrandsService
  ) {
    this.collectionService.getAllcollections().subscribe((collection) => {
      // tslint:disable-next-line: no-console
      // console.log(collection);
      this.collections = collection["data"];
    });
    this.brandService.getAllBrands().subscribe((brands) => {
      // console.log(brands);

      this.brands = brands["data"];
    });
  }

  ngOnInit(): void {}

  form = new FormGroup({
    name: new FormControl("", Validators.required),
    description: new FormControl("", Validators.required),
    productPrice: new FormControl("", Validators.required),
    productCostPrice: new FormControl("", Validators.required),
    saleOfferPrice: new FormControl("", Validators.required),
    productType: new FormControl("", Validators.required),
    showcase: new FormControl(""),
    itemCollection: new FormControl("", Validators.required),
    itemBrand: new FormControl("", Validators.required),
    qty: new FormControl("", Validators.required),
    colors: new FormControl(""),
    size: new FormControl(""),
    itemWeight: new FormControl("", Validators.required),
    sizeArray: new FormControl(""),
    category: new FormControl(""),

    subProductType: new FormControl(""),

    gemstone: new FormControl(""),
    birthstone: new FormControl(""),

    metal: new FormControl(""),
    gemstoneWeight: new FormControl(""),
    gemstoneCut: new FormControl(""),
    rodiumPlated: new FormControl(""),
    measurement: new FormControl(""),

    necklaceLength: new FormControl(""),

    species: new FormControl(""),
    variety: new FormControl(""),
    weight: new FormControl(""),
    cut: new FormControl(""),
    treatments: new FormControl(""),

    grmas: new FormControl(""),
    xs: new FormControl(true),
    s: new FormControl(true),
    m: new FormControl(true),
    l: new FormControl(true),
    xl: new FormControl(true),
    xxl: new FormControl(true),

    xsQty: new FormControl(1),
    sQty: new FormControl(1),
    mQty: new FormControl(1),
    lQty: new FormControl(1),
    xlQty: new FormControl(1),
    xxlQty: new FormControl(1),
  });

  radioGroupValue = "all";
  subRadioGroupValue = "alljewelry";

  create() {
    this.form.value["picUrl"] = this.pictureUrlArray;

    const temp: string[] = [];
    var sizeArrayQty;

    if (this.form.value["productType"] == "fashion") {
      if (this.xs) temp.push("xs");
      if (this.s) temp.push("s");
      if (this.m) temp.push("m");
      if (this.l) temp.push("l");
      if (this.xl) temp.push("xl");
      if (this.xxl) temp.push("xxl");
      sizeArrayQty = [
        { size: "xs", qty: this.form.value["xsQty"] },
        { size: "s", qty: this.form.value["sQty"] },
        { size: "m", qty: this.form.value["mQty"] },
        { size: "l", qty: this.form.value["lQty"] },
        { size: "xl", qty: this.form.value["xlQty"] },
        { size: "xxl", qty: this.form.value["xxlQty"] },
      ];
    }
    this.form.value["sizeArrayQty"] = sizeArrayQty;
    this.form.value["sizeArray"] = temp;

    if (this.form.value["showcase"] == "inDemand") {
      this.form.value["inDemand"] = true;
      this.form.value["newArrival"] = false;
    } else if (this.form.value["showcase"] == "newArrival") {
      this.form.value["newArrival"] = true;
      this.form.value["inDemand"] = false;
    } else {
      this.form.value["newArrival"] = false;
      this.form.value["inDemand"] = false;
    }
    // console.log(this.form.value);
    this.productService
      .create(this.form.value)
      .toPromise()
      .then((data) => {
        console.log(data);
        this.form.reset();
        this.files = [];
        this.pictureUrlArray = [];
      });
  }

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }

  get name() {
    return this.form.get("name");
  }
  get description() {
    return this.form.get("description");
  }
  get productPrice() {
    return this.form.get("productPrice");
  }
  get productCostPrice() {
    return this.form.get("productCostPrice");
  }
  get saleOfferPrice() {
    return this.form.get("saleOfferPrice");
  }
  get productType() {
    return this.form.get("productType");
  }
  get showcase() {
    return this.form.get("showcase");
  }
  get subProductType() {
    return this.form.get("subProductType");
  }
  get colors() {
    return this.form.get("colors");
  }
  get grmas() {
    return this.form.get("grmas");
  }
  get itemCollection() {
    return this.form.get("itemCollection");
  }
  get itemBrand() {
    return this.form.get("itemBrand");
  }
  get category() {
    return this.form.get("category");
  }
  get qty() {
    return this.form.get("qty");
  }
  get size() {
    return this.form.get("size");
  }
  get itemWeight() {
    return this.form.get("itemWeight");
  }
  get metal() {
    return this.form.get("metal");
  }
  get gemstone() {
    return this.form.get("gemstone");
  }
  get birthstone() {
    return this.form.get("birthstone");
  }
  get gemstoneWeight() {
    return this.form.get("gemstoneWeight");
  }
  get gemstoneCut() {
    return this.form.get("gemstoneCut");
  }
  get rodiumPlated() {
    return this.form.get("rodiumPlated");
  }
  get measurement() {
    return this.form.get("measurement");
  }
  get necklaceLength() {
    return this.form.get("necklaceLength");
  }
  get species() {
    return this.form.get("species");
  }
  get variety() {
    return this.form.get("variety");
  }
  get weight() {
    return this.form.get("weight");
  }
  get cut() {
    return this.form.get("cut");
  }
  get treatments() {
    return this.form.get("treatments");
  }
}

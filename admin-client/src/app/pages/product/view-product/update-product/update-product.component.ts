import { Component, OnInit, Input } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ProductService } from "../../product.service";
import { CollectionsService } from "../../../collections/collections.service";
import { BrandsService } from "../../../collections/brands.service";

@Component({
  selector: "ngx-update-product",
  templateUrl: "./update-product.component.html",
  styleUrls: ["./update-product.component.scss"],
})
export class UpdateProductComponent implements OnInit {
  @Input() data;
  pictureUrlArray = ([] = []);

  collections;
  brands;

  xs: any = false;
  s: any = false;
  m: any = false;
  l: any = false;
  xl: any = false;
  xxl: any = false;

  showcaseValue;

  toggleXS(xs) {
    this.xs = xs;
  }
  toggleS(s) {
    this.s = s;
  }
  toggleM(m) {
    this.m = m;
  }
  toggleL(l) {
    this.l = l;
  }
  toggleXL(xl) {
    this.xl = xl;
  }
  toggleXXL(xxl) {
    this.xxl = xxl;
  }
  arr = ([] = []);

  constructor(
    protected ref: NbDialogRef<UpdateProductComponent>,
    private productService: ProductService,
    private collectionService: CollectionsService,
    private brandService: BrandsService
  ) {
    this.collectionService.getAllcollections().subscribe((collection) => {
      this.collections = collection["data"];
    });
    this.brandService.getAllBrands().subscribe((brands) => {
      this.brands = brands["data"];
    });
  }

  dismiss() {
    this.ref.close();
  }

  form;
  ngOnInit(): void {
    if (this.data["size"].includes("xs")) this.xs = true;
    if (this.data["size"].includes("s")) this.s = true;
    if (this.data["size"].includes("m")) this.m = true;
    if (this.data["size"].includes("l")) this.l = true;
    if (this.data["size"].includes("xl")) this.xl = true;
    if (this.data["size"].includes("xxl")) this.xxl = true;
    if (this.data["inDemand"] == true) {
      this.showcaseValue = "inDemand";
    } else if (this.data["newArrival"] == true) {
      this.showcaseValue = "newArrival";
    } else {
      this.showcaseValue = "none";
    }
    this.form = new FormGroup({
      name: new FormControl(this.data["name"], Validators.required),
      description: new FormControl(
        this.data["description"],
        Validators.required
      ),
      productPrice: new FormControl(
        this.data["productPrice"],
        Validators.required
      ),
      productCostPrice: new FormControl(
        this.data["productCostPrice"],
        Validators.required
      ),
      saleOfferPrice: new FormControl(
        this.data["saleOfferPrice"],
        Validators.required
      ),
      productType: new FormControl(
        this.data["productType"],
        Validators.required
      ),
      showcase: new FormControl(this.showcaseValue),
      itemCollection: new FormControl(
        this.data["itemCollection"],
        Validators.required
      ),
      itemBrand: new FormControl(this.data["itemBrand"], Validators.required),
      qty: new FormControl(this.data["qty"], Validators.required),
      // colors: new FormControl(this.data["colors"]),
      size: new FormControl(this.data["size"]),
      itemWeight: new FormControl(this.data["itemWeight"]),
      sizeArray: new FormControl(this.data["sizeArray"]),
      subProductType: new FormControl(this.data["subProductType"]),
      category: new FormControl(this.data["category"]),

      gemstone: new FormControl(this.data["gemstone"]),
      birthstone: new FormControl(this.data["birthstone"]),
      metal: new FormControl(this.data["metal"]),

      gemstoneWeight: new FormControl(this.data["gemstoneWeight"]),
      gemstoneCut: new FormControl(this.data["gemstoneCut"]),
      rodiumPlated: new FormControl(this.data["saleOfferPrice"]),
      measurement: new FormControl(this.data["measurement"]),
      necklaceLength: new FormControl(this.data["necklaceLength"]),
      species: new FormControl(this.data["species"]),
      variety: new FormControl(this.data["variety"]),
      weight: new FormControl(this.data["weight"]),
      cut: new FormControl(this.data["cut"]),
      treatments: new FormControl(this.data["treatments"]),
      grams: new FormControl(this.data["grams"]),
      xs: new FormControl(this.xs),
      s: new FormControl(this.s),
      m: new FormControl(this.m),
      l: new FormControl(this.l),
      xl: new FormControl(this.xl),
      xxl: new FormControl(this.xxl),

      xsQty: new FormControl(this.data["sizeArrayQty"][0]["qty"]),
      sQty: new FormControl(this.data["sizeArrayQty"][1]["qty"]),
      mQty: new FormControl(this.data["sizeArrayQty"][2]["qty"]),
      lQty: new FormControl(this.data["sizeArrayQty"][3]["qty"]),
      xlQty: new FormControl(this.data["sizeArrayQty"][4]["qty"]),
      xxlQty: new FormControl(this.data["sizeArrayQty"][5]["qty"]),
    });
  }

  radioGroupValue = "all";
  subRadioGroupValue = "alljewelry";

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }

  update() {
    // console.log(this.form.value);
    this.form.value["picUrl"] = this.pictureUrlArray;
    this.form.value["_id"] = this.data["_id"];
    this.form.value["__v"] = this.data["__v"];
    var temp = ([] = []);
    var sizeArrayQty;
    if (this.form.value["productType"] == "fashion") {
      if (this.xs) temp.push("xs");
      if (this.s) temp.push("s");
      if (this.m) temp.push("m");
      if (this.l) temp.push("l");
      if (this.xl) temp.push("xl");
      if (this.xxl) temp.push("xxl");
      sizeArrayQty = [
        { size: "xs", qty: this.form.value["xsQty"] },
        { size: "s", qty: this.form.value["sQty"] },
        { size: "m", qty: this.form.value["mQty"] },
        { size: "l", qty: this.form.value["lQty"] },
        { size: "xl", qty: this.form.value["xlQty"] },
        { size: "xxl", qty: this.form.value["xxlQty"] },
      ];
    }
    this.form.value["sizeArrayQty"] = sizeArrayQty;
    this.form.value["sizeArray"] = temp;

    if (this.form.value["showcase"] == "inDemand") {
      this.form.value["inDemand"] = true;
      this.form.value["newArrival"] = false;
    } else if (this.form.value["showcase"] == "newArrival") {
      this.form.value["newArrival"] = true;
      this.form.value["inDemand"] = false;
    } else {
      this.form.value["newArrival"] = false;
      this.form.value["inDemand"] = false;
    }

    this.productService
      .update(this.form.value)
      .toPromise()
      .then((data) => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.ref.close();
      });
  }

  get name() {
    return this.form.get("name");
  }
  get description() {
    return this.form.get("description");
  }
  get productPrice() {
    return this.form.get("productPrice");
  }
  get productCostPrice() {
    return this.form.get("productCostPrice");
  }
  get saleOfferPrice() {
    return this.form.get("saleOfferPrice");
  }
  get productType() {
    return this.form.get("productType");
  }
  get showcase() {
    return this.form.get("showcase");
  }
  get category() {
    return this.form.get("category");
  }
  get subProductType() {
    return this.form.get("subProductType");
  }
  // get colors() {
  //   return this.form.get("colors");
  // }
  get grmas() {
    return this.form.get("grmas");
  }
  get itemCollection() {
    return this.form.get("itemCollection");
  }
  get itemBrand() {
    return this.form.get("itemBrand");
  }
  get qty() {
    return this.form.get("qty");
  }
  get size() {
    return this.form.get("size");
  }
  get itemWeight() {
    return this.form.get("itemWeight");
  }
  get metal() {
    return this.form.get("metal");
  }
  get gemstone() {
    return this.form.get("gemstone");
  }
  get birthstone() {
    return this.form.get("birthstone");
  }
  get gemstoneWeight() {
    return this.form.get("gemstoneWeight");
  }
  get gemstoneCut() {
    return this.form.get("gemstoneCut");
  }
  get rodiumPlated() {
    return this.form.get("rodiumPlated");
  }
  get measurement() {
    return this.form.get("measurement");
  }
  get necklaceLength() {
    return this.form.get("necklaceLength");
  }
  get species() {
    return this.form.get("species");
  }
  get variety() {
    return this.form.get("variety");
  }
  get weight() {
    return this.form.get("weight");
  }
  get cut() {
    return this.form.get("cut");
  }
  get treatments() {
    return this.form.get("treatments");
  }
}

import { Component, OnInit, OnDestroy } from "@angular/core";
import { ProductService } from "../product.service";
import * as io from "socket.io-client";
import { SubSink } from "subsink";
import { NbDialogService } from "@nebular/theme";
import { UpdateProductComponent } from "./update-product/update-product.component";

@Component({
  selector: "ngx-view-product",
  templateUrl: "./view-product.component.html",
  styleUrls: ["./view-product.component.scss"],
})
export class ViewProductComponent implements OnInit, OnDestroy {
  private subs = new SubSink();

  settings = {
    actions: {
      add: false,
    },

    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },

    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      // },
      name: {
        title: "Product Name",
        type: "string",
      },
      description: {
        title: "Description",
        type: "string",
      },
      productPrice: {
        title: "Product Price",
        type: "string",
      },
      productCostPrice: {
        title: "Cost Price",
        type: "string",
      },
      saleOfferPrice: {
        title: "Offer Price",
        type: "string",
      },
      qty: {
        title: "QTY",
        type: "string",
      },
      picUrl: {
        title: "Picture",
        type: "html",
        valuePrepareFunction: (picUrl: string) => {
          return `<img width="50px" src="${picUrl[0]}" />`;
        },
      },
    },
  };

  source = ([] = []);
  socket;

  constructor(
    private productService: ProductService,
    private dialogService: NbDialogService
  ) {
    this.socket = io("https://ceylonstreet.herokuapp.com/");
    this.getAllProducts();
  }

  ngOnInit(): void {
    this.getAllProducts();

    this.socket.on("delete-product", () => {
      this.getAllProducts();
    });
    this.socket.on("edit-product", () => {
      this.getAllProducts();
    });
  }

  getAllProducts() {
    this.subs.sink = this.productService.getAllProducts().subscribe((data) => {
      this.source = [];
      data["data"].forEach((element) => {
        this.source.push(element);
      });
    });
  }

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      // event.confirm.resolve();
      // console.log(event.data.email);

      this.productService
        .delete(event.data._id)
        .toPromise()
        .then((data) => {
          // tslint:disable-next-line: no-console
          console.log(data);
        });
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    // console.log(event.data);
    if (window.confirm("Are you sure you want to edit with advance options?")) {
      // event.newData["name"] += " + added in code";
      // console.log(event.data);
      // console.log(event.newData);

      this.dialogService.open(UpdateProductComponent, {
        context: {
          data: event.data,
        },
      });
    } else {
      this.productService
        .update(event.newData)
        .toPromise()
        .then((data) => {
          // tslint:disable-next-line: no-console
          console.log(data);

          event.confirm.resolve();
        });
      // event.confirm.reject();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}

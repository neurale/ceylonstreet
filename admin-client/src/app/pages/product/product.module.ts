import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  FormsModule as ngFormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { ProductComponent } from './product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ViewProductComponent } from './view-product/view-product.component';
import { ProductRoutingModule } from './product-routing.module';
import {
  NbInputModule,
  NbCardModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCheckboxModule,
  NbRadioModule,
  NbDatepickerModule,
  NbSelectModule,
  NbIconModule,
  NbTreeGridModule,
} from '@nebular/theme';
import { FormsRoutingModule } from '../forms/forms-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DropzoneDirective } from './add-product/dropzone.directive';
import { UploadTaskComponent } from './add-product/upload-task/upload-task.component';
import { UpdateProductComponent } from './view-product/update-product/update-product.component';

@NgModule({
  declarations: [
    ProductComponent,
    AddProductComponent,
    ViewProductComponent,
    DropzoneDirective,
    UploadTaskComponent,
    UpdateProductComponent,
    // FormsComponent,
    // FormInputsComponent,
    // FormLayoutsComponent,
    // ButtonsComponent,
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    ReactiveFormsModule,
  ],
  providers: [],
})
export class ProductModule {}

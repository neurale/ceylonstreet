import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: "root",
})
export class ProductService {
  constructor(private http: HttpClient) {}

  baseUri = "https://ceylonstreet.herokuapp.com/api/products";
  // baseUri = "http://localhost:3000/api/products";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  create(product) {
    // console.log(product);
    return this.http.post(`${this.baseUri}/add`, product);
  }

  getAllProducts() {
    return this.http.get(`${this.baseUri}/product`);
  }

  update(product) {
    // tslint:disable-next-line: no-console
    console.log(product);

    return this.http.post(`${this.baseUri}/product`, product);
  }

  delete(id) {
    return this.http.delete(`${this.baseUri}/product/${id}`);
  }
}

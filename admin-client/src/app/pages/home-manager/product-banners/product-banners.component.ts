import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HomeManagerService } from "../home-manager.service";
import { ProductService } from "../../product/product.service";
import { CollectionsService } from "../../collections/collections.service";

@Component({
  selector: "ngx-product-banners",
  templateUrl: "./product-banners.component.html",
  styleUrls: ["./product-banners.component.scss"],
})
export class ProductBannersComponent implements OnInit {
  metaData;
  products;
  collections;
  constructor(
    private homeManagerService: HomeManagerService,
    private productService: ProductService,
    private collectionService: CollectionsService
  ) {
    this.productService.getAllProducts().subscribe((products) => {
      // console.log(products);
      this.products = products["data"];
    });
    this.collectionService.getAllcollections().subscribe((collections) => {
      this.collections = collections["data"];
      // console.log(collections['data'])
    });
    this.getMetaData();
  }
  htmlContent: String;

  radioGroupValue = "collection";

  ngOnInit(): void {}

  form = new FormGroup({
    bannerType: new FormControl("", Validators.required),
    line1: new FormControl("", Validators.required),
    line2: new FormControl("", Validators.required),
    buttonText: new FormControl("", Validators.required),
    product: new FormControl(""),
    collection: new FormControl(""),
  });

  get bannerType() {
    return this.form.get("bannerType");
  }
  get product() {
    return this.form.get("product");
  }
  get collection() {
    return this.form.get("collection");
  }
  get line1() {
    return this.form.get("line1");
  }
  get line2() {
    return this.form.get("line2");
  }
  get buttonText() {
    return this.form.get("buttonText");
  }

  // metaData=[]=[];
  getMetaData() {
    this.homeManagerService.getAll().subscribe((data) => {
      // console.log(data['data'])
      this.metaData = data["data"];
    });
  }

  create() {
    // this.metaData[0][this.form.value["bannerType"]] = this.pictureUrlArray[0];
    // console.log(this.metaData[0]);
    this.form.value["navigationType"] = this.radioGroupValue;
    this.form.value["picUrl"] = this.pictureUrlArray[0];
    this.metaData[0][this.form.value["bannerType"]] = this.form.value;
    console.log(this.metaData);
    // this.blogService
    //   .create(this.form.value)
    //   .toPromise()
    //   .then((data) => {
    //     console.log(data);
    //     this.pictureUrlArray = [];
    //     this.files = [];
    //     this.form.reset();
    //   });

    this.homeManagerService
      .update(this.metaData[0])
      .toPromise()
      .then((data) => {
        // console.log(data);
        this.pictureUrlArray = [];
        this.files = [];
        this.form.reset();
      });
  }
  onChange(event) {
    this.htmlContent = event;
  }

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  pictureUrlArray = ([] = []);

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }
}

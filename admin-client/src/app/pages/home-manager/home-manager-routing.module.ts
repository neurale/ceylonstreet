import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeManagerComponent } from './home-manager.component';
import { HomeBannersComponent } from './home-banners/home-banners.component';
import { ProductBannersComponent } from './product-banners/product-banners.component';


const routes: Routes = [
  {
    path: '',
    component: HomeManagerComponent,
    children: [
      {
        path: 'home-banners',
        component: HomeBannersComponent,
      },
      {
        path: 'product-banners',
        component: ProductBannersComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeManagerRoutingModule {}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-home-manager',
  template: ` <router-outlet></router-outlet> `,
})
export class HomeManagerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

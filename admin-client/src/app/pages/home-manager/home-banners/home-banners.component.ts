import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HomeManagerService } from "../home-manager.service";
import { ProductService } from "../../product/product.service";
import { CollectionsService } from "../../collections/collections.service";

@Component({
  selector: "ngx-home-banners",
  templateUrl: "./home-banners.component.html",
  styleUrls: ["./home-banners.component.scss"],
})
export class HomeBannersComponent implements OnInit {
  products;
  collections;

  constructor(
    private homeManagerService: HomeManagerService,
    private productService: ProductService,
    private collectionService: CollectionsService
  ) {
    this.productService.getAllProducts().subscribe((products) => {
      // console.log(products);
      this.products = products["data"];
    });
    this.collectionService.getAllcollections().subscribe((collections) => {
      this.collections = collections["data"];
      // console.log(collections['data'])
    });
    this.getMetaData();
  }
  htmlContent: String;

  ngOnInit(): void {}

  form = new FormGroup({
    bannerType: new FormControl("", Validators.required),
    line1: new FormControl(""),
    line2: new FormControl(""),
    line3: new FormControl(""),
    line4: new FormControl(""),
    youtubeLink: new FormControl(
      ""
      // Validators.pattern(
      //   "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?"
      // )
    ),
  });

  get bannerType() {
    return this.form.get("bannerType");
  }
  get line1() {
    return this.form.get("line1");
  }
  get line2() {
    return this.form.get("line2");
  }
  get line3() {
    return this.form.get("line2");
  }
  get line4() {
    return this.form.get("line2");
  }
  get youtubeLink() {
    return this.form.get("youtubeLink");
  }

  metaData;
  getMetaData() {
    this.homeManagerService.getAll().subscribe((data) => {
      // console.log(data['data'])
      this.metaData = data["data"];
    });
  }

  create() {
    let temp;

    if (this.form.value["bannerType"] == "videoBanner") {
      temp = {
        line1: this.form.value["line1"],
        line2: this.form.value["line2"],
        line3: this.form.value["line3"],
        line4: this.form.value["line4"],
        youtubeLink: this.form.value["youtubeLink"],
        picUrl: this.pictureUrlArray[0],
      };
    } else {
      temp = {
        line1: this.form.value["line1"],
        line2: this.form.value["line2"],
        line3: this.form.value["line3"],
        line4: this.form.value["line4"],
        picUrl: this.pictureUrlArray[0],
      };
    }

    // this.form.value["picUrl"] = this.pictureUrlArray[0];
    this.metaData[0][this.form.value["bannerType"]] = temp;
    // console.log(this.metaData);
    // this.blogService
    //   .create(this.form.value)
    //   .toPromise()
    //   .then((data) => {
    //     console.log(data);
    //     this.pictureUrlArray = [];
    //     this.files = [];
    //     this.form.reset();
    //   });

    this.homeManagerService
      .update(this.metaData[0])
      .toPromise()
      .then((data) => {
        // console.log(data);
        this.pictureUrlArray = [];
        this.files = [];
        this.form.reset();
      });
  }
  onChange(event) {
    this.htmlContent = event;
  }

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  pictureUrlArray = ([] = []);

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }
}

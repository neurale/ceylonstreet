import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadBannerPicturesComponent } from './upload-banner-pictures.component';

describe('UploadBannerPicturesComponent', () => {
  let component: UploadBannerPicturesComponent;
  let fixture: ComponentFixture<UploadBannerPicturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadBannerPicturesComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadBannerPicturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbTreeGridModule,
  NbAlertModule,
  NbTabsetModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule } from '@angular/forms';
// import { AgmCoreModule } from "@agm/core";
import { HomeManagerRoutingModule } from './home-manager-routing.module';
import { HomeManagerComponent } from './home-manager.component';
import { HomeBannersComponent } from './home-banners/home-banners.component';
import { ProductBannersComponent } from './product-banners/product-banners.component';
import { DropzoneDirective } from './dropzone.directive';
import { UploadBannerPicturesComponent } from './upload-banner-pictures/upload-banner-pictures.component';



@NgModule({
  imports: [
    HomeManagerRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbAlertModule,
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbTreeGridModule,
    NbTabsetModule,

    // AgmCoreModule,
  ],
  declarations: [
    HomeManagerComponent,
    HomeBannersComponent,
    ProductBannersComponent,
    DropzoneDirective,
    UploadBannerPicturesComponent,
  ],
  providers: [],
})
export class HomeManagerModule {}

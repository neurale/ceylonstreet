import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HomeManagerService {
  constructor(private http: HttpClient) {}
  baseUri = "https://ceylonstreet.herokuapp.com/api/homeMeta";
  // baseUri = "http://localhost:3000/api/homeMeta";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  update(metaData) {
    // console.log(article);
    return this.http.post(`${this.baseUri}/homeMetaSchema`, metaData);
  }

  getAll() {
    return this.http.get(`${this.baseUri}/homeMetaSchema`);
  }
}

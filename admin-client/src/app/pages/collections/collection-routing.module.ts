import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollectionsComponent } from './collections.component';
import { AddCollectionsComponent } from './add-collections/add-collections.component';
import { ViewCollectionsComponent } from './view-collections/view-collections.component';



const routes: Routes = [
  {
    path: '',
    component: CollectionsComponent,
    children: [
      {
        path: 'add-collections',
        component: AddCollectionsComponent,
      },
      {
        path: 'view-collections',
        component: ViewCollectionsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollectionRoutingModule {}

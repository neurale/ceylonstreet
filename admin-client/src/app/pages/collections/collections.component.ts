import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-collections',
  template: ` <router-outlet></router-outlet> `,
})
export class CollectionsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { CollectionsService } from "../collections.service";
import { BrandsService } from "../brands.service";

@Component({
  selector: "ngx-add-collections",
  templateUrl: "./add-collections.component.html",
  styleUrls: ["./add-collections.component.scss"],
})
export class AddCollectionsComponent implements OnInit {
  ngOnInit() {}
  pictureUrlArray = ([] = []);

  constructor(
    private collectionService: CollectionsService,
    private brandService: BrandsService
  ) {
    this.brandService.getAllBrands().subscribe((brands) => {
      this.brands = brands["data"];
    });
  }

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },

    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: "Brands",
        type: "string",
      },
    },
  };

  brands;

  form = new FormGroup({
    name: new FormControl("", Validators.required),
    // description: new FormControl("", Validators.required),
  });

  create() {
    this.form.value["picUrl"] = this.pictureUrlArray;
    // console.log(this.form.value);
    this.collectionService
      .create(this.form.value)
      .toPromise()
      .then((data) => {
        console.log(data);
        this.form.reset();
        this.files = [];
        this.pictureUrlArray = [];
      });
  }

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }

  get name() {
    return this.form.get("name");
  }
  // get description() {
  //   return this.form.get("description");
  // }

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      this.brandService
        .deleteBrand(event.data["_id"])
        .toPromise()
        .then((data) => {
          console.log(data);
          event.confirm.resolve();
        });
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    if (window.confirm("Are you sure you want to add this?")) {
      let brand = { name: event.newData["name"] };
      this.brandService
        .create(brand)
        .toPromise()
        .then((data) => {
          console.log(data);
          event.confirm.resolve();
        });
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event) {
    if (window.confirm("Are you sure you want to edit this?")) {
      this.brandService
        .editBrand(event.newData)
        .toPromise()
        .then((data) => {
          console.log(data);
          event.confirm.resolve();
        });
    } else {
      event.confirm.reject();
    }
  }
}

import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class CollectionsService {
  constructor(private http: HttpClient) {}

  baseUri = "https://ceylonstreet.herokuapp.com/api/collections";
  // baseUri = "http://localhost:3000/api/collections";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  create(collection) {
    // console.log(collection);
    return this.http.post(`${this.baseUri}/add`, collection);
  }

  getAllcollections() {
    return this.http.get(`${this.baseUri}/collection`);
  }

  update(collection) {
    console.log(collection);

    return this.http.post(`${this.baseUri}/collection`, collection);
  }

  delete(id) {
    return this.http.delete(`${this.baseUri}/delete/${id}`);
  }
}

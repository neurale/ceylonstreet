import { Component, OnInit } from '@angular/core';
import { CollectionsService } from '../collections.service';
import { NbDialogService } from '@nebular/theme';
import * as io from 'socket.io-client';
import { SubSink } from 'subsink';
import { UpdateCollectionComponent } from './update-collection/update-collection.component';

@Component({
  selector: 'ngx-view-collections',
  templateUrl: './view-collections.component.html',
  styleUrls: ['./view-collections.component.scss'],
})
export class ViewCollectionsComponent implements OnInit {
  private subs = new SubSink();

  settings = {
    actions: {
      add: false,
    },

    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },

    columns: {
      name: {
        title: 'Collection Name',
        type: 'string',
      },
      // description: {
      //   title: 'Description',
      //   type: 'string',
      // },
    },
  };

  source = ([] = []);
  socket;

  constructor(
    private collectionService: CollectionsService,
    private dialogService: NbDialogService,
  ) {
    this.socket = io('https://ceylonstreet.herokuapp.com/');
    this.getAllCollections();
  }

  ngOnInit(): void {
    this.getAllCollections();

    this.socket.on("delete-collection", () => {
      this.getAllCollections();
    });
    this.socket.on("edit-collection", () => {
      this.getAllCollections();
    });
  }

  getAllCollections() {
    this.subs.sink = this.collectionService
      .getAllcollections()
      .subscribe((data) => {
        this.source = [];
        data['data'].forEach((element) => {
          this.source.push(element);
        });
      });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      // event.confirm.resolve();
      // console.log(event.data.email);
      this.collectionService
        .delete(event.data._id)
        .toPromise()
        .then((data) => {
          // tslint:disable-next-line: no-console
          console.log(data);
        });
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    // console.log(event.data);
    if (window.confirm('Are you sure you want to edit with advance options?')) {
      // event.newData["name"] += " + added in code";
      // console.log(event.data);
      // console.log(event.newData);
      this.dialogService.open(UpdateCollectionComponent, {
        context: {
          data: event.data,
        },
      });
    } else {
      this.collectionService
        .update(event.newData)
        .toPromise()
        .then((data) => {
          // tslint:disable-next-line: no-console
          console.log(data);
          event.confirm.resolve();
        });
      // event.confirm.reject();
    }
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}

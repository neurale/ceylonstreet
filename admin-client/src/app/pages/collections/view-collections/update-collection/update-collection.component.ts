import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CollectionsService } from '../../collections.service';

@Component({
  selector: 'ngx-update-collection',
  templateUrl: './update-collection.component.html',
  styleUrls: ['./update-collection.component.scss'],
})
export class UpdateCollectionComponent implements OnInit {
  @Input() data;
  pictureUrlArray = ([] = []);

  constructor(
    protected ref: NbDialogRef<UpdateCollectionComponent>, private collectionService: CollectionsService,
  ) {}

  dismiss() {
    this.ref.close();
  }

  form;
  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(this.data['name'], Validators.required),
      // description: new FormControl(
      //   this.data['description'],
      //   Validators.required,
      // ),
    });
  }

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }

  update() {
    // console.log(this.form.value);
    this.form.value['picUrl'] = this.pictureUrlArray;
    this.form.value['_id'] = this.data['_id'];
    this.form.value['__v'] = this.data['__v'];

    this.collectionService
      .update(this.form.value)
      .toPromise()
      .then((data) => {
        console.log(data);
        this.ref.close();
      });
  }

  get name() {
    return this.form.get('name');
  }
  // get description() {
  //   return this.form.get('description');
  // }
}

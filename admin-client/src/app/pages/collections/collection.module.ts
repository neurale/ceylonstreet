import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  FormsModule as ngFormsModule,
  ReactiveFormsModule,
} from '@angular/forms';

import {
  NbInputModule,
  NbCardModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCheckboxModule,
  NbRadioModule,
  NbDatepickerModule,
  NbSelectModule,
  NbIconModule,
  NbTreeGridModule,
} from '@nebular/theme';
import { FormsRoutingModule } from '../forms/forms-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
// import { DropzoneDirective } from "./add-product/dropzone.directive";
// import { UploadTaskComponent } from "./add-product/upload-task/upload-task.component";
// import { UpdateProductComponent } from "./view-product/update-product/update-product.component";
import { CollectionRoutingModule } from './collection-routing.module';
import { CollectionsComponent } from './collections.component';
import { AddCollectionsComponent } from './add-collections/add-collections.component';
import { ViewCollectionsComponent } from './view-collections/view-collections.component';
import { UploadTaskComponent } from './add-collections/upload-task/upload-task.component';
import { DropzoneDirective } from './dropzone.directive';
import { UpdateCollectionComponent } from './view-collections/update-collection/update-collection.component';



@NgModule({
  declarations: [
    CollectionsComponent,
    AddCollectionsComponent,
    ViewCollectionsComponent,
    UploadTaskComponent,
    DropzoneDirective,
    UpdateCollectionComponent,
    // UploadTaskComponent,
    // UpdateProductComponent,
    // FormsComponent,
    // FormInputsComponent,
    // FormLayoutsComponent,
    // ButtonsComponent,
  ],
  imports: [
    CommonModule,
    CollectionRoutingModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    ReactiveFormsModule,
  ],
  providers: [],
})
export class CollectionModule {}

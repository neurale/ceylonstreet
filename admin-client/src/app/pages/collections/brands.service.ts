import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class BrandsService {
  constructor(private http: HttpClient) {}

  baseUri = "https://ceylonstreet.herokuapp.com/api/brands";
  // baseUri = "http://localhost:3000/api/brands";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  create(brand) {
    return this.http.post(`${this.baseUri}/add`, brand);
  }
  getAllBrands() {
    return this.http.get(`${this.baseUri}/brand`);
  }

  editBrand(brand) {
    return this.http.post(`${this.baseUri}/brand`, brand);
  }

  deleteBrand(id) {
    return this.http.delete(`${this.baseUri}/brand/${id}`);
  }
}

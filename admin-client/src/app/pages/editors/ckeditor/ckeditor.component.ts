import { Component } from '@angular/core';

import './ckeditor.loader';
import 'ckeditor';

@Component({
  selector: 'ngx-ckeditor',
  template: `
    <nb-card>
      <nb-card-header>
        New Article
      </nb-card-header>
      <nb-card-body>
        <ckeditor
          (change)="onChange($event)"
          [config]="{ extraPlugins: 'divarea', height: '320' }"
        ></ckeditor>
      </nb-card-body>
    </nb-card>
  `,
})
export class CKEditorComponent {
  constructor() {}
  onChange(event) {
    // tslint:disable-next-line: no-console
    console.log(event);
  }
}

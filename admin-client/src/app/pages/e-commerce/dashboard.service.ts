import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  constructor(private http: HttpClient) {}
  // orderUri = "http://localhost:3000/api/orders";
  // productUri = "http://localhost:3000/api/products";
  orderUri = "https://ceylonstreet.herokuapp.com/api/orders";
  productUri = "https://ceylonstreet.herokuapp.com/api/products";

  headers = new HttpHeaders().set("Content-Type", "application/json");

  getAllOrders() {
    return this.http.get(`${this.orderUri}/order`);
  }

  getAllProducts() {
    return this.http.get(`${this.productUri}/product`);
  }
}

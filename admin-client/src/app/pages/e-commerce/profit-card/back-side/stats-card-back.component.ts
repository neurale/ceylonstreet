import { Component, OnDestroy, AfterViewInit } from "@angular/core";
import { StatsBarData } from "../../../../@core/data/stats-bar";
import { takeWhile } from "rxjs/operators";
import { DashboardService } from "../../dashboard.service";

@Component({
  selector: "ngx-stats-card-back",
  styleUrls: ["./stats-card-back.component.scss"],
  templateUrl: "./stats-card-back.component.html",
})
export class StatsCardBackComponent implements OnDestroy {
  private alive = true;

  chartData: Number[] = [];
  profitArray = ([] = []);

  constructor(
    private statsBarData: StatsBarData,
    private dashboardService: DashboardService
  ) {
    this.dashboardService.getAllOrders().subscribe((data) => {
      let i = 0;
      data["data"].forEach((element) => {
        i++;

        if (i >= data["data"].length - 7)
          this.profitArray.push(Number(element.profit));
      });
      console.log(this.profitArray);
    });
    // this.statsBarData
    //   .getStatsBarData()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe((data) => {
    //     this.chartData = data;
    //   });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}

import { Component } from "@angular/core";
import { DashboardService } from "./dashboard.service";
import { DatePipe } from "@angular/common";

@Component({
  selector: "ngx-ecommerce",
  templateUrl: "./e-commerce.component.html",
})
export class ECommerceComponent {
  profit;
  salesValue;
  totalItems;
  totalStock;
  costValue;

  constructor(
    private dashboardService: DashboardService,
    public datepipe: DatePipe
  ) {
    this.dashboardService.getAllOrders().subscribe((data) => {
      this.profit = 0;
      this.salesValue = 0;
      this.totalItems = 0;
      let i = 0;
      data["data"].forEach((element) => {
        i++;
        if (
          datepipe.transform(element.date, "yyyy-MM-dd") ==
          datepipe.transform(Date.now(), "yyyy-MM-dd")
        ) {
          this.profit += Number(element.profit);

          this.salesValue += Number(element.fixtotal);
          this.totalItems += element.items.length;
        }
      });
      // console.log(this.profitArray);
    });
    this.dashboardService.getAllProducts().subscribe((data) => {
      this.totalStock = 0;
      this.costValue = 0;
      data["data"].forEach((element) => {
        console.log(element);
        this.totalStock += Number(element.productPrice) * Number(element.qty);
        this.costValue +=
          Number(element.productCostPrice) * Number(element.qty);
      });
    });
  }
}

import { Component, OnDestroy } from "@angular/core";
import { TrafficList, TrafficListData } from "../../../@core/data/traffic-list";
import { TrafficBarData, TrafficBar } from "../../../@core/data/traffic-bar";
import { takeWhile } from "rxjs/operators";
import { DashboardService } from "../dashboard.service";

@Component({
  selector: "ngx-traffic-reveal-card",
  styleUrls: ["./traffic-reveal-card.component.scss"],
  templateUrl: "./traffic-reveal-card.component.html",
})
export class TrafficRevealCardComponent implements OnDestroy {
  private alive = true;

  trafficBarData: TrafficBar;
  trafficListData: TrafficList;
  revealed = false;
  period: string = "week";

  constructor(
    private trafficListService: TrafficListData,
    private dashboardService: DashboardService,
    private trafficBarService: TrafficBarData
  ) {
    this.getTrafficFrontCardData(this.period);
    this.getTrafficBackCardData(this.period);
  }

  toggleView() {
    this.revealed = !this.revealed;
  }

  setPeriodAngGetData(value: string): void {
    this.period = value;

    this.getTrafficFrontCardData(value);
    // this.getTrafficBackCardData(value);
  }

  getTrafficBackCardData(period: string) {
    this.trafficBarService
      .getTrafficBarData(period)
      .pipe(takeWhile(() => this.alive))
      .subscribe((trafficBarData) => {
        this.trafficBarData = trafficBarData;
      });
  }

  getTrafficFrontCardData(period: string) {
    // this.trafficListService.getTrafficListData(period)
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe(trafficListData => {
    //     this.trafficListData = trafficListData;
    //   });
    if (period == "week") {
      this.dashboardService.getAllOrders().subscribe((data) => {
        this.trafficListData = data["data"].reverse();
        console.log(this.trafficListData);
      });
    } else if (period == "month") {
      this.dashboardService.getAllOrders().subscribe((data) => {
        this.trafficListData = data["data"];
      });
    } else if (period == "year") {
      this.dashboardService.getAllOrders().subscribe((data) => {
        this.trafficListData = data["data"];
      });
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }
}

import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class CouponsService {
  constructor(private http: HttpClient) {}

  baseUri = "https://ceylonstreet.herokuapp.com/api/promoCode";
  // baseUri = "http://localhost:3000/api/promoCode";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  create(coupon) {
    // console.log(coupon);
    return this.http.post(`${this.baseUri}/add`, coupon);
  }

  getAllCoupons() {
    return this.http.get(`${this.baseUri}/promoCode`);
  }

  update(coupon) {
    // tslint:disable-next-line: no-console
    console.log(coupon);

    return this.http.post(`${this.baseUri}/promoCode`, coupon);
  }

  delete(id) {
    return this.http.delete(`${this.baseUri}/promoCode/${id}`);
  }
}

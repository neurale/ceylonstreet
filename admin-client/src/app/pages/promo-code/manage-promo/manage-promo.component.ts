import { Component, OnInit } from "@angular/core";
import { CouponsService } from '../coupons.service';

@Component({
  selector: "ngx-manage-promo",
  templateUrl: "./manage-promo.component.html",
  styleUrls: ["./manage-promo.component.scss"],
})
export class ManagePromoComponent implements OnInit {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },

    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      // },
      couponName: {
        title: "Coupon Name",
        type: "string",
      },
      discount: {
        title: "Discount (%)",
        type: "string",
      },
      minLimit: {
        title: "Min Limit",
        type: "string",
      },
      maxLimit: {
        title: "Max Limit",
        type: "string",
      },
    },
  };

  source;

  constructor(private couponService: CouponsService) {
    this.couponService.getAllCoupons().subscribe((data) => {
      this.source = data["data"];
      console.log(data);
    });
  }

  ngOnInit(): void {}

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      this.couponService
        .delete(event.data["_id"])
        .toPromise()
        .then((data) => {
          console.log(data);
          event.confirm.resolve();
        });
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    if (window.confirm("Are you sure you want to add this?")) {
      // let brand = { name: event.newData["couponName"] };
      this.couponService
        .create(event.newData)
        .toPromise()
        .then((data) => {
          console.log(data);
          event.confirm.resolve();
        });
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event) {
    if (window.confirm("Are you sure you want to edit this?")) {
      this.couponService
        .update(event.newData)
        .toPromise()
        .then((data) => {
          console.log(data);
          event.confirm.resolve();
        });
    } else {
      event.confirm.reject();
    }
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: "ngx-promo-code",
  template: ` <router-outlet></router-outlet> `,
})
export class PromoCodeComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ManagePromoComponent } from './manage-promo/manage-promo.component';
import { PromoCodeComponent } from './promo-code.component';

const routes: Routes = [
  {
    path: "",
    component: PromoCodeComponent,
    children: [
      {
        path: "manage-promo",
        component: ManagePromoComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromoCodeRoutingModule {}

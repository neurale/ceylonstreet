import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbTreeGridModule,
  NbAlertModule,
  NbTabsetModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule } from '@angular/forms';
// import { AgmCoreModule } from "@agm/core";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PromoCodeRoutingModule } from './promo-code-routing.module';
import { PromoCodeComponent } from './promo-code.component';
import { ManagePromoComponent } from './manage-promo/manage-promo.component';



@NgModule({
  imports: [
    PromoCodeRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbAlertModule,
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbTreeGridModule,
    NbTabsetModule,

    // AgmCoreModule,
    Ng2SmartTableModule,
  ],
  declarations: [PromoCodeComponent, ManagePromoComponent],
  providers: [],
})
export class PromoCodeModule {}

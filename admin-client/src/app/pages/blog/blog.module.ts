import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  FormsModule as ngFormsModule,
  ReactiveFormsModule,
} from '@angular/forms';


import {
  NbInputModule,
  NbCardModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCheckboxModule,
  NbRadioModule,
  NbDatepickerModule,
  NbSelectModule,
  NbIconModule,
  NbTreeGridModule,
} from '@nebular/theme';
import { FormsRoutingModule } from '../forms/forms-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BlogComponent } from './blog.component';
import { AddBlogArticleComponent } from './add-blog-article/add-blog-article.component';
import { BlogRoutingModule } from './blog-routing.module';
import { CKEditorModule } from 'ng2-ckeditor';
import { UploadBlogPictureComponent } from './add-blog-article/upload-blog-picture/upload-blog-picture.component';
import { DropzoneDirective } from './dropzone.directive';

@NgModule({
  declarations: [
    BlogComponent,
    AddBlogArticleComponent,
    UploadBlogPictureComponent,
    DropzoneDirective,

    // FormsComponent,
    // FormInputsComponent,
    // FormLayoutsComponent,
    // ButtonsComponent,
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    ReactiveFormsModule,
    CKEditorModule,
    // ThemeModule,
  ],
  providers: [],
})
export class BlogModule {}

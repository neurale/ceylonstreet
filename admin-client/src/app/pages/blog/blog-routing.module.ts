import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogComponent } from './blog.component';
import { AddBlogArticleComponent } from './add-blog-article/add-blog-article.component';

const routes: Routes = [
  {
    path: '',
    component: BlogComponent,
    children: [
      {
        path: 'add-blog-article',
        component: AddBlogArticleComponent,
      },
      // {
      //   path: "view-product",
      //   component: ViewProductComponent,
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogRoutingModule {}

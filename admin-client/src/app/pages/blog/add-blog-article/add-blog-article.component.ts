import { Component, OnInit } from '@angular/core';

import './ckeditor.loader';
import 'ckeditor';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BlogService } from '../blog.service';

@Component({
  selector: 'ngx-add-blog-article',
  templateUrl: './add-blog-article.component.html',
  styleUrls: ['./add-blog-article.component.scss'],
})
export class AddBlogArticleComponent implements OnInit {
  constructor(private blogService: BlogService) {}
  htmlContent: String;

  ngOnInit(): void {}

  form = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  get title() {
    return this.form.get('title');
  }
  get description() {
    return this.form.get('description');
  }

  create() {
    this.form.value['content'] = this.htmlContent;
    this.form.value['picUrl'] = this.pictureUrlArray;
    this.blogService
      .create(this.form.value)
      .toPromise()
      .then((data) => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.pictureUrlArray = [];
        this.files = [];
        this.form.reset();
      });
  }
  onChange(event) {
    this.htmlContent = event;
  }

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  pictureUrlArray = ([] = []);

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadBlogPictureComponent } from './upload-blog-picture.component';

describe('UploadBlogPictureComponent', () => {
  let component: UploadBlogPictureComponent;
  let fixture: ComponentFixture<UploadBlogPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadBlogPictureComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadBlogPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

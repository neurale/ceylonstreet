import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  constructor(private http: HttpClient) {}

  baseUri = 'https://ceylonstreet.herokuapp.com/api/blogs';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  create(article) {
    // console.log(article);
    return this.http.post(`${this.baseUri}/add`, article);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderMangePopupComponent } from './order-mange-popup.component';

describe('OrderMangePopupComponent', () => {
  let component: OrderMangePopupComponent;
  let fixture: ComponentFixture<OrderMangePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderMangePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderMangePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

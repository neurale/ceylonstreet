import { Component, OnInit, Input } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { OrderService } from "../../order.service";

@Component({
  selector: "ngx-order-mange-popup",
  templateUrl: "./order-mange-popup.component.html",
  styleUrls: ["./order-mange-popup.component.scss"],
})
export class OrderMangePopupComponent implements OnInit {
  @Input() data;
  source;

  settings = {
    actions: {
      columnTitle: "View",

      add: false,
      edit: false,
      delete: false,
    },

    columns: {
      item_id: {
        title: "ID",
        type: "string",
      },
      item_name: {
        title: "Item Name",
        type: "string",
      },
      price: {
        title: "Price",
        type: "string",
      },
      qty: {
        title: "QTY",
        type: "string",
      },
      picUrl: {
        title: "Picture",
        type: "html",
        valuePrepareFunction: (picture: string) => {
          return `<img width="50px" src="${picture}" />`;
        },
      },
    },
  };

  constructor(
    protected ref: NbDialogRef<OrderMangePopupComponent>,
    private orderService: OrderService
  ) {}

  ngOnInit(): void {
    this.data["items"].forEach((element) => {
      if (element.size.length > 0) {
        var temp;
        element.size.forEach((s) => {
          temp = s.size + " x " + s.qty + "\n";
        });

        element["item_name"] =
          element["item_name"] + " " +temp;
      }
    });

    this.source = this.data["items"];
    console.log(this.data);
  }

  dismiss() {
    this.ref.close();
  }

  setShipped(_id, status, phone) {
    if (window.confirm("Are you sure you want to proceed?")) {
      this.orderService.setState(_id, status, phone).subscribe((data) => {
        console.log(data);
      });
    }
  }
}

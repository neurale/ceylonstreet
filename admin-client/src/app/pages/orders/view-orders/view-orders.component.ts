import { Component, OnInit } from "@angular/core";
import { OrderService } from "../order.service";
import { NbDialogService } from "@nebular/theme";
import { OrderMangePopupComponent } from "./order-mange-popup/order-mange-popup.component";
import * as io from "socket.io-client";
import { SubSink } from "subsink";

@Component({
  selector: "ngx-view-orders",
  templateUrl: "./view-orders.component.html",
  styleUrls: ["./view-orders.component.scss"],
})
export class ViewOrdersComponent implements OnInit {
  private subs = new SubSink();

  settings = {
    actions: {
      columnTitle: "View",

      add: false,
      edit: false,
      delete: false,
      custom: [{ name: "viewrecord", title: '<i class="fa fa-eye"></i>' }],
      position: "right",
    },

    // edit: {
    //   editButtonContent: '<i class="nb-edit"></i>',
    //   saveButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    //   confirmSave: true
    // },
    // delete: {
    //   deleteButtonContent: '<i class="nb-trash"></i>',
    //   confirmDelete: true
    // },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      // },
      // date: {
      //   title: "Date",
      //   type: "string",
      // },
      name: {
        title: "Name",
        type: "string",
      },
      email: {
        title: "Email",
        type: "string",
      },
      phone: {
        title: "Phone",
        type: "string",
      },
      address: {
        title: "Address",
        type: "string",
      },
      total: {
        title: "Total",
        type: "string",
      },
      coupon: {
        title: "Coupon",
        type: "string",
      },
      // country: {
      //   title: "Country",
      //   type: "string",
      // },
    },
  };

  socket;

  pending = ([] = []);
  shipped = ([] = []);
  deilverd = ([] = []);

  constructor(
    private orderService: OrderService,
    private dialogService: NbDialogService
  ) {
    this.socket = io("http://localhost:3000/");
    this.getAllOrders();
  }

  ngOnInit(): void {
    this.socket.on("edit-order", () => {
      this.getAllOrders();
    });
    this.socket.on("add-order", () => {
      this.getAllOrders();
    });
  }

  getAllOrders() {
    this.subs.sink = this.orderService.getAllOrders().subscribe((data) => {
      this.pending = [];
      this.shipped = [];
      this.deilverd = [];
      data["data"].forEach((element) => {
        if (element.valid == "true") {
          if (element.status == "pending") {
            this.pending.push(element);
            console.log(element);
          } else if (element.status == "deilverd") {
            this.deilverd.push(element);
          } else {
            this.shipped.push(element);
          }
        }
      });
    });
  }
  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
  create() {
    // this.mechanicService.create(this.form.value);
    // this.form.reset();
    this.makeToast();
  }

  makeToast() {
    // this.showToast(this.status, this.title, this.content);
  }

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      // event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    if (window.confirm("Are you sure you want to edit this?")) {
      // event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onCustomAction(event) {
    // console.log(event);

    this.dialogService.open(OrderMangePopupComponent, {
      context: {
        data: event.data,
      },
    });
  }
}

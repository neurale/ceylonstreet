import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbTreeGridModule,
  NbAlertModule,
  NbTabsetModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule } from '@angular/forms';
// import { AgmCoreModule } from "@agm/core";
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ViewOrdersComponent } from './view-orders/view-orders.component';
import { OrdersComponent } from './orders.component';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrderMangePopupComponent } from './view-orders/order-mange-popup/order-mange-popup.component';

@NgModule({
  imports: [
    OrdersRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbAlertModule,
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbTreeGridModule,
    NbTabsetModule,

    // AgmCoreModule,
    Ng2SmartTableModule,
  ],
  declarations: [OrdersComponent, ViewOrdersComponent, OrderMangePopupComponent],
  providers: [],
})
export class OrdersModule {}

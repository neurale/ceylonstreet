import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { id } from "@swimlane/ngx-charts";
@Injectable({
  providedIn: "root",
})
export class OrderService {
  constructor(private http: HttpClient) {}
  baseUri = "https://ceylonstreet.herokuapp.com/api/orders";
  // baseUri = "http://localhost:3000/api/orders";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  // create(collection) {
  //   // console.log(collection);
  //   return this.http.post(`${this.baseUri}/add`, collection);
  // }

  getAllOrders() {
    return this.http.get(`${this.baseUri}/order`);
  }

  setState(_id, status, phone) {
    return this.http.post(`${this.baseUri}/order`, {
      _id: _id,
      status: status,
      phone: phone,
    });
  }
}

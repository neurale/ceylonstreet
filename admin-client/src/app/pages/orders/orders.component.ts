import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-orders',
  template: ` <router-outlet></router-outlet> `,
})
export class OrdersComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: "root",
})
export class LookBookService {
  constructor(private http: HttpClient) {}

  // baseUri = "http://localhost:3000/api/lookBooks";
  baseUri = "https://ceylonstreet.herokuapp.com/api/lookBooks";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  create(data) {
    // console.log(article);
    return this.http.post(`${this.baseUri}/add`, data);
  }
}

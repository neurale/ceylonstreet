import { TestBed } from '@angular/core/testing';

import { LookBookService } from './look-book.service';

describe('LookBookService', () => {
  let service: LookBookService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LookBookService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-look-book",
  template: ` <router-outlet></router-outlet> `,
})
export class LookBookComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

import { Component, OnInit } from "@angular/core";
import { ProductService } from "../../product/product.service";
import * as io from "socket.io-client";
import { SubSink } from "subsink";
import { LookBookService } from "../look-book.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "ngx-add-look-book",
  templateUrl: "./add-look-book.component.html",
  styleUrls: ["./add-look-book.component.scss"],
})
export class AddLookBookComponent implements OnInit {
  private subs = new SubSink();

  settings = {
    actions: {
      add: false,
      edit: false,
    },

    // edit: {
    //   editButtonContent: '<i class="nb-edit"></i>',
    //   saveButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    //   confirmSave: true,
    // },
    delete: {
      deleteButtonContent: '<i class="fas fa-plus"></i>',
      confirmDelete: true,
    },

    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      // },
      name: {
        title: "Product Name",
        type: "string",
      },
      productType: {
        title: "Product Type",
        type: "string",
      },
      itemCollection: {
        title: "Collection",
        type: "string",
      },

      itemBrand: {
        title: "Brand",
        type: "string",
      },

      picUrl: {
        title: "Picture",
        type: "html",
        valuePrepareFunction: (picUrl: string) => {
          return `<img width="50px" src="${picUrl[0]}" />`;
        },
      },
    },
  };

  source = ([] = []);
  productSet = new Set();
  socket;
  radioValue=1;

  constructor(
    private productService: ProductService,
    private lookBookService: LookBookService
  ) {
    this.socket = io("https://ceylonstreet.herokuapp.com/");
    this.getAllProducts();
  }




  ngOnInit(): void {
    this.getAllProducts();

    this.socket.on("delete-product", () => {
      this.getAllProducts();
    });
    this.socket.on("edit-product", () => {
      this.getAllProducts();
    });
  }

  create() {
    // var data = {
    //   picUrl: "Hello",
    //   sliders: [{picUrl: "One",products: ["A", "B", "C"],},{ picUrl: "Two",products: ["A", "B", "C"],},],
    // };

    var data = {
      picUrl: this.pictureUrlArray[0],
      sliders: Array(this.pictureUrlArray.length - 1),
    };

    for (var i = 1; i < this.pictureUrlArray.length; i++) {
      var products = ([] = []);
      this.productSet.forEach((element) => {
        if (element["slide"] == i) {
          products.push({
            name: element["name"],
            id: element["_id"],
            picUrl: element["picUrl"][0],
          });
        }
      });
      data.sliders[i - 1] = {
        picUrl: this.pictureUrlArray[i],
        products: products,
      };
    }


    // console.log(data);
    this.lookBookService
      .create(data)
      .toPromise()
      .then((result) => {
        console.log(result);
        this.pictureUrlArray = [];
        this.files = [];
        this.productSet.clear();
        // this.form.reset();
      });
  }

  getAllProducts() {
    this.subs.sink = this.productService.getAllProducts().subscribe((data) => {
      this.source = [];
      data["data"].forEach((element) => {
        this.source.push(element);
      });
    });
  }

  deleteProduct($event) {
    this.productSet.delete($event);
  }

  addProduct(event): void {
    if (window.confirm("Are you sure you want to add?")) {
      // event.confirm.resolve();
      // this.productSet.add(event.data);
      event.data["slide"] = this.radioValue;

      this.productSet.add(event.data);
      // this.productSet.add(product);
      console.log(this.productSet);
    } else {
      event.confirm.reject();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  pictureUrlArray = ([] = []);

  isHovering: boolean;

  files: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

  getUrl(url) {
    this.pictureUrlArray.push(url);
  }
}

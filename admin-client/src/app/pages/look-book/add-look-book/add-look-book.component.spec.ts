import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLookBookComponent } from './add-look-book.component';

describe('AddLookBookComponent', () => {
  let component: AddLookBookComponent;
  let fixture: ComponentFixture<AddLookBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLookBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLookBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: "ngx-product-card",
  templateUrl: "./product-card.component.html",
  styleUrls: ["./product-card.component.scss"],
})
export class ProductCardComponent implements OnInit {
  @Input() product;

  @Output() deleteProduct = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  callParent(product): void {
    this.deleteProduct.next(product);
  }
}

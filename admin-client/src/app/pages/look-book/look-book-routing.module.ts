import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LookBookComponent } from "./look-book.component";
import { AddLookBookComponent } from "./add-look-book/add-look-book.component";

const routes: Routes = [
  {
    path: "",
    component: LookBookComponent,
    children: [
      {
        path: "add-look-book",
        component: AddLookBookComponent,
      },
      // {
      //   path: "view-product",
      //   component: ViewProductComponent,
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LookBookRoutingModule {}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import {
  FormsModule as ngFormsModule,
  ReactiveFormsModule,
} from "@angular/forms";

import {
  NbInputModule,
  NbCardModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCheckboxModule,
  NbRadioModule,
  NbDatepickerModule,
  NbSelectModule,
  NbIconModule,
  NbTreeGridModule,
} from "@nebular/theme";
import { FormsRoutingModule } from "../forms/forms-routing.module";
import { Ng2SmartTableModule } from "ng2-smart-table";

// import { UpdateProductComponent } from "./view-product/update-product/update-product.component";
import { LookBookRoutingModule } from "./look-book-routing.module";
import { LookBookComponent } from "./look-book.component";
import { AddLookBookComponent } from "./add-look-book/add-look-book.component";
import { DropzoneDirective } from "./add-look-book/dropzone.directive";
import { UploadTaskComponent } from "./add-look-book/upload-task/upload-task.component";
import { ProductCardComponent } from './add-look-book/product-card/product-card.component';

@NgModule({
  declarations: [
    LookBookComponent,
    AddLookBookComponent,
    // ViewProductComponent,
    DropzoneDirective,
    UploadTaskComponent,
    ProductCardComponent,
    // UpdateProductComponent,
    // FormsComponent,
    // FormInputsComponent,
    // FormLayoutsComponent,
    // ButtonsComponent,
  ],
  imports: [
    CommonModule,
    LookBookRoutingModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    ReactiveFormsModule,
  ],
  providers: [],
})
export class LookBookModule {}

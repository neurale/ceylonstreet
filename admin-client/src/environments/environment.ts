/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDVJoopj0rJXNTnmze_GZ5mQOVUuUb0bB0",
    authDomain: "ceylonstreet-52df2.firebaseapp.com",
    databaseURL: "https://ceylonstreet-52df2.firebaseio.com",
    projectId: "ceylonstreet-52df2",
    storageBucket: "ceylonstreet-52df2.appspot.com",
    messagingSenderId: "68807030675",
    appId: "1:68807030675:web:373de7f4259406f9df5a66",
    measurementId: "G-LTVCF55S7P",
  },
};
